/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    
    if($('.add_header_photo').length){
        var previewNode = document.querySelector(".previewTemplate");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzoneA = new Dropzone($('.add_header_photo')[0], {
            maxFiles:1,
            uploadMultiple:false,
            url: "../../../administration/default/savepageheaderphoto",
            previewTemplate:previewTemplate,
            acceptedFiles:'image/*',
            clickable: ".add_header_photo",
            previewsContainer: ".header_photo",
        });

        myDropzoneA.on("maxfilesexceeded", function(file) {
            myDropzoneA.removeAllFiles();
            myDropzoneA.addFile(file);
        });

        myDropzoneA.on("complete", function(response) {
            if (response.status == 'success') {
                $('#pages-header_img').val(response.xhr.response);
                $('.show_header_photo').css('background-image','url(/images/pages/'+response.xhr.response+')')
                $('.deleteUploadPhoto').show();
            }
            $('.header_photo').hide();            
        });
        
        myDropzoneA.on("addedfile", function(response) {
            $('.header_photo').show();            
        });
        
        
        myDropzoneA.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });
        
        $(document).on('click', '.deleteUploadPhoto', function(){
            $('.show_header_photo').css('background-image','url(/images/pages/default.jpg)')
            $('#pages-header_img').val('');
            $('.deleteUploadPhoto').hide();
            $('.header_photo').hide();
        })
    }
    
    
    
    if($('.add_slider_photo').length){
        var previewNode = document.querySelector(".previewTemplateS");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzoneA = new Dropzone($('.add_slider_photo')[0], {
            maxFiles:1,
            uploadMultiple:false,
            url: "../../../administration/default/savesliderphoto",
            previewTemplate:previewTemplate,
            acceptedFiles:'image/*',
            clickable: ".add_slider_photo",
            previewsContainer: ".slider_photo",
        });

        myDropzoneA.on("maxfilesexceeded", function(file) {
            myDropzoneA.removeAllFiles();
            myDropzoneA.addFile(file);
        });

        myDropzoneA.on("complete", function(response) {
            if (response.status == 'success') {
                $('#slider-img_src').val(response.xhr.response);
                $('.show_slider_photo').css('background-image','url(/images/slider/'+response.xhr.response+')')
                $('.deleteSliderUploadPhoto').show();
            }
            $('.slider_photo').hide();            
        });
        
        myDropzoneA.on("addedfile", function(response) {
            $('.slider_photo').show();            
        });
        
        
        myDropzoneA.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });
        
        $(document).on('click', '.deleteSliderUploadPhoto', function(){
            $('.show_slider_photo').css('background-image','url(/images/pages/default.jpg)')
            $('#slider-img_src').val('');
            $('.deleteSliderUploadPhoto').hide();
            $('.slider_photo').hide();
        })
    }
    
    
    if($('.add_brand_photo').length){
        var previewNode = document.querySelector(".previewTemplate");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzoneA = new Dropzone($('.add_brand_photo')[0], {
            maxFiles:1,
            uploadMultiple:false,
            url: "../../../administration/repairs/savebrandphoto",
            previewTemplate:previewTemplate,
            acceptedFiles:'image/*',
            clickable: ".add_brand_photo",
            previewsContainer: ".brand_photo",
        });

        myDropzoneA.on("maxfilesexceeded", function(file) {
            myDropzoneA.removeAllFiles();
            myDropzoneA.addFile(file);
        });

        myDropzoneA.on("complete", function(response) {
            if (response.status == 'success') {
                $('#brand-img_src').val(response.xhr.response);
                $('.show_brand_photo').find('img').attr('src', '/images/brand/'+response.xhr.response);                
                $('.deleteBrandPhoto').show();
            }
            $('.brand_photo').hide();            
        });
        
        myDropzoneA.on("addedfile", function(response) {
            $('.brand_photo').show();            
        });
        
        myDropzoneA.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });
        
        $(document).on('click', '.deleteBrandPhoto', function(){
            $('.show_brand_photo').find('img').attr('src', '/images/pages/default.jpg');
            $('#brand-img_src').val('');
            $('.deleteBrandPhoto').hide();
            $('.brand_photo').hide();
        })
    }
    
    $(document).on('click','.changeModelServicePrice', function(){
        var models_id = $(this).data('models_id');
        var service_id = $(this).data('service_id');
        var new_service_price = $('#ModelsService'+models_id).val();
        
        $.ajax({
            type:'POST',
            url:'../../../administration/repairs/changeserviceprice',
            data:{models_id:models_id, new_service_price:new_service_price, service_id:service_id},
            dataType:'json',
            success:function(response){
                if(response.status == 'success'){
                    $('.successSavedPrice').show();
                }
            }
        });
        
    });
        
    
    if($('.add_repair_photo').length){
        var previewNode = document.querySelector(".previewTemplate");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzoneA = new Dropzone($('.add_repair_photo')[0], {
            maxFiles:1,
            uploadMultiple:false,
            url: "../../../administration/repairs/saverepairphoto",
            previewTemplate:previewTemplate,
            acceptedFiles:'image/*',
            clickable: ".add_repair_photo",
            previewsContainer: ".repair_photo",
        });

        myDropzoneA.on("maxfilesexceeded", function(file) {
            myDropzoneA.removeAllFiles();
            myDropzoneA.addFile(file);
        });

        myDropzoneA.on("complete", function(response) {
            if (response.status == 'success') {
                $('#repairs-img_src').val(response.xhr.response);
                $('.show_repair_photo').find('img').attr('src', '/images/repair/'+response.xhr.response);                
                $('.deleteRepairPhoto').show();
            }
            $('.repair_photo').hide();            
        });
        
        myDropzoneA.on("addedfile", function(response) {
            $('.repair_photo').show();            
        });
        
        myDropzoneA.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });
        
        $(document).on('click', '.deleteRepairPhoto', function(){
            $('.show_repair_photo').find('img').attr('src', '/images/pages/default.jpg');
            $('#repairs-img_src').val('');
            $('.deleteRepairPhoto').hide();
            $('.repair_photo').hide();
        })
    }
        
});
