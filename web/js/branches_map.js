/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


    function initMap(key,address) {
        var map = new google.maps.Map(document.getElementById('map'+key), {
            zoom: 4,
        });
        
        if(address != ''){
            var geocoder = new google.maps.Geocoder();
            //var address = $(this).val();
                geocoder.geocode({'address': address}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    var Local = results[0].geometry.location;
                    var latLng = {lat:parseFloat(Local.lat()),lng:parseFloat(Local.lng())};
                    
                    var marker = new google.maps.Marker({
                      position: latLng,
                      zoom:12,
                      map: map,
                   //   title: 'Hello World!'
                    });
                    map.setCenter(latLng);
                    map.setZoom(19);
                }else{
                    alert('error');
                }
            });
        }
        
        
//        var marker = new google.maps.Marker({
//            position: uluru,
//            map: map
//        });
    }

$(document).ready(function(){
    
    $(".blockMap").each(function(){
        var key = $(this).data("branch_id");
        var address = $(this).data("address");
        
        initMap(key,address);
        
    });
    
});
