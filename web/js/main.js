/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    
    $('.owl-carousel').owlCarousel({
        animateOut: 'flipoutY',
        animateIn: 'flipInX',
        autoplay:true,
        autoplayTimeout:8000,
        autoheight: true,
        items: 1,
        margin: 0,
//        nav: false,
//        dots: true

    })

    $(document).on('click', '.inputSearchButton', function(){
        if($(this).hasClass('active')){
            $(this).removeClass('active')
        }else{
            $(this).addClass('active')            
        }
        $('.inputSearch').toggle();
    });
    
    function getBrand(thisElement){
        var repairs_id = thisElement.data('repairs_id');
        $('.BrandMenuBlock'+repairs_id).show();
        if($('.BrandMenuBlock'+repairs_id).find('.blockBrand').html() == ''){
            $.ajax({
                type: 'POST',
                url: '../../../site/getrepairsbrand',
                data: {repairs_id:repairs_id},
                dataType:'json',
                success: function(response){
                    if($('.BrandMenuBlock'+repairs_id).find('.blockBrand').html() == ''){
                        for(var key in response){
                            var brand = response[key];
                            var band_img = '<p class="brand_p">'+ brand['name'] +'</p>'
                            if( (brand['img_src']!='') && (brand['img_src']!= null) ){
                                band_img = '<img src="/images/brand/'+brand['img_src']+'" style="padding-right: 5px; width:100px;height:50px; padding-bottom:1px; margin:0 auto; display: inline-block">'
                            }
                            $('.BrandMenuBlock'+repairs_id).find('.blockBrand').append('<div class="col-sm-2 BrandShowModelsHover brand_sow_design"  data-repairs_id="'+repairs_id+'" data-brand_id="'+brand['brand_id']+'">'+
                                    '<a >'+
                                        band_img +
                                    // '<p style="text-align:center;">'+brand['name']+'</p>'+'</a>'+
                                    '</div>');
                        }
                    }
                }
            });
        }
    }          
    
    function getModels(thisElement){
        var repairs_id = thisElement.data('repairs_id');
        var brand_id = thisElement.data('brand_id');
            $.ajax({
                type: 'POST',
                url: '../../../site/getmodelsbrand',
                data: {brand_id:brand_id, repairs_id:repairs_id},
                dataType:'json',
                success: function(response){
                    $('.BrandMenuBlock'+repairs_id).find('.blockModels').html('');
                    var models = response['models'];
                    var r_slug = response['repair_url'];
                    $('.blockModels').show(); 
                    //console.log(models);
                    if(!jQuery.isEmptyObject(models)){
                        $('.BrandMenuBlock'+repairs_id).find('.viewAll').show()
                        
                        for(var key in models){
                            var model = models[key];
                            $('.BrandMenuBlock'+repairs_id).find('.blockModels').append('<div class="col-sm-12 menu_brend">'+
                                    '<a href="/repairs/'+r_slug+'/'+model['slug']+'">'+
                                    //'<img src="/images/models/'+model['img_src']+'" style="width:100px;height:100px;">'+
                                    '<p>'+model['name']+'</p>'+'</a>'+
                                    '</div>');
                        }                        
                    }else{
                        $('.BrandMenuBlock'+repairs_id).find('.blockModels').html('<a href="/makeenquiry" style="text-align:center;">Please Make Enquiry</a>');
                        $('.BrandMenuBlock'+repairs_id).find('.viewAll').hide()
                    }
                }
            });
    }          
    
    $(document).on('mouseenter',".BrandShowModelsHover",function(){
        var thisElement = $(this);
        
        $('.BrandShowModelsHover').css('border','0px')
        getModels(thisElement);
        $(thisElement).css({'border-left':'1px solid #ddd','border-right':'1px solid #ddd','border-bottom':'1px solid white'})
        
    })
    
    
    $('.repairsMenuHover').hover(function() {
            var thisElement = $(this);
            getBrand(thisElement);            
            $('.blockModels').html('');            
            $('.blockModels').hide();            
            $('.viewAll').hide();            
        },function(){
            var thisElement = $(this);
            var repairs_id = thisElement.data('repairs_id');
            var classH = $('.BrandMenuBlock'+repairs_id);
            if(classH.length){
                setTimeout(function () {
                    if (classH.is(':hover')) {
                    } else {
                        classH.hide();
                    }
                }, 100);
            }
        }
    );
    
    
    $('.BrandMenuBlock').hover(function() {
        },function(){
            var thisElement = $(this);
            var repairs_id = thisElement.data('repairs_id');
            
            console.log(repairs_id);
            var classH = $('.repairsMenuHover'+repairs_id);
            if(classH.length){
                setTimeout(function () {
                    if (classH.is(':hover')) {
                    } else {
                        thisElement.hide();
                    }
                }, 100);
            }
        }
    )
    

    $("#slider1").revolution({
			sliderType:"standard",
			sliderLayout:"auto",
			//delay:9000,
                        height:330,
			navigationStyle: "round",
			navigation: {
				arrows:{enable:false},
				thumbnails:{
					style:".tp-thumb{background: red;}",
					enable:true,
					container:"slider",
					rtl:true,
					width:50,
					height:20,
					wrapper_padding:2,
					wrapper_color:'',
					wrapper_opacity:1,
					visibleAmount:5,
					hide_onmobile:false,
					hide_onleave:false,
					hide_delay:200,
					hide_delay_mobile:1200,
					hide_under:0,
					hide_over:9999,
					tmp:'<div class="sl-thumb"></div>',
					direction:"horizontal",
					span:true,
					position:"inner",
					space:0,
					h_align:"center",
					v_align:"bottom",
					h_offset:0,
					v_offset:50
				},
				bullets: {
								enable:true,
								hide_onmobile:false,
								style:"hermes",
								hide_onleave:false,
								direction:"horizontal",
								h_align:"center",
								v_align:"bottom",
								h_offset:0,
								v_offset:20,
								space:5,
								tmp:'<span class="tp-bullet-image"></span>'
							}
			},
			//gridwidth:1300,
			gridheight:[330, 768, 960, 720],
		});


})