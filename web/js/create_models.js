/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    $(document).on('change', '#models-repairs_id', function(){
        var repairs_id = $(this).val();
        $.ajax({
            type:'POST',
            url:'../../../administration/repairs/dataformodelcreate',
            data:{repairs_id:repairs_id},
            dataType:'json',
            success:function(response){
                var brands = response['brands'];
                $('#models-brand_id').html('');
                if(!$.isEmptyObject(brands)){
                    for(var brand_id in brands){
                        $('#models-brand_id').append('<option value="'+brand_id+'">'+brands[brand_id]+'</option>');                        
                    }
                }else{
                    $('#models-brand_id').append('<option value=""> -- Pleace choise brand --</option>');                    
                }
                
                var services = response['services'];
                $('.tableServices').html('');
                if(!$.isEmptyObject(services)){
                    for(var service_id in services){
                        $('.tableServices').append('<tr>'+
                                    '<td>'+'<input type="checkbox" name="Models[services][]" value="'+service_id+'">'+'</td>'+
                                    '<td>'+services[service_id]+'</td>'+
                                    '<td><input type="number" min="1" value="1" class="form-control" name="Models[servicesprice]['+service_id+']" ></td>'+
                                '</tr>');
                    }
                }else{
                    $('.tableServices').append('<tr><td colspan="3">No results found.</td></tr>');                    
                }
                
            }
        })
        
    })
    
    
    if($('.add_models_photo').length){
        var previewNode = document.querySelector(".previewTemplate");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        var myDropzoneA = new Dropzone($('.add_models_photo')[0], {
            maxFiles:1,
            uploadMultiple:false,
            url: "../../../administration/repairs/savemodelsphoto",
            previewTemplate:previewTemplate,
            acceptedFiles:'image/*',
            clickable: ".add_models_photo",
            previewsContainer: ".models_photo",
        });

        myDropzoneA.on("maxfilesexceeded", function(file) {
            myDropzoneA.removeAllFiles();
            myDropzoneA.addFile(file);
        });

        myDropzoneA.on("complete", function(response) {
            if (response.status == 'success') {
                $('#models-img_src').val(response.xhr.response);
                $('.show_models_photo').find('img').attr('src', '/images/models/'+response.xhr.response);                
                $('.deleteModelsPhoto').show();
            }
            $('.models_photo').hide();            
        });
        
        myDropzoneA.on("addedfile", function(response) {
            $('.models_photo').show();            
        });
        
        myDropzoneA.on("removedfile", function(response) {
            if(response.xhr != null){
               //deleteFile(response.xhr.response);
            }
        });
        
        $(document).on('click', '.deleteModelsPhoto', function(){
            $('.show_models_photo').find('img').attr('src', '/images/pages/default.jpg');
            $('#models-img_src').val('');
            $('.deleteModelsPhoto').hide();
            $('.models_photo').hide();
        })
    }
    
    
});
