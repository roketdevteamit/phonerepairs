<?php
namespace app\widgets;
use yii\db\Query;

class GetRepairsNameWidget extends \yii\bootstrap\Widget
{
    public $repair_id;
    
    public function init(){}

    public function run() {
        $modelRepairs = \app\models\Repairs::find()->where(['id' => $this->repair_id])->one();
        $repair_name = [];
        if($modelRepairs){
            $repair_name = $modelRepairs->name;
        }
        return $repair_name;
    }
}
