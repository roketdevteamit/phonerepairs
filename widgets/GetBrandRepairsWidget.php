<?php
namespace app\widgets;
use yii\db\Query;
class GetBrandRepairsWidget extends \yii\bootstrap\Widget
{
    public $brand_id;
    
    public function init(){}

    public function run() {
        
        $query = new Query();
        $query->select(['*', 'id' => '{{brand_repairs}}.id'])
                        ->from('{{brand_repairs}}')
                        ->join('LEFT JOIN',
                                    '{{repairs}}',
                                    '{{repairs}}.id = {{brand_repairs}}.repairs_id')
                ->where(['{{brand_repairs}}.brand_id' => $this->brand_id]);
        
        $command = $query->createCommand();
        $modelRepairs = $command->queryAll();
        $repairsT = '';
        foreach($modelRepairs as $repairs){
            $repairsT = $repairsT.$repairs['name'].'; ';
        }
        return $repairsT;
    }
}
