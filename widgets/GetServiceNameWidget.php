<?php
namespace app\widgets;
use yii\db\Query;

class GetServiceNameWidget extends \yii\bootstrap\Widget
{
    public $service_id;
    
    public function init(){}

    public function run() {
        $modelService = \app\models\Service::find()->where(['id' => $this->service_id])->one();
        $service_name = [];
        if($modelService){
            $service_name = $modelService->name;
        }
        return $service_name;
    }
}
