<?php
namespace app\widgets;
use yii\db\Query;

class GetModelsNameWidget extends \yii\bootstrap\Widget
{
    public $models_id;
    
    public function init(){}

    public function run() {
        $modelModels = \app\models\Models::find()->where(['id' => $this->models_id])->one();
        $models_name = [];
        if($modelModels){
            $models_name = $modelModels->name;
        }
        return $models_name;
    }
}
