<?php
namespace app\widgets;
use yii\db\Query;

class GetBranchAddressWidget extends \yii\bootstrap\Widget
{
    public $branch_id;
    
    public function init(){}

    public function run() {
        $modelBranch = \app\models\Branches::find()->where(['id' => $this->branch_id])->one();
        $branch_address = [];
        if($modelBranch){
            $branch_address = $modelBranch->address;
        }
        return $branch_address;
    }
}
