<?php
namespace app\widgets;
use yii\db\Query;

class GetServiceRepairsWidget extends \yii\bootstrap\Widget
{
    public $service_id;
    
    public function init(){}

    public function run() {
        $query = new Query();
        $query->select(['*', 'id' => '{{service_repairs}}.id'])
                        ->from('{{service_repairs}}')
                        ->join('LEFT JOIN',
                                    '{{repairs}}',
                                    '{{repairs}}.id = {{service_repairs}}.repairs_id')
                ->where(['{{service_repairs}}.service_id' => $this->service_id]);
        
        $command = $query->createCommand();
        $modelRepairs = $command->queryAll();
        $repairsT = '';
        foreach($modelRepairs as $repairs){
            $repairsT = $repairsT.$repairs['name'].'; ';
        }
        return $repairsT;
    }
}
