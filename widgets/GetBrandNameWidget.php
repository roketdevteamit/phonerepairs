<?php
namespace app\widgets;
use yii\db\Query;

class GetBrandNameWidget extends \yii\bootstrap\Widget
{
    public $brand_id;
    
    public function init(){}

    public function run() {
        $modelBrand = \app\models\Brand::find()->where(['id' => $this->brand_id])->one();
        $brand_name = [];
        if($modelBrand){
            $brand_name = $modelBrand->name;
        }
        return $brand_name;
    }
}
