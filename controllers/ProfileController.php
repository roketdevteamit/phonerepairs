<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Leads;
use app\models\Models;
use app\models\Repairs;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Setting;
use app\models\Slider;
use app\models\Branches;
use app\models\Leadsmessage;
use app\models\Service;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use app\models\Modelsservice;
use yii\helpers\ArrayHelper;

class ProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }
     public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        if(!\Yii::$app->user->isGuest){
            return parent::beforeAction($action);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
            $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
            if($modelUser){
                    $modelUser->scenario = 'signup';
                    if ($modelUser->load(Yii::$app->request->post())) {
                        $modelUser->password_hash = \Yii::$app->security->generatePasswordHash($modelUser->password);
                        $modelUser->auth_key = 'key';
                        if ($modelUser->save()) {
                            Yii::$app->session->setFlash('profile_updated');
                            $modelUser = User::find()->where(['id' => \Yii::$app->user->id])->one();
                        }else{
                            Yii::$app->session->setFlash('profile_not_updated');                            
                        }
                    }
                
                return $this->render('index',[
                    'modelUser' => $modelUser,
                ]);                
            }else{
                throw new \yii\web\NotFoundHttpException();                
            }
            
    }    

    public function actionEnquiries()
    {
        $queryEnquiry = Leads::find()->where(['leads_type' => 0, 'user_id' => \Yii::$app->user->id]);
        $modelEnquiry = new ActiveDataProvider(['query' => $queryEnquiry, 'pagination' => ['pageSize' => 10]]);

        return $this->render('enquiry',[
            'modelEnquiry' => $modelEnquiry,
        ]);
    }
    
    public function actionEnquiriesshow($id)
    {
            
        $modelEnquiry = Leads::find()->where(['id' => $id, 'user_id' => \Yii::$app->user->id])->one();
        if($modelEnquiry){
            $modelNewLeadmessage = new Leadsmessage();
            $modelNewLeadmessage->scenario = 'add';
            
            
            if($_POST){
                if(isset($_POST['sent_commect'])){
                    if($modelNewLeadmessage->load(Yii::$app->request->post())){
                        if($modelNewLeadmessage->save()){
                            Yii::$app->session->setFlash('message_send');
                        }else{
                            Yii::$app->session->setFlash('message_not_send');  
                        }
                    }
                }                
                
            }
            
            $queryLeadmessage = new Query();
            $queryLeadmessage->select(['*', 'id' => '{{leads_message}}.id', 'date_create' => '{{leads_message}}.date_create'])
                            ->from('{{leads_message}}')
                            ->join('LEFT JOIN',
                                        '{{users}}',
                                        '{{users}}.id = {{leads_message}}.user_id')
                    ->where(['{{leads_message}}.leads_id' => $id]);
            $command = $queryLeadmessage->createCommand();
            $modelLeadmessage = $command->queryAll();

            return $this->render('enquiryshow',[
                'modelNewLeadmessage' => $modelNewLeadmessage,
                'modelEnquiry' => $modelEnquiry,
                'modelLeadmessage' => $modelLeadmessage,
            ]);            
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    
    public function actionBookings()
    {
        $queryBookings = Leads::find()->where(['leads_type' => 2, 'user_id' => \Yii::$app->user->id]);
        $modelBookings = new ActiveDataProvider(['query' => $queryBookings, 'pagination' => ['pageSize' => 50]]);
        
        $modelBranch = Branches::find()->asArray()->all();
        
        return $this->render('bookings',[
            'modelBookings' => $modelBookings,
        ]);
    }
    
    public function actionBookingsshow($id)
    {            
        $modelBooking = Leads::find()->where(['id' => $id, 'user_id' => \Yii::$app->user->id])->one();
        if($modelBooking){
            $modelNewLeadmessage = new Leadsmessage();
            $modelNewLeadmessage->scenario = 'add';
            
            
            if($_POST){
                if(isset($_POST['sent_commect'])){
                    if($modelNewLeadmessage->load(Yii::$app->request->post())){
                        if($modelNewLeadmessage->save()){
//                            var_dump($_POST);exit;
                            Yii::$app->session->setFlash('message_send');
                        }else{
                            Yii::$app->session->setFlash('message_not_send');  
                        }
                    }
                }                
                
            }
            $queryLeadmessage = new Query();
            $queryLeadmessage->select(['*', 'id' => '{{leads_message}}.id', 'date_create' => '{{leads_message}}.date_create'])
                            ->from('{{leads_message}}')
                            ->join('LEFT JOIN',
                                        '{{users}}',
                                        '{{users}}.id = {{leads_message}}.user_id')
                    ->where(['{{leads_message}}.leads_id' => $id]);
            $command = $queryLeadmessage->createCommand();
            $modelLeadmessage = $command->queryAll();
            
            $arrayBranch = ArrayHelper::map(Branches::find()->asArray()->all(), 'id', 'address');
            $arrayModels = ArrayHelper::map(Models::find()->asArray()->all(), 'id', 'name');
            $arrayService = ArrayHelper::map(Service::find()->asArray()->all(), 'id', 'name');
            $arrayAssigned = ArrayHelper::map(User::find()->where(['type' => 'admin'])->where(['type' => 'manager'])->asArray()->all(), 'id', 'name');

            return $this->render('bookingsshow',[
                'modelNewLeadmessage' => $modelNewLeadmessage,
                'modelBooking' => $modelBooking,
                'modelLeadmessage' => $modelLeadmessage,
                'arrayBranch' => $arrayBranch,
                'arrayModels' => $arrayModels,
                'arrayService' => $arrayService,
                'arrayAssigned' => $arrayAssigned,
            ]);            
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    
     public function actionMail_inshow($id)
    {            
        $modelMailin = Leads::find()->where(['id' => $id, 'user_id' => \Yii::$app->user->id])->one();
        $modelMailin->scenario = 'all';
        if($modelMailin){
            $modelNewLeadmessage = new Leadsmessage();
            $modelNewLeadmessage->scenario = 'add';
            
            
            if($_POST){
                if(isset($_POST['sent_commect'])){
                    if($modelNewLeadmessage->load(Yii::$app->request->post())){
                        if($modelNewLeadmessage->save()){
//                            var_dump($_POST);exit;
                            Yii::$app->session->setFlash('message_send');
                        }else{
                            Yii::$app->session->setFlash('message_not_send');  
                        }
                    }
                }
            }
            $queryLeadmessage = new Query();
            $queryLeadmessage->select(['*', 'id' => '{{leads_message}}.id', 'date_create' => '{{leads_message}}.date_create'])
                            ->from('{{leads_message}}')
                            ->join('LEFT JOIN',
                                        '{{users}}',
                                        '{{users}}.id = {{leads_message}}.user_id')
                    ->where(['{{leads_message}}.leads_id' => $id]);
            $command = $queryLeadmessage->createCommand();
            $modelLeadmessage = $command->queryAll();
            
            return $this->render('mail_inshow',[
                'modelNewLeadmessage' => $modelNewLeadmessage,
                'modelLeadmessage' => $modelLeadmessage,
                'modelMailin' => $modelMailin,
            ]);            
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    
    public function actionMail_in()
    {
        $queryMailin = Leads::find()->where(['leads_type' => 1, 'user_id' => \Yii::$app->user->id]);
        $modelMailin = new ActiveDataProvider(['query' => $queryMailin, 'pagination' => ['pageSize' => 10]]);

        return $this->render('mail_in',[
            'modelMailin' => $modelMailin,
        ]);
    }
    
    public function actionPick_ups()
    {
        $queryPickups = Leads::find()->where(['leads_type' => 3, 'user_id' => \Yii::$app->user->id]);
        $modelPickups = new ActiveDataProvider(['query' => $queryPickups, 'pagination' => ['pageSize' => 10]]);

        return $this->render('pick_ups',[
            'modelPickups' => $modelPickups,
        ]);
    }
    
    public function actionCorporate()
    {
        $queryCorporate = Leads::find()->where(['leads_type' => 4, 'user_id' => \Yii::$app->user->id]);
        $modelCorporate = new ActiveDataProvider(['query' => $queryCorporate, 'pagination' => ['pageSize' => 10]]);

        return $this->render('corporate',[
            'modelCorporate' => $modelCorporate,
        ]);
    }
    
    public function actionCorporateshow($id)
    {
        $modelCorporate = Leads::find()->where(['id' => $id, 'user_id' => \Yii::$app->user->id])->one();
        $modelCorporate->scenario = 'all';
        if($modelCorporate){
            $modelNewLeadmessage = new Leadsmessage();
            $modelNewLeadmessage->scenario = 'add';
            
            
            if($_POST){
                if(isset($_POST['sent_commect'])){
                    if($modelNewLeadmessage->load(Yii::$app->request->post())){
                        if($modelNewLeadmessage->save()){
//                            var_dump($_POST);exit;
                            Yii::$app->session->setFlash('message_send');
                        }else{
                            Yii::$app->session->setFlash('message_not_send');  
                        }
                    }
                }
            }
            $queryLeadmessage = new Query();
            $queryLeadmessage->select(['*', 'id' => '{{leads_message}}.id', 'date_create' => '{{leads_message}}.date_create'])
                            ->from('{{leads_message}}')
                            ->join('LEFT JOIN',
                                        '{{users}}',
                                        '{{users}}.id = {{leads_message}}.user_id')
                    ->where(['{{leads_message}}.leads_id' => $id]);
            $command = $queryLeadmessage->createCommand();
            $modelLeadmessage = $command->queryAll();
            
            return $this->render('corporateshow',[
                'modelNewLeadmessage' => $modelNewLeadmessage,
                'modelLeadmessage' => $modelLeadmessage,
                'modelCorporate' => $modelCorporate,
            ]);            
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    
}
