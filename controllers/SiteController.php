<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Leads;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Setting;
use app\models\Slider;
use yii\db\Query;
set_time_limit(5000);

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $modelSlider = Slider::find()->orderBy('sort ASC')->all();
        $configData = \yii\helpers\ArrayHelper::map(Setting::find()->all(), 'key', 'value');
        $modelNewLeads = new Leads();
        $modelNewLeads->scenario = 'enquiry_home_page';

        if ($_POST) {
            if ($modelNewLeads->load(Yii::$app->request->post())) {
                $user_email = $modelNewLeads->email;
                if ($modelNewLeads->save()) {
                    
                    $modelNewLeads->scenario = 'add_identification_id';
                    $modelNewLeads->identification_id = $modelNewLeads->key.$modelNewLeads->id;
                    $modelNewLeads->save();
                    
                    $user_signup = '';
                    $useremail = '';
                    $userpassword_2 = "";
                    $userpassword = '';
                    if (Yii::$app->user->isGuest) {
                        $modelUser = User::find()->where(['email' => $modelNewLeads->email])->one();
                        if (!$modelUser) {
                            $userpassword = substr(str_shuffle(str_repeat("0123456789abcdefghij!_klmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
                            $modelNewUser = new User();
                            $modelNewUser->scenario = 'signup_t';
                            $modelNewUser->email = $modelNewLeads->email;
                            $modelNewUser->password_hash = \Yii::$app->security->generatePasswordHash($userpassword);
                            $modelNewUser->auth_key = 'key';
                            if ($modelNewUser->save()) {
                                $modelNewLeads->scenario = 'add_user';
                                $modelNewLeads->user_id = $modelNewUser->id;
                                $modelNewLeads->save();
                                $user_signup = 'Please keep Password Secure, this is uniquely generated and will only be issued once <br>
                                        Future password changes can be made in your PhoneRepairs4u profile page';
                            }
                        } else {
                            $modelNewLeads->scenario = 'add_user';
                            $modelNewLeads->user_id = $modelUser->id;
                            $modelNewLeads->save();
                        }

                        $userpassword_2 = "<b>Password:</b> ".$userpassword;
                        $useremail = $modelNewLeads->email;
                    } else {
                        //                    var_dump($modelNewLeads->email);exit;
                        $modelNewLeads->scenario = 'add_user';
                        $modelNewLeads->user_id = \Yii::$app->user->id;
                        $modelNewLeads->save();

                        $modelNewLeads->user_id = \Yii::$app->user->id;
                        $useremail = $modelNewLeads->email;
                    }


                    $social_links_facebook = '';
                    if (isset($configData['facebook'])) {
                        $social_links_facebook = strip_tags($configData['facebook']);
                    }
                    else{
                    }
                    
                    $social_links_twitter = '';
                    if (isset($configData['twitter'])) {
                        $social_links_twitter = strip_tags($configData['twitter']);
                    }
                    else{
                    }
                    $social_links_linkedin = '';
                    if (isset($configData['linkedin'])) {
                        $social_links_linkedin = strip_tags($configData['linkedin']);
                    }
                    else{
                    }

                    if (isset($configData['telephoneinstagram'])) {
                        $social_links_telephoneinstagram = strip_tags($configData['telephoneinstagram']);
                    }
                    else{
                    }

                    
                    if (\Yii::$app->mailer->compose()
                        ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                        ->setTo($modelNewLeads->email)
                        ->setSubject('Thanks for contacting us. Reference: (EN' . $modelNewLeads->id . ')')
                        ->setHtmlBody("    
                      <html>
                        <head>
                                <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                        </head>
                             <body>
                               <div style='text-align: center'>
                                    <div style='display: inline-block; width: 50%; text-align: right'>
                                            <ul>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a href='".$social_links_facebook."'>
                                                        <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                    </a>
                                                </li>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a href='".$social_links_twitter."'>
                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                    </a>
                                                </li>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a  href='".$social_links_linkedin."'>
                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                    </a>
                                                </li>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a href='".$social_links_telephoneinstagram."'>
                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                    </a>
                                            </ul>
                                        </div>
                                    </div>
                                <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                    <div style='text-align: center'>
                                    <div style='text-align: right; display: inline-block'>
                                    </div>
                                    <div style='display: inline-block; text-align: left'>
                                        <p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'>
                                        Dear " . $modelNewLeads->name . ",
                                        <br>
                                        <br>
                                        Thank you for contacting us, we have received your enquiry and will be
                                        <br>
                                        replying to you within the next hour.
                                        <br>
                                        <br>
                                        Your unique reference number is: <b>EN" . $modelNewLeads->id . "</b>.<br>
                                        It's advised you keep this number in a safe place as if you choose to walk into
                                        <br>
                                        one of our repair centres, it will help us to keep you up to date on the progress of your repair.
                                        <br>
                                        <br>
                                        <b>Discount on repairs</b>
                                        <br>
                                        <br>
                                        We would like to inform you about our Special Deal.
                                        <br>
                                        We are now giving a <b>£ 5 discount</b> on all the repairs when you make <a style='color:blue'>a booking
                                            online.</a>
                                        <br>
                                        <br>
                                        Kind Regards,
                                        <br>
                                        The Team at PhoneRepairs4u.co.uk
                                        <br>
                                        You can check status of your booking in your profile.
                                        <br>
                                        https://www.phonesrepair4u.com/login
                                        <br>
                                        <br>
                                        <b>Username:</b> " . $useremail . "
                                         <br>
                                        " . $userpassword_2 . "
                                         <br>
                                        " . $user_signup . "
                                        </p>
                                    </div>
                                    </div>
                                    <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                    <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                    <div><p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'> If you’d like to unsubscribe and stop receiving these emails <a href='#' style='text-decoration: none'>click here.</a></div>
                                </body>
                            </html>
                           ")
                        ->send()
                    ){
                        Yii::$app->session->setFlash('form_sended');
                    } else {
                        Yii::$app->session->setFlash('form__not_sended');
                    }
                    
                    
                    
                    
                    //admin email begin
                    
                        $usersAdmin = User::find()->where(['type' => 'admin'])->asArray()->all();
                        if($usersAdmin){
                            foreach($usersAdmin as $adminu){
                                if (filter_var($adminu['email'], FILTER_VALIDATE_EMAIL)) {
                                      if (\Yii::$app->mailer->compose()
                                        ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                                        ->setTo($adminu['email'])
                                        ->setSubject('New Reference: (EN' . $modelNewLeads->id . ')')
                                        ->setHtmlBody("    
                                                    <html>
                                                      <head>
                                                              <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                                                      </head>
                                                           <body>
                                                             <div style='text-align: center'>
                                                                  <div style='display: inline-block; width: 50%; text-align: right'>
                                                                          <ul>
                                                                              <li style='list-style: none; display: inline-block'>
                                                                                  <a href='".$social_links_facebook."'>
                                                                                      <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                                  </a>
                                                                              </li>
                                                                              <li style='list-style: none; display: inline-block'>
                                                                                  <a href='".$social_links_twitter."'>
                                                                                      <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                                  </a>
                                                                              </li>
                                                                              <li style='list-style: none; display: inline-block'>
                                                                                  <a  href='".$social_links_linkedin."'>
                                                                                      <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                                  </a>
                                                                              </li>
                                                                              <li style='list-style: none; display: inline-block'>
                                                                                  <a href='".$social_links_telephoneinstagram."'>
                                                                                      <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                                  </a>
                                                                          </ul>
                                                                      </div>
                                                                  </div>
                                                              <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                                  <div style='text-align: center'>
                                                                  <div style='text-align: right; display: inline-block'>
                                                                  </div>
                                                                  <div style='display: inline-block; text-align: left'>
                                                                        <h2>New reference number: <b>EN" . $modelNewLeads->id . "</b></h2><br>
                                                                        <table border='1'>
                                                                            <tr>
                                                                                <td>Name</td>
                                                                                <td>".$modelNewLeads->name."</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Phone number</td>
                                                                                <td>".$modelNewLeads->phone_number."</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Email</td>
                                                                                <td>".$modelNewLeads->email."</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Device model</td>
                                                                                <td>".$modelNewLeads->device_model."</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Description</td>
                                                                                <td>".$modelNewLeads->description."</td>
                                                                            </tr>
                                                                        </table>
                                                                  </div>
                                                                  </div>
                                                                  <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                                  <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                              </body>
                                                          </html>
                                                         ")
                                                      ->send()
                                                  ){}

                                }
                            }
                        }
                    
                    //admin email end
                    
                    
                    
                    
                    
                    
                }

                if ($modelNewLeads->save()) {
                    Yii::$app->session->setFlash('form_home_sended');
                    $modelNewLeads = new Leads();
                    $modelNewLeads->scenario = 'enquiry_home_page';
                } else {
                    Yii::$app->session->setFlash('form_home_not_sended');
                }
            }
        }
        
        
        return $this->render('index', [
            'modelNewLeads' => $modelNewLeads,
            'configData' => $configData,
            'modelSlider' => $modelSlider
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
//    public function actionLogin()
//    {
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new LoginForm();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
//        }
//        return $this->render('login', [
//            'model' => $model,
//        ]);
//    }
    public function actionLogin()
    {
        $serviceName = \Yii::$app->getRequest()->getQueryParam('service');
        if (isset($serviceName)) {
            /** @var $eauth \nodge\eauth\ServiceBase */
            $eauth = Yii::$app->get('eauth')->getIdentity($serviceName);
            $eauth->setRedirectUrl(Yii::$app->getUser()->getReturnUrl());
            $eauth->setCancelUrl(Yii::$app->getUrlManager()->createAbsoluteUrl('site/login'));

            try {
                if ($eauth->authenticate()) {
//                  var_dump($eauth->getIsAuthenticated(), $eauth->getAttributes()); exit;

                    $identity = User::findByEAuth($eauth);
                    $profile = $eauth->getAttributes();
                    //var_dump($profile);exit;
                    if (!User::find()->where(['username' => $profile['name'], 'email' => (isset($profile['email'])) ? $profile['email'] : $profile['id']])->exists()) {
                        $model = new User();
                        $model->username = (isset($profile['name'])) ? $profile['name'] : $profile['id'];
                        $model->password = \Yii::$app->security->generateRandomString(8);
                        $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
                        $model->auth_key = \Yii::$app->security->generateRandomString(8);
                        $model->username = $profile['name'];
                        $model->email = (isset($profile['email'])) ? $profile['email'] : $profile['id'];
                        $model->signup_type = $eauth->getServiceName();
                        $model->social_id = (isset($profile['id'])) ? $profile['id'] : '';
                        if ($model->save()) {
                            if (Yii::$app->getUser()->login($model)) {
                                $eauth->redirect();
                            }
                        }
                    } else {
                        $model = User::find()->where(['social_id' => (isset($profile['id'])) ? $profile['id'] : $profile['id'], 'username' => $profile['name'], 'email' => (isset($profile['email'])) ? $profile['email'] : $profile['id']])->one();
                        if (Yii::$app->getUser()->login($model)) {
                            $eauth->redirect();
                        }
                    }
                    //Yii::$app->getUser()->login($identity);

                    // special redirect with closing popup window
//                    $eauth->redirect();
                } else {
                    // close popup window and redirect to cancelUrl
                    $eauth->cancel();
                }
            } catch (\nodge\eauth\ErrorException $e) {
                // save error to show it later
                Yii::$app->getSession()->setFlash('error', 'EAuthException: ' . $e->getMessage());

                // close popup window and redirect to cancelUrl
//              $eauth->cancel();
                $eauth->redirect($eauth->getCancelUrl());
            }
        }

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if((\Yii::$app->user->identity->type == 'admin') ||(\Yii::$app->user->identity->type == 'manager')){
                return $this->redirect('/administration');
            }else{
                return $this->goBack();                
            }
        }
        return $this->render('login', [
            'model' => $model,
        ]);

    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }
        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
            return $this->goHome();
        }
        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(Url::home());
        }
        $model = new User();

        $request = Yii::$app->request;
        $postedForm = $request->post('ContactForm');

        $model->scenario = 'signup';
        if ($model->load($request->post())) {
            $model->password_hash = \Yii::$app->security->generatePasswordHash($model->password);
            $model->auth_key = 'key';
            if ($model->save()) {
                if (Yii::$app->getUser()->login($model)) {
                    return $this->redirect(Url::home());
                }
            }
        }

        return $this->render('signup', [
            'newModel' => $model,
        ]);
    }

    public function actionGetrepairsbrand()
    {
        $repairs_id = $_POST['repairs_id'];
        $queryBrands = new Query();
        $queryBrands->select(['*', 'id' => '{{brand}}.id'])
            ->from('{{brand_repairs}}')
            ->join('LEFT JOIN',
                '{{brand}}',
                '{{brand}}.id = {{brand_repairs}}.brand_id')
            ->where(['{{brand_repairs}}.repairs_id' => $repairs_id])->orderBy('sort ASC');
        $command = $queryBrands->createCommand();
        $modelBrands = $command->queryAll();

        echo json_encode($modelBrands);
    }

    public function actionGetmodelsbrand()
    {
        $result = [];
        $brand_id = $_POST['brand_id'];
        $repairs_id = $_POST['repairs_id'];

        $modelRepairs = \app\models\Repairs::find()->where(['id' => $repairs_id])->one();
        $modelModels = \app\models\Models::find()->where(['brand_id' => $brand_id, 'repairs_id' => $repairs_id])->asArray()->orderBy('date_create DESC')->all();
        $result['repair_url'] = $modelRepairs->slug;
        $result['models'] = $modelModels;

        echo json_encode($result);
    }

    
    public function actionMakeenquiry()
    {
        $modelNewLeads = new Leads();
        $modelNewLeads->scenario = 'enquiry_home_page';
        $configData = \yii\helpers\ArrayHelper::map(Setting::find()->all(), 'key', 'value');
        
        
        if ($_POST) {
            if ($modelNewLeads->load(Yii::$app->request->post())) {
                $user_email = $modelNewLeads->email;
                if ($modelNewLeads->save()) {
                    
                    $modelNewLeads->scenario = 'add_identification_id';
                    $modelNewLeads->identification_id = $modelNewLeads->key.$modelNewLeads->id;
                    $modelNewLeads->save();
                    
                    $useremail = '';
                    $userpassword = '';
                    $user_signup = '';
                    if (Yii::$app->user->isGuest) {
                        $modelUser = User::find()->where(['email' => $modelNewLeads->email])->one();
                        if (!$modelUser) {
                            $userpassword = substr(str_shuffle(str_repeat("0123456789abcdefghij!_klmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
                            $modelNewUser = new User();
                            $modelNewUser->scenario = 'signup_t';
                            $modelNewUser->email = $modelNewLeads->email;
                            $modelNewUser->password_hash = \Yii::$app->security->generatePasswordHash($userpassword);
                            $modelNewUser->auth_key = 'key';
                            if ($modelNewUser->save()) {
                                $modelNewLeads->scenario = 'add_user';
                                $modelNewLeads->user_id = $modelNewUser->id;
                                $modelNewLeads->save();
                                $user_signup = 'Please keep Password Secure, this is uniquely generated and will only be issued once <br>
                                Future password changes can be made in your PhoneRepairs4u profile page';
                            }
                        } else {
                            $modelNewLeads->scenario = 'add_user';
                            $modelNewLeads->user_id = $modelUser->id;
                            $modelNewLeads->save();
                        }

                        $useremail = $modelNewLeads->email;
                    } else {
                        //                    var_dump($modelNewLeads->email);exit;
                        $modelNewLeads->scenario = 'add_user';
                        $modelNewLeads->user_id = \Yii::$app->user->id;
                        $modelNewLeads->save();

                        $modelNewLeads->user_id = \Yii::$app->user->id;
                        $useremail = $modelNewLeads->email;
                    }


                    $social_links_facebook = '';
                    if (isset($configData['facebook'])) {
                        $social_links_facebook = strip_tags($configData['facebook']);
                    }
                    else{
                    }
                    
                    $social_links_twitter = '';
                    if (isset($configData['twitter'])) {
                        $social_links_twitter = strip_tags($configData['twitter']);
                    }
                    else{
                    }
                    $social_links_linkedin = '';
                    if (isset($configData['linkedin'])) {
                        $social_links_linkedin = strip_tags($configData['linkedin']);
                    }
                    else{
                    }

                    if (isset($configData['telephoneinstagram'])) {
                        $social_links_telephoneinstagram = strip_tags($configData['telephoneinstagram']);
                    }
                    else{
                    }

                    $userpassword_2 ="<b>Password:</b> ".$userpassword;
                    if (\Yii::$app->mailer->compose()
                        ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                        ->setTo($modelNewLeads->email)
                        ->setSubject('Thanks for contacting us. Reference: (EN' . $modelNewLeads->id . ')')
                        ->setHtmlBody("    
                      <html>
                        <head>
                                <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                        </head>
                             <body>
                               <div style='text-align: center'>
                                    <div style='display: inline-block; width: 50%; text-align: right'>
                                            <ul>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a href='".$social_links_facebook."'>
                                                        <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                    </a>
                                                </li>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a href='".$social_links_twitter."'>
                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                    </a>
                                                </li>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a  href='".$social_links_linkedin."'>
                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                    </a>
                                                </li>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a href='".$social_links_telephoneinstagram."'>
                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                    </a>
                                            </ul>
                                        </div>
                                    </div>
                                <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                    <div style='text-align: center'>
                                    <div style='text-align: right; display: inline-block'>
                                    </div>
                                    <div style='display: inline-block; text-align: left'>
                                        <p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'>
                                        Dear " . $modelNewLeads->name . ",
                                        <br>
                                        <br>
                                        Thank you for contacting us, we have received your enquiry and will be
                                        <br>
                                        replying to you within the next hour.
                                        <br>
                                        <br>
                                        Your unique reference number is: <b>EN" . $modelNewLeads->id . "</b>.<br>
                                        It's advised you keep this number in a safe place as if you choose to walk into
                                        <br>
                                        one of our repair centres, it will help us to keep you up to date on the progress of your repair.
                                        <br>
                                        <br>
                                        <b>Discount on repairs</b>
                                        <br>
                                        <br>
                                        We would like to inform you about our Special Deal.
                                        <br>
                                        We are now giving a <b>£ 5 discount</b> on all the repairs when you make <a style='color:blue'>a booking
                                            online.</a>
                                        <br>
                                        <br>
                                        Kind Regards,
                                        <br>
                                        The Team at PhoneRepairs4u.co.uk
                                        <br>
                                        You can check status of your booking in your profile.
                                        <br>
                                        https://www.phonesrepair4u.com/login
                                        <br>
                                        <br>
                                        <b>Username:</b> " . $useremail . "
                                         <br>
                                        " . $userpassword_2 . "
                                         <br>
                                        " . $user_signup . "
                                            
                                        </p>
                                    </div>
                                    </div>
                                    <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                    <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                    <div><p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'> If you’d like to unsubscribe and stop receiving these emails <a href='#' style='text-decoration: none'>click here.</a></div>
                                </body>
                            </html>
                           ")
                        ->send()
                    ){
                        Yii::$app->session->setFlash('form_sended');
                    } else {
                        Yii::$app->session->setFlash('form__not_sended');
                    }
                    
                    
                    $usersAdmin = User::find()->where(['type' => 'admin'])->asArray()->all();
                    //admin email begin
                    if($usersAdmin){
                        foreach($usersAdmin as $adminu){
                            if (filter_var($adminu['email'], FILTER_VALIDATE_EMAIL)) {
                                  if (\Yii::$app->mailer->compose()
                                    ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                                    ->setTo($adminu['email'])
                                    ->setSubject('New Reference: (EN' . $modelNewLeads->id . ')')
                                    ->setHtmlBody("    
                                                <html>
                                                  <head>
                                                          <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                                                  </head>
                                                       <body>
                                                         <div style='text-align: center'>
                                                              <div style='display: inline-block; width: 50%; text-align: right'>
                                                                      <ul>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_facebook."'>
                                                                                  <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_twitter."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a  href='".$social_links_linkedin."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_telephoneinstagram."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                              </a>
                                                                      </ul>
                                                                  </div>
                                                              </div>
                                                          <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                              <div style='text-align: center'>
                                                              <div style='text-align: right; display: inline-block'>
                                                              </div>
                                                              <div style='display: inline-block; text-align: left'>
                                                                    <h2>New reference number: <b>EN" . $modelNewLeads->id . "</b></h2><br>
                                                                    <table border='1'>
                                                                        <tr>
                                                                            <td>Name</td>
                                                                            <td>".$modelNewLeads->name."</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Phone number</td>
                                                                            <td>".$modelNewLeads->phone_number."</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Email</td>
                                                                            <td>".$modelNewLeads->email."</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Device model</td>
                                                                            <td>".$modelNewLeads->device_model."</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>Description</td>
                                                                            <td>".$modelNewLeads->description."</td>
                                                                        </tr>
                                                                    </table>
                                                              </div>
                                                              </div>

                                                              <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>
                                                              
                                                              <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                          </body>
                                                      </html>
                                                     ")
                                                  ->send()
                                              ){}
                                
                            }
                        }
                    }
                    
                    //admin email end
                    
                    
                    
                    
                    
                }

                if ($modelNewLeads->save()) {
                    Yii::$app->session->setFlash('form_home_sended');
                    $modelNewLeads = new Leads();
                    $modelNewLeads->scenario = 'enquiry_home_page';
                } else {
                    Yii::$app->session->setFlash('form_home_not_sended');
                }
            }
        }

        return $this->render('makeenquiry', [
            'modelNewLeads' => $modelNewLeads,
        ]);
    }
    
//    public function actionParser()
//    {
//        $modelContact = \app\models\Contact::find()->asArray()->all();
//        $configData = \yii\helpers\ArrayHelper::map(Setting::find()->all(), 'key', 'value');
////        $modelContact = [];
////        $modelContact[] = ['email' => 'datsivStepan@ukr.net'];
////        $modelContact[] = ['email' => 'datsivStepan'];
//        foreach($modelContact as $contact){
//            if (filter_var($contact['email'], FILTER_VALIDATE_EMAIL)) {
//                        $modelUser = User::find()->where(['email' => $contact['email']])->one();
//                        if(!$modelUser){
//                            $userpassword = substr(str_shuffle(str_repeat("0123456789abcdefghij!_klmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
//                            $modelNewUser = new User();
//                            $modelNewUser->scenario = 'signup_t';
//                            $modelNewUser->email = $contact['email'];
//                            $modelNewUser->password_hash = \Yii::$app->security->generatePasswordHash($userpassword);
//                            $modelNewUser->auth_key = 'key';
//                            if($modelNewUser->save()){
//
//                                $social_links_facebook = '';
//                                if (isset($configData['facebook'])) {
//                                    $social_links_facebook = strip_tags($configData['facebook']);
//                                }
//                                else{
//                                }
//
//                                $social_links_twitter = '';
//                                if (isset($configData['twitter'])) {
//                                    $social_links_twitter = strip_tags($configData['twitter']);
//                                }
//                                else{
//                                }
//                                $social_links_linkedin = '';
//                                if (isset($configData['linkedin'])) {
//                                    $social_links_linkedin = strip_tags($configData['linkedin']);
//                                }
//                                else{
//                                }
//
//                                if (isset($configData['telephoneinstagram'])) {
//                                    $social_links_telephoneinstagram = strip_tags($configData['telephoneinstagram']);
//                                }
//                                else{
//                                }
//
//                            \Yii::$app->mailer->compose()
//                                    ->setFrom('repairs@phonerepairs4u.co.uk')->setFrom(['repairs@phonerepairs4u.co.uk' => 'Phone Repairs 4u'])
//                                    ->setTo($contact['email'])
//                                    ->setSubject('Phone Repairs 4u')
//                                    ->setHtmlBody("    
//                                  <html>
//                                    <head>
//                                            <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
//                                    </head>
//                                         <body>
//                                           <div style='text-align: center'>
//                                                <div style='display: inline-block; width: 50%; text-align: right'>
//                                                        <ul>
//                                                            <li style='list-style: none; display: inline-block'>
//                                                                <a href='".$social_links_facebook."'>
//                                                                    <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
//                                                                </a>
//                                                            </li>
//                                                            <li style='list-style: none; display: inline-block'>
//                                                                <a href='".$social_links_twitter."'>
//                                                                    <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
//                                                                </a>
//                                                            </li>
//                                                            <li style='list-style: none; display: inline-block'>
//                                                                <a  href='".$social_links_linkedin."'>
//                                                                    <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
//                                                                </a>
//                                                            </li>
//                                                            <li style='list-style: none; display: inline-block'>
//                                                                <a href='".$social_links_telephoneinstagram."'>
//                                                                    <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
//                                                                </a>
//                                                        </ul>
//                                                    </div>
//                                                </div>
//                                            <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
//                                                <div style='text-align: center'>
//                                                <div style='text-align: right; display: inline-block'>
//                                                </div>
//                                                <div style='display: inline-block; text-align: left'>
//                                                    <p style='font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 16px;'>
//                                                        <i>Hello Friends, We have updated our brand new Phones Repair 4U website!!<br>
//                                                            The site has many new features primarily we have a tracking service, <br> so you know what the status of your device at all times!! 
//                                                            </i><br>
//                                                            <br>
//                                                                <i>Have a look at the website and feel free to give feedback,<br> thank you for your loyal continued support.</i>
//                                                            <br>
//                                                            <br>
//                                                                <i>Enjoy the new web experience.</i>
//                                                            <br>
//                                                            <br>
//                                                            <i>Klodian Gica</i>
//                                                            <br>
//                                                            <i>Director</i>
//                                                        <br>
//                                                        <br>
//                                                        http://phonerepairs4u.co.uk
//                                                        <br>
//                                                        <br>
//                                                        <b>Username:</b> " . $contact['email'] . "
//                                                         <br>
//                                                        <b>Password:</b> " . $userpassword . "
//                                                    </p>
//                                                </div>
//                                                </div>
//                                                <div style='text-align: center'>
//                                                    <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>http://phonerepairs4u.co.uk/<br>
//                                                    020 3302 2930 </a>
//                                                </div>
//                                                <div>
//                                                    <p style='font-size:12px; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;'> If you’d like to unsubscribe and stop receiving these emails <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>click here.</a></div>
//                                            </body>
//                                        </html>
//                                       ")
//                                    ->send();
//                            }
//                    }
//                 
//            }
//        }
//    }
    
}
