<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Leads;
use app\models\Models;
use app\models\Repairs;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Setting;
use app\models\Slider;
use app\models\Pages;
use app\models\Service;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use app\models\Modelsservice;

class PagesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionPage($page_slug)
    {
        $modelPage = Pages::find()->where(['slug' => $page_slug])->one();
        $configData = \yii\helpers\ArrayHelper::map(Setting::find()->all(), 'key', 'value');
        if($modelPage){
            if($modelPage->type != 1){                
                return $this->render('page',[
                    'modelPage' => $modelPage,
                ]); 
            }else{
                switch ($modelPage->id) {
                    case 1:
                        
                                $modelNewLeads = new Leads();
                                $modelNewLeads->scenario = 'booking_service_page';
                        
                                if($_POST){
                                    if($modelNewLeads->load(Yii::$app->request->post())){
                                        if($modelNewLeads->save()){
                                            
                                            $modelNewLeads->scenario = 'add_identification_id';
                                            $modelNewLeads->identification_id = $modelNewLeads->key.$modelNewLeads->id;
                                            $modelNewLeads->save();
                    
                                            $wordsLeads = 'BK';
                                            $useremail = '';
                                            $user_signup = '';
                                            $userpassword = '';
                                            if(Yii::$app->user->isGuest){
                                                $modelUser = User::find()->where(['email' => $modelNewLeads->email])->one();
                                                if(!$modelUser){
                                                        $userpassword = substr(str_shuffle(str_repeat("0123456789abcdefghij!_klmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
                                                        $modelNewUser = new User();
                                                        $modelNewUser->scenario = 'signup_t';
                                                        $modelNewUser->name = $modelNewLeads->name;
                                                        $modelNewUser->email = $modelNewLeads->email;
                                                        $modelNewUser->password_hash = \Yii::$app->security->generatePasswordHash($userpassword);
                                                        $modelNewUser->auth_key = 'key';
                                                        if($modelNewUser->save()){
                                                            $modelNewLeads->scenario = 'add_user';
                                                            $modelNewLeads->user_id = $modelNewUser->id;
                                                            $modelNewLeads->save();
                                                            $user_signup = 'Please keep Password Secure, this is uniquely generated and will only be issued once <br>
                                                            Future password changes can be made in your PhoneRepairs4u profile page';
                                                        }
                                                }else{
                                                    $modelNewLeads->scenario = 'add_user';
                                                    $modelNewLeads->user_id = $modelUser->id;
                                                    $modelNewLeads->save();
                                                }

                                                $useremail = $modelNewLeads->email;
                                            }else{
                            //                    var_dump($modelNewLeads->email);exit;
                                                $modelNewLeads->scenario = 'add_user';
                                                $modelNewLeads->user_id = \Yii::$app->user->id;
                                                $modelNewLeads->save();

                                                $modelNewLeads->user_id = \Yii::$app->user->id;
                                                $useremail = $modelNewLeads->email;
                                            }
                                            
                                            $social_links_facebook = '';
                                            if (isset($configData['facebook'])) {
                                                $social_links_facebook = strip_tags($configData['facebook']);
                                            }
                                            else{
                                            }

                                            $social_links_twitter= '';
                                            if (isset($configData['twitter'])) {
                                                $social_links_twitter = strip_tags($configData['twitter']);
                                            }
                                            else{
                                            }

                                            $social_links_linkedin = '';
                                            if (isset($configData['linkedin'])) {
                                                $social_links_linkedin = strip_tags($configData['linkedin']);
                                            }
                                            else{
                                            }

                                            $social_links_telephoneinstagram = '';
                                            if (isset($configData['telephoneinstagram'])) {
                                                $social_links_telephoneinstagram = strip_tags($configData['telephoneinstagram']);
                                            }
                                            else{
                                            }
                                            $userpassword_2 ="<b>Password: </b>".$userpassword;
                                            if(\Yii::$app->mailer->compose()
                                                ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phonerepairs'])
                                                ->setTo($modelNewLeads->email)
                                                ->setSubject('Thanks for contacting us. Reference: ('.$wordsLeads.$modelNewLeads->id.')')
                                                ->setHtmlBody("
                                                    <div style='text-align: center'>
                                                    <div style='display: inline-block; width: 50%; text-align: right'>
                                                            <ul>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a href='".$social_links_facebook."'>
                                                                        <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                    </a>
                                                                </li>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a href='".$social_links_twitter."'>
                                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                    </a>
                                                                </li>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a  href='".$social_links_linkedin."'>
                                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                    </a>
                                                                </li>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a href='".$social_links_telephoneinstagram."'>
                                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                    </a>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                    <div style='text-align: center'>
                                                    <div style='text-align: right; display: inline-block'>
                                                    </div>
                                                    <div style='display: inline-block; text-align: left'>
                                                        <p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'>
                                                        Dear " . $modelNewLeads->name . ",
                                                        <br>
                                                        <br>
                                                        Thank you for contacting us, we have received your enquiry and will be
                                                        <br>
                                                        replying to you within the next hour.
                                                        <br>
                                                        <br>
                                                        Your unique reference number is: <b>EN" . $modelNewLeads->id . "</b>.<br>
                                                        It's advised you keep this number in a safe place as if you choose to walk into
                                                        <br>
                                                        one of our repair centres, it will help us to keep you up to date on the progress of your repair.
                                                        <br>
                                                        <br>
                                                        <b>Discount on repairs</b>
                                                        <br>
                                                        <br>
                                                        We would like to inform you about our Special Deal.
                                                        <br>
                                                        We are now giving a <b>£ 5 discount</b> on all the repairs when you make <a style='color:blue'>a booking
                                                            online.</a>
                                                        <br>
                                                        <br>
                                                        Kind Regards,
                                                        <br>
                                                        The Team at PhoneRepairs4u.co.uk
                                                        <br>
                                                        You can check status of your booking in your profile.
                                                        <br>
                                                        https://www.phonesrepair4u.com/login
                                                        <br>
                                                        <br>
                                                        <b>Username:</b> " . $useremail . "
                                                        <br>
                                                        " . $userpassword_2 . "
                                                        <br>
                                                        " . $user_signup . "
                                                        <br>
                                                        <b>Phone:</b> " . $modelNewLeads->phone_number . "
                                                        <br>
                                                        <b>Device color:</b> " . $modelNewLeads->device_color . "
                                                        </p>
                                                    </div>
                                                    </div>
                                                    <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                    <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                    <div><p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'> If you’d like to unsubscribe and stop receiving these emails <a href='#' style='text-decoration: none'>click here.</a></div>

                                                    ")
                                                ->send()){
                                                    Yii::$app->session->setFlash('form_sended');
                                                }else{
                                                    Yii::$app->session->setFlash('form__not_sended');                            
                                                } 
                                                
                                                
                                                
                                                
                                                
                                                 $usersAdmin = User::find()->where(['type' => 'admin'])->asArray()->all();
                                                    if($usersAdmin){
                                                        foreach($usersAdmin as $adminu){
                                                            if (filter_var($adminu['email'], FILTER_VALIDATE_EMAIL)) {
                                                                  if (\Yii::$app->mailer->compose()
                                                                    ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                                                                    ->setTo($adminu['email'])
                                                                    ->setSubject('New Reference: (' .$wordsLeads. $modelNewLeads->id . ')')
                                                                    ->setHtmlBody("    
                                                                                <html>
                                                                                  <head>
                                                                                          <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                                                                                  </head>
                                                                                       <body>
                                                                                         <div style='text-align: center'>
                                                                                              <div style='display: inline-block; width: 50%; text-align: right'>
                                                                                                      <ul>
                                                                                                          <li style='list-style: none; display: inline-block'>
                                                                                                              <a href='".$social_links_facebook."'>
                                                                                                                  <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                                                              </a>
                                                                                                          </li>
                                                                                                          <li style='list-style: none; display: inline-block'>
                                                                                                              <a href='".$social_links_twitter."'>
                                                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                                                              </a>
                                                                                                          </li>
                                                                                                          <li style='list-style: none; display: inline-block'>
                                                                                                              <a  href='".$social_links_linkedin."'>
                                                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                                                              </a>
                                                                                                          </li>
                                                                                                          <li style='list-style: none; display: inline-block'>
                                                                                                              <a href='".$social_links_telephoneinstagram."'>
                                                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                                                              </a>
                                                                                                      </ul>
                                                                                                  </div>
                                                                                              </div>
                                                                                          <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                                                              <div style='text-align: center'>
                                                                                              <div style='text-align: right; display: inline-block'>
                                                                                              </div>
                                                                                              <div style='display: inline-block; text-align: left'>
                                                                                                    <h2>New reference number: <b>EN" . $modelNewLeads->id . "</b></h2><br>
                                                                                                    <table border='1'>
                                                                                                        <tr>
                                                                                                            <td>Name</td>
                                                                                                            <td>".$modelNewLeads->name."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Phone number</td>
                                                                                                            <td>".$modelNewLeads->phone_number."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Email</td>
                                                                                                            <td>".$modelNewLeads->email."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Device color</td>
                                                                                                            <td>".$modelNewLeads->device_color."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Booking date</td>
                                                                                                            <td>".$modelNewLeads->booking_date."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Device model</td>
                                                                                                            <td>".$modelNewLeads->device_model."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Device fault</td>
                                                                                                            <td>".$modelNewLeads->device_fault."</td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                              </div>
                                                                                              </div>
                                                                                              <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                                                              <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                                                          </body>
                                                                                      </html>
                                                                                     ")
                                                                                  ->send()
                                                                              ){}

                                                            }
                                                        }
                                                    }

                                                //admin email end
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                $modelNewLeads = new Leads();
                                                $modelNewLeads->scenario = 'booking_service_page';
                                        }
                                    }
                                }

                                //$branchArray = \yii\helpers\ArrayHelper::map(\app\models\Branches::find()->all(), 'id', 'address_street');
                                $branchArray = [];
                                foreach(\app\models\Branches::find()->all() as $branch){
                                    $branchArray[$branch['id']] = strip_tags($branch['address_street']);                                    
                                }
                                return $this->render('book',[
                                    'modelNewLeads' => $modelNewLeads,
                                    'modelPage' => $modelPage,
                                    'branchArray' => $branchArray,
                                ]); 
                    case 2:
                       
                                $modelNewLeads = new Leads();
                                $modelNewLeads->scenario = 'mail_page';
                        
                                if($_POST){
                                    if($modelNewLeads->load(Yii::$app->request->post())){
                                        if($modelNewLeads->save()){
                                            
                                            $modelNewLeads->scenario = 'add_identification_id';
                                            $modelNewLeads->identification_id = $modelNewLeads->key.$modelNewLeads->id;
                                            $modelNewLeads->save();
                                            
                                            $wordsLeads = 'ML';
                                            $useremail = '';
                                            $userpassword = '';
                                            $user_signup = '';
                                            if(Yii::$app->user->isGuest){
                                                $modelUser = User::find()->where(['email' => $modelNewLeads->email])->one();
                                                if(!$modelUser){
                                                        $userpassword = substr(str_shuffle(str_repeat("0123456789abcdefghij!_klmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
                                                        $modelNewUser = new User();
                                                        $modelNewUser->scenario = 'signup_t';
                                                        $modelNewUser->name = $modelNewLeads->name;
                                                        $modelNewUser->email = $modelNewLeads->email;
                                                        $modelNewUser->password_hash = \Yii::$app->security->generatePasswordHash($userpassword);
                                                        $modelNewUser->auth_key = 'key';
                                                        if($modelNewUser->save()){
                                                            $modelNewLeads->scenario = 'add_user';
                                                            $modelNewLeads->user_id = $modelNewUser->id;
                                                            $modelNewLeads->save();
                                                            $user_signup = 'Please keep Password Secure, this is uniquely generated and will only be issued once <br>
                                                            Future password changes can be made in your PhoneRepairs4u profile page';
                                                        }
                                                }else{
                                                    $modelNewLeads->scenario = 'add_user';
                                                    $modelNewLeads->user_id = $modelUser->id;
                                                    $modelNewLeads->save();
                                                }

                                                $useremail = $modelNewLeads->email;
                                            }else{
                            //                    var_dump($modelNewLeads->email);exit;
                                                $modelNewLeads->scenario = 'add_user';
                                                $modelNewLeads->user_id = \Yii::$app->user->id;
                                                $modelNewLeads->save();

                                                $modelNewLeads->user_id = \Yii::$app->user->id;
                                                $useremail = $modelNewLeads->email;
                                            }
                                            
                                            $social_links_facebook = '';
                                            if (isset($configData['facebook'])) {
                                                $social_links_facebook = strip_tags($configData['facebook']);
                                            }
                                            else{
                                            }
                                            
                                            $social_links_twitter = '';
                                            if (isset($configData['twitter'])) {
                                                $social_links_twitter = strip_tags($configData['twitter']);
                                            }
                                            else{
                                            }
                                            
                                            $social_links_linkedin = '';
                                            if (isset($configData['linkedin'])) {
                                                $social_links_linkedin = strip_tags($configData['linkedin']);
                                            }
                                            else{
                                            }
                                            
                                            $social_links_telephoneinstagram = '';
                                            if (isset($configData['telephoneinstagram'])) {
                                                $social_links_telephoneinstagram = strip_tags($configData['telephoneinstagram']);
                                            }
                                            else{
                                            }


                                            $userpassword_2 ="<b>Password:</b> ".$userpassword;
                                            if(\Yii::$app->mailer->compose()
                                                ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phonerepairs'])
                                                ->setTo($modelNewLeads->email)
                                                ->setSubject('Thanks for contacting us. Reference: ('.$wordsLeads.$modelNewLeads->id.')')
                                                ->setHtmlBody( "
                                                    <div style='text-align: center'>
                                                    <div style='display: inline-block; width: 50%; text-align: right'>
                                                            <ul>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a href='".$social_links_facebook."'>
                                                                        <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                    </a>
                                                                </li>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a href='".$social_links_twitter."'>
                                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                    </a>
                                                                </li>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a  href='".$social_links_linkedin."'>
                                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                    </a>
                                                                </li>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a href='".$social_links_telephoneinstagram."'>
                                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                    </a>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                    <div style='text-align: center'>
                                                    <div style='text-align: right; display: inline-block'>
                                                    </div>
                                                    <div style='display: inline-block; text-align: left'>
                                                        <p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'>
                                                        Dear " . $modelNewLeads->name . ",
                                                        <br>
                                                        <br>
                                                        Thank you for contacting us, we have received your enquiry and will be
                                                        <br>
                                                        replying to you within the next hour.
                                                        <br>
                                                        <br>
                                                        Your unique reference number is: <b>ML" . $modelNewLeads->id . "</b>.<br>
                                                        It's advised you keep this number in a safe place as if you choose to walk into
                                                        <br>
                                                        one of our repair centres, it will help us to keep you up to date on the progress of your repair.
                                                        <br>
                                                        <br>
                                                        <b>Discount on repairs</b>
                                                        <br>
                                                        <br>
                                                        We would like to inform you about our Special Deal.
                                                        <br>
                                                        We are now giving a <b>£ 5 discount</b> on all the repairs when you make <a style='color:blue'>a booking
                                                            online.</a>
                                                        <br>
                                                        <br>
                                                        Kind Regards,
                                                        <br>
                                                        The Team at PhoneRepairs4u.co.uk
                                                        <br>
                                                        You can check status of your booking in your profile.
                                                        <br>
                                                        https://www.phonesrepair4u.com/login
                                                        <br>
                                                        <br>
                                                        <b>Username:</b> " . $useremail . "
                                                        <br>
                                                        " . $userpassword_2 . "
                                                        <br>
                                                        " . $user_signup . "
                                                        <br>
                                                        <br>
                                                        <b>Address:</b> " . $modelNewLeads->address . "
                                                        <br>
                                                        <b>Phone:</b> " . $modelNewLeads->phone_number . "
                                                        <br>
                                                        <b>Phone password:</b> " . $modelNewLeads->phone_password . "
                                                        <br>
                                                        <b>Device color:</b> " . $modelNewLeads->device_color . "
                                                        <br>
                                                        <b>Device model:</b> " . $modelNewLeads->device_model . "
                                                        <br>
                                                        <b>Quote:</b> " . $modelNewLeads->quote . "
                                                        <br>
                                                        <b>IMEI:</b> " . $modelNewLeads->IMEI . "
                                                        </p>
                                                    </div>
                                                    </div>
                                                    <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                    <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                    <div><p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'> If you’d like to unsubscribe and stop receiving these emails <a href='#' style='text-decoration: none'>click here.</a></div>

                                                    ")
                                                ->send()){
                                                    Yii::$app->session->setFlash('form_sended');
                                                }else{
                                                    Yii::$app->session->setFlash('form__not_sended');                            
                                                }
                                                
                                                
                                                //admin email begin
                                                $usersAdmin = User::find()->where(['type' => 'admin'])->asArray()->all();
                                                    if($usersAdmin){
                                                        foreach($usersAdmin as $adminu){
                                                            if (filter_var($adminu['email'], FILTER_VALIDATE_EMAIL)) {
                                                                  if (\Yii::$app->mailer->compose()
                                                                    ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                                                                    ->setTo($adminu['email'])
                                                                    ->setSubject('New Reference: (' .$wordsLeads. $modelNewLeads->id . ')')
                                                                    ->setHtmlBody("    
                                                                                <html>
                                                                                  <head>
                                                                                          <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                                                                                  </head>
                                                                                       <body>
                                                                                         <div style='text-align: center'>
                                                                                              <div style='display: inline-block; width: 50%; text-align: right'>
                                                                                                      <ul>
                                                                                                          <li style='list-style: none; display: inline-block'>
                                                                                                              <a href='".$social_links_facebook."'>
                                                                                                                  <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                                                              </a>
                                                                                                          </li>
                                                                                                          <li style='list-style: none; display: inline-block'>
                                                                                                              <a href='".$social_links_twitter."'>
                                                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                                                              </a>
                                                                                                          </li>
                                                                                                          <li style='list-style: none; display: inline-block'>
                                                                                                              <a  href='".$social_links_linkedin."'>
                                                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                                                              </a>
                                                                                                          </li>
                                                                                                          <li style='list-style: none; display: inline-block'>
                                                                                                              <a href='".$social_links_telephoneinstagram."'>
                                                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                                                              </a>
                                                                                                      </ul>
                                                                                                  </div>
                                                                                              </div>
                                                                                          <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                                                              <div style='text-align: center'>
                                                                                              <div style='text-align: right; display: inline-block'>
                                                                                              </div>
                                                                                              <div style='display: inline-block; text-align: left'>
                                                                                                    <h2>New reference number: <b>ML" . $modelNewLeads->id . "</b></h2><br>
                                                                                                    <table border='1'>
                                                                                                        <tr>
                                                                                                            <td>Name</td>
                                                                                                            <td>".$modelNewLeads->name."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Phone number</td>
                                                                                                            <td>".$modelNewLeads->phone_number."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Email</td>
                                                                                                            <td>".$modelNewLeads->email."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Device color</td>
                                                                                                            <td>".$modelNewLeads->device_color."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Booking date</td>
                                                                                                            <td>".$modelNewLeads->booking_date."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Device model</td>
                                                                                                            <td>".$modelNewLeads->device_model."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Device fault</td>
                                                                                                            <td>".$modelNewLeads->device_fault."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Description</td>
                                                                                                            <td>".$modelNewLeads->description."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>IMEI</td>
                                                                                                            <td>".$modelNewLeads->IMEI."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Phone password</td>
                                                                                                            <td>".$modelNewLeads->phone_password."</td>
                                                                                                        </tr>
                                                                                                        <tr>
                                                                                                            <td>Address</td>
                                                                                                            <td>".$modelNewLeads->address."</td>
                                                                                                        </tr>
                                                                                                    </table>
                                                                                              </div>
                                                                                              </div>
                                                                                              <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                                                              <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                                                          </body>
                                                                                      </html>
                                                                                     ")
                                                                                  ->send()
                                                                              ){}

                                                            }
                                                        }
                                                    }

                                                //admin email end
                                                
                                                
                                                
                                                $modelNewLeads = new Leads();
                                                $modelNewLeads->scenario = 'booking_service_page';
                                        }
                                    }
                                }

                                $branchArray = \yii\helpers\ArrayHelper::map(\app\models\Branches::find()->all(), 'id', 'address_street');
                        
                                return $this->render('mail',[
                                    'modelNewLeads' => $modelNewLeads,
                                    'modelPage' => $modelPage,
                                    'branchArray' => $branchArray,
                                ]); 
                        
                    case 4:
                                $modelNewLeads = new Leads();
                                $modelNewLeads->scenario = 'corporate_page';
                        
                                if($_POST){
                                    if($modelNewLeads->load(Yii::$app->request->post())){
                                        if($modelNewLeads->save()){
                                            
                                            $modelNewLeads->scenario = 'add_identification_id';
                                            $modelNewLeads->identification_id = $modelNewLeads->key.$modelNewLeads->id;
                                            $modelNewLeads->save();

                                            $wordsLeads = 'CR';
                                            $user_signup = '';
                                            $useremail = '';
                                            $userpassword = '';
                                            if(Yii::$app->user->isGuest){
                                                $modelUser = User::find()->where(['email' => $modelNewLeads->email])->one();
                                                if(!$modelUser){
                                                        $userpassword = substr(str_shuffle(str_repeat("0123456789abcdefghij!_klmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
                                                        $modelNewUser = new User();
                                                        $modelNewUser->scenario = 'signup_t';
                                                        $modelNewUser->name = $modelNewLeads->name;
                                                        $modelNewUser->email = $modelNewLeads->email;
                                                        $modelNewUser->password_hash = \Yii::$app->security->generatePasswordHash($userpassword);
                                                        $modelNewUser->auth_key = 'key';
                                                        if($modelNewUser->save()){
                                                            $modelNewLeads->scenario = 'add_user';
                                                            $modelNewLeads->user_id = $modelNewUser->id;
                                                            $modelNewLeads->save();
                                                            $user_signup = 'Please keep Password Secure, this is uniquely generated and will only be issued once <br>
                                                            Future password changes can be made in your PhoneRepairs4u profile page';
                                                        }
                                                }else{
                                                    $modelNewLeads->scenario = 'add_user';
                                                    $modelNewLeads->user_id = $modelUser->id;
                                                    $modelNewLeads->save();
                                                }

                                                $useremail = $modelNewLeads->email;
                                            }else{
                            //                    var_dump($modelNewLeads->email);exit;
                                                $modelNewLeads->scenario = 'add_user';
                                                $modelNewLeads->user_id = \Yii::$app->user->id;
                                                $modelNewLeads->save();

                                                $modelNewLeads->user_id = \Yii::$app->user->id;
                                                $useremail = $modelNewLeads->email;
                                            }

                                            $social_links_facebook = '';
                                            if (isset($configData['facebook'])) {
                                                $social_links_facebook = strip_tags($configData['facebook']);
                                            }
                                            else{
                                            }

                                            $social_links_twitter = '';
                                            if (isset($configData['twitter'])) {
                                                $social_links_twitter = strip_tags($configData['twitter']);
                                            }
                                            else{
                                            }

                                            $social_links_linkedin = '';
                                            if (isset($configData['linkedin'])) {
                                                $social_links_linkedin = strip_tags($configData['linkedin']);
                                            }
                                            else{
                                            }

                                            $social_links_telephoneinstagram = '';
                                            if (isset($configData['telephoneinstagram'])) {
                                                $social_links_telephoneinstagram = strip_tags($configData['telephoneinstagram']);
                                            }
                                            else{
                                            }


                                            $userpassword_2 ="<b>Password:</b> ".$userpassword;
                                            if(\Yii::$app->mailer->compose()
                                                ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phonerepairs'])
                                                ->setTo($modelNewLeads->email)
                                                ->setSubject('Thanks for contacting us. Reference: ('.$wordsLeads.$modelNewLeads->id.')')
                                                ->setHtmlBody( "
                                                    <div style='text-align: center'>
                                                    <div style='display: inline-block; width: 50%; text-align: right'>
                                                            <ul>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a href='".$social_links_facebook."'>
                                                                        <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                    </a>
                                                                </li>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a href='".$social_links_twitter."'>
                                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                    </a>
                                                                </li>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a  href='".$social_links_linkedin."'>
                                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                    </a>
                                                                </li>
                                                                <li style='list-style: none; display: inline-block'>
                                                                    <a href='".$social_links_telephoneinstagram."'>
                                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                    </a>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                    <div style='text-align: center'>
                                                    <div style='text-align: right; display: inline-block'>
                                                    </div>
                                                    <div style='display: inline-block; text-align: left'>
                                                        <p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'>
                                                        Dear " . $modelNewLeads->name . ",
                                                        <br>
                                                        <br>
                                                        Thank you for contacting us, we have received your enquiry and will be
                                                        <br>
                                                        replying to you within the next hour.
                                                        <br>
                                                        <br>
                                                        Your unique reference number is: <b>CR" . $modelNewLeads->id . "</b>.<br>
                                                        It's advised you keep this number in a safe place as if you choose to walk into
                                                        <br>
                                                        one of our repair centres, it will help us to keep you up to date on the progress of your repair.
                                                        <br>
                                                        <br>
                                                        <b>Discount on repairs</b>
                                                        <br>
                                                        <br>
                                                        We would like to inform you about our Special Deal.
                                                        <br>
                                                        We are now giving a <b>£ 5 discount</b> on all the repairs when you make <a style='color:blue'>a booking
                                                            online.</a>
                                                        <br>
                                                        <br>
                                                        Kind Regards,
                                                        <br>
                                                        The Team at PhoneRepairs4u.co.uk
                                                        <br>
                                                        You can check status of your booking in your profile.
                                                        <br>
                                                        https://www.phonesrepair4u.com/login
                                                        <br>
                                                        <br>
                                                        <b>Username:</b> " . $useremail . "
                                                        <br>
                                                        " . $userpassword_2 . "
                                                        <br>
                                                        " . $user_signup . "
                                                        <br>
                                                        <br>
                                                        <b>Company name:</b> " . $modelNewLeads->company_r_name . "
                                                        <br>
                                                        <b>Phone:</b> " . $modelNewLeads->phone_number . "
                                                        <br>
                                                        <b>Company url:</b> " . $modelNewLeads->company_url . "
                                                        <br>
                                                        <b>Description:</b> " . $modelNewLeads->description . "
                                                        <br>
                                                        </p>
                                                    </div>
                                                    </div>
                                                    <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                    <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk<br> 020 3302 2930 </a></div>
                                                    <div><p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'> If you’d like to unsubscribe and stop receiving these emails <a href='#' style='text-decoration: none'>click here.</a></div>

                                                    ")
                                                ->send()){
                                                    Yii::$app->session->setFlash('form_sended');
                                                }else{
                                                    Yii::$app->session->setFlash('form__not_sended');                            
                                                } 
                                                
                                                
                                                
                                                
                                                
                                                //admin email begin
                                                        $usersAdmin = User::find()->where(['type' => 'admin'])->asArray()->all();
                                                            if($usersAdmin){
                                                                foreach($usersAdmin as $adminu){
                                                                    if (filter_var($adminu['email'], FILTER_VALIDATE_EMAIL)) {
                                                                          if (\Yii::$app->mailer->compose()
                                                                            ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                                                                            ->setTo($adminu['email'])
                                                                            ->setSubject('New Reference: (' .$wordsLeads. $modelNewLeads->id . ')')
                                                                            ->setHtmlBody("    
                                                                                        <html>
                                                                                          <head>
                                                                                                  <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                                                                                          </head>
                                                                                               <body>
                                                                                                 <div style='text-align: center'>
                                                                                                      <div style='display: inline-block; width: 50%; text-align: right'>
                                                                                                              <ul>
                                                                                                                  <li style='list-style: none; display: inline-block'>
                                                                                                                      <a href='".$social_links_facebook."'>
                                                                                                                          <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                                                                      </a>
                                                                                                                  </li>
                                                                                                                  <li style='list-style: none; display: inline-block'>
                                                                                                                      <a href='".$social_links_twitter."'>
                                                                                                                          <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                                                                      </a>
                                                                                                                  </li>
                                                                                                                  <li style='list-style: none; display: inline-block'>
                                                                                                                      <a  href='".$social_links_linkedin."'>
                                                                                                                          <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                                                                      </a>
                                                                                                                  </li>
                                                                                                                  <li style='list-style: none; display: inline-block'>
                                                                                                                      <a href='".$social_links_telephoneinstagram."'>
                                                                                                                          <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                                                                      </a>
                                                                                                              </ul>
                                                                                                          </div>
                                                                                                      </div>
                                                                                                  <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                                                                      <div style='text-align: center'>
                                                                                                      <div style='text-align: right; display: inline-block'>
                                                                                                      </div>
                                                                                                      <div style='display: inline-block; text-align: left'>
                                                                                                            <h2>New reference number: <b>CR" . $modelNewLeads->id . "</b></h2><br>
                                                                                                            <table border='1'>
                                                                                                                <tr>
                                                                                                                    <td>Company name</td>
                                                                                                                    <td>".$modelNewLeads->name."</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>Company Representative Name</td>
                                                                                                                    <td>".$modelNewLeads->company_r_name."</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>Company Contact No.</td>
                                                                                                                    <td>".$modelNewLeads->phone_number."</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>Company Email</td>
                                                                                                                    <td>".$modelNewLeads->email."</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>Company URL</td>
                                                                                                                    <td>".$modelNewLeads->company_url."</td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td>Additional Info</td>
                                                                                                                    <td>".$modelNewLeads->description."</td>
                                                                                                                </tr>                                                                                                                
                                                                                                            </table>
                                                                                                      </div>
                                                                                                      </div>
                                                                                                      <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                                                                      <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                                                                  </body>
                                                                                              </html>
                                                                                             ")
                                                                                          ->send()
                                                                                      ){}

                                                                    }
                                                                }
                                                            }

                                                        //admin email end
                                                
                                                
                                                
                                                
                                                
                                                $modelNewLeads = new Leads();
                                                $modelNewLeads->scenario = 'booking_service_page';
                                        }
                                    }
                                }

                        
                                return $this->render('corporate',[
                                    'modelNewLeads' => $modelNewLeads,
                                    'modelPage' => $modelPage,
                                ]); 
                    case 3:
                    case 5:
                        $modelBranches = \app\models\Branches::find()->all();
                        
                        return $this->render('branches',[
                            'modelBranches' => $modelBranches,
                            'modelPage' => $modelPage,
                        ]); 
                }
                
            }
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }    

}
