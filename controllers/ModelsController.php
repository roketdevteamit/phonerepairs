<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\Leads;
use app\models\Models;
use app\models\Repairs;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Setting;
use app\models\Slider;
use app\models\Service;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use app\models\Modelsservice;

class ModelsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'eauth' => [
                // required to disable csrf validation on OpenID requests
                'class' => \nodge\eauth\openid\ControllerBehavior::className(),
                'only' => ['login'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
       
    }
    
    public function actionRepairs($repair_slug)
    {
        
        $modelRepair = Repairs::find()->where(['slug' => $repair_slug])->one();
        if($modelRepair){
//            $queryModels = Models::find()->where(['repairs_id' => $modelRepair->id]);
            $queryModels = new Query();
            $queryModels->select(['*', 'id' => '{{models}}.id', 'name' => '{{models}}.name', 'slug' => '{{models}}.slug' , 'img_src' => '{{models}}.img_src'])
                ->from('{{models}}')
                ->join('LEFT JOIN',
                    '{{brand}}',
                    '{{brand}}.id = {{models}}.brand_id')->where(['{{models}}.repairs_id' => $modelRepair->id]);
            if($_GET){
                if(isset($_GET['models_name'])){
                    if($_GET['models_name'] != ''){
                        $queryModels->andWhere(['like', '{{models}}.name', $_GET['models_name']]);
                    }
                }
                if(isset($_GET['models_brand'])){
                    if($_GET['models_brand'] != ''){
                        $queryModels->andWhere(['{{models}}.brand_id' => $_GET['models_brand']]);
                    }
                }
            }
            $queryModels->orderBy(['{{brand}}.sort' => SORT_ASC, '{{models}}.name' => SORT_DESC]);
            $modelModels = new ActiveDataProvider(['query' => $queryModels, 'pagination' => ['pageSize' => 24]]);
            
            
            $queryBrands = new Query();
            $queryBrands->select(['*', 'id' => '{{brand}}.id'])
                ->from('{{brand_repairs}}')
                ->join('LEFT JOIN',
                    '{{brand}}',
                    '{{brand}}.id = {{brand_repairs}}.brand_id')
                ->where(['{{brand_repairs}}.repairs_id' => $modelRepair->id])->orderBy('sort ASC');
            
            $command = $queryBrands->createCommand();
            $modelBrands = $command->queryAll();
            $arrayBrands = \yii\helpers\ArrayHelper::map($modelBrands, 'id', 'name');
            
            return $this->render('allmodels',[
                'modelRepair' => $modelRepair,
                'arrayBrands' => $arrayBrands,
                'modelModels' => $modelModels->getModels(),
                'pagination' => $modelModels->pagination,
                'count' => $modelModels->pagination->totalCount,
            ]); 
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionSearch()
    {
        if(isset($_GET['search'])){
            $search_value = $_GET['search'];
        }else{
            $search_value = '';
        }
        
        $queryModels = new Query();
        $queryModels->select(['*', 'id' => '{{models}}.id',
            'name' => '{{models}}.name', 'repairs_slug' => '{{repairs}}.slug',
            'img_src' => '{{models}}.img_src', 'models_slug' => '{{models}}.slug'])
                        ->from('{{models}}')
                        ->join('LEFT JOIN',
                                    '{{repairs}}',
                                    '{{repairs}}.id = {{models}}.repairs_id')
                ->where(['like', '{{models}}.name', $search_value]);

        $modelModels = new ActiveDataProvider(['query' => $queryModels, 'pagination' => ['pageSize' => 20]]);
        
        return $this->render('search',[
            'search_value' => $search_value,
            'modelModels' => $modelModels->getModels(),
            'pagination' => $modelModels->pagination,
            'count' => $modelModels->pagination->totalCount,
        ]); 
    }
    
    public function actionModels($repair_slug,$model_slug)
    {
        $modelRepair = Repairs::find()->where(['slug' => $repair_slug])->one();
        $modelModel = Models::find()->where(['slug' => $model_slug])->one();        
        if($modelRepair && $modelModel){
            
            $queryService = new Query();
            $queryService->select(['*', 'id' => '{{models_service}}.id'])
                            ->from('{{models_service}}')
                            ->join('LEFT JOIN',
                                        '{{service}}',
                                        '{{service}}.id = {{models_service}}.service_id')
                    ->where(['{{models_service}}.models_id' => $modelModel->id]);
            $command = $queryService->createCommand();
            $modelServices = $command->queryAll();
            
            return $this->render('models',[
                'modelModel' => $modelModel,
                'modelRepair' => $modelRepair,
                'modelServices' => $modelServices,
            ]); 
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }

    public function actionService($repair_slug, $model_slug, $service_slug)
    {
        $modelRepair = Repairs::find()->where(['slug' => $repair_slug])->one();
        $modelModel = Models::find()->where(['slug' => $model_slug])->one();        
        $modelService = Service::find()->where(['slug' => $service_slug])->one();        
        $modelModelsService = Modelsservice::find()->where(['service_id' => $modelService->id,'models_id' => $modelModel->id])->one();        
        if($modelRepair && $modelModel && $modelService){
            $modelNewLeads = new Leads();
            $modelNewLeads->scenario = 'booking_book_page';
            
//            $modelNewMail = new Leads();
//            $modelNewMail->scenario = 'mail_service_page';
            
            if($_POST){
                if($modelNewLeads->load(Yii::$app->request->post())){
                    $modelNewLeads->status = 1;
                    if($modelNewLeads->save()){
                        
                        $configData = \yii\helpers\ArrayHelper::map(Setting::find()->all(), 'key', 'value');
                        $wordsLeads = 'BK';
                        if($modelNewLeads->leads_type == 1){
                            $wordsLeads = 'ML';                            
                        }
                        $user_signup = '';
                        $useremail = '';
                        $userpassword = '';
                        if(Yii::$app->user->isGuest){
                            $modelUser = User::find()->where(['email' => $modelNewLeads->email])->one();
                            if(!$modelUser){
                                    $userpassword = substr(str_shuffle(str_repeat("0123456789abcdefghij!_klmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
                                    $modelNewUser = new User();
                                    $modelNewUser->scenario = 'signup_t';
                                    $modelNewUser->name = $modelNewLeads->name;
                                    $modelNewUser->email = $modelNewLeads->email;
                                    $modelNewUser->password_hash = \Yii::$app->security->generatePasswordHash($userpassword);
                                    $modelNewUser->auth_key = 'key';
                                    if($modelNewUser->save()){
                                        $modelNewLeads->scenario = 'add_user';
                                        $modelNewLeads->user_id = $modelNewUser->id;
                                        $modelNewLeads->save();
                                        $user_signup = 'Please keep Password Secure, this is uniquely generated and will only be issued once <br>
                                            Future password changes can be made in your PhoneRepairs4u profile page';
                                    }
                            }else{
                                $modelNewLeads->scenario = 'add_user';
                                $modelNewLeads->user_id = $modelUser->id;
                                $modelNewLeads->save();
                            }

                            $useremail = $modelNewLeads->email;
                        }else{
        //                    var_dump($modelNewLeads->email);exit;
                            $modelNewLeads->scenario = 'add_user';
                            $modelNewLeads->user_id = \Yii::$app->user->id;
                            $modelNewLeads->save();

                            $modelNewLeads->user_id = \Yii::$app->user->id;
                            $useremail = $modelNewLeads->email;
                        }
                        
                        $social_links_facebook = '';
                        if (isset($configData['facebook'])) {
                            $social_links_facebook = strip_tags($configData['facebook']);
                        }
                        else{
                        }

                        $social_links_twitter = '';
                        if (isset($configData['twitter'])) {
                            $social_links_twitter = strip_tags($configData['twitter']);
                        }
                        else{
                        }
                        $social_links_linkedin = '';
                        if (isset($configData['linkedin'])) {
                            $social_links_linkedin = strip_tags($configData['linkedin']);
                        }
                        else{
                        }

                        if (isset($configData['telephoneinstagram'])) {
                            $social_links_telephoneinstagram = strip_tags($configData['telephoneinstagram']);
                        }
                        else{
                        }
                        
                        if(\Yii::$app->mailer->compose()
                            ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phonerepairs'])
                            ->setTo($modelNewLeads->email)
                            ->setSubject('Thanks for contacting us. Reference: ('.$wordsLeads.$modelNewLeads->id.')')
                            ->setHtmlBody("
                                <html>
                        <head>
                                <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                        </head>
                             <body>
                               <div style='text-align: center'>
                                    <div style='display: inline-block; width: 50%; text-align: right'>
                                            <ul>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a href='".$social_links_facebook."'>
                                                        <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                    </a>
                                                </li>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a href='".$social_links_twitter."'>
                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                    </a>
                                                </li>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a  href='".$social_links_linkedin."'>
                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                    </a>
                                                </li>
                                                <li style='list-style: none; display: inline-block'>
                                                    <a href='".$social_links_telephoneinstagram."'>
                                                        <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                    </a>
                                            </ul>
                                        </div>
                                    </div>
                                <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                    <div style='text-align: center'>
                                    <div style='text-align: right; display: inline-block'>
                                    </div>
                                    <div style='display: inline-block; text-align: left'>
                                        <p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'>
                                        Dear " . $modelNewLeads->name . ",
                                        <br>
                                        <br>
                                        Thank you for contacting us, we have received your enquiry and will be
                                        <br>
                                        replying to you within the next hour.
                                        <br>
                                        <br>
                                        Your unique reference number is: <b>" .$wordsLeads. $modelNewLeads->id . "</b>.<br>
                                        It's advised you keep this number in a safe place as if you choose to walk into
                                        <br>
                                        one of our repair centres, it will help us to keep you up to date on the progress of your repair.
                                        <br>
                                        <br>
                                        <b>Discount on repairs</b>
                                        <br>
                                        <br>
                                        We would like to inform you about our Special Deal.
                                        <br>
                                        We are now giving a <b>£ 5 discount</b> on all the repairs when you make <a style='color:blue'>a booking
                                            online.</a>
                                        <br>
                                        <br>
                                        Kind Regards,
                                        <br>
                                        The Team at PhoneRepairs4u.co.uk
                                        <br>
                                        You can check status of your booking in your profile.
                                        <br>
                                        https://www.phonesrepair4u.com/login
                                        <br>
                                        <br>
                                        <b>Username:</b> " . $useremail . "
                                         <br>
                                        " . $userpassword_2 . "
                                         <br>
                                        " . $user_signup . "
                                        </p>
                                    </div>
                                    </div>
                                    <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                    <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                    <div><p style='sans-serif; font-style: normal;color: #222222;text-rendering: optimizeLegibility; margin-top: 0.2rem;margin-bottom: 0.5rem;font-size: 18px;'> If you’d like to unsubscribe and stop receiving these emails <a href='#' style='text-decoration: none'>click here.</a></div>
                                </body>
                            </html>
                           ")
                            ->send()){
                                Yii::$app->session->setFlash('form_sended');
                            }else{
                                Yii::$app->session->setFlash('form__not_sended');                            
                            } 
                            
                            
                            
                            
                            
                            //admin email begin
                                
                                $usersAdmin = User::find()->where(['type' => 'admin'])->asArray()->all();
                                if($usersAdmin){
                                    foreach($usersAdmin as $adminu){
                                        if (filter_var($adminu['email'], FILTER_VALIDATE_EMAIL)) {
                                              if (\Yii::$app->mailer->compose()
                                                ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                                                ->setTo($adminu['email'])
                                                ->setSubject('New Reference: (' .$wordsLeads. $modelNewLeads->id . ')')
                                                ->setHtmlBody("    
                                                            <html>
                                                              <head>
                                                                      <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                                                              </head>
                                                                   <body>
                                                                     <div style='text-align: center'>
                                                                          <div style='display: inline-block; width: 50%; text-align: right'>
                                                                                  <ul>
                                                                                      <li style='list-style: none; display: inline-block'>
                                                                                          <a href='".$social_links_facebook."'>
                                                                                              <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                                          </a>
                                                                                      </li>
                                                                                      <li style='list-style: none; display: inline-block'>
                                                                                          <a href='".$social_links_twitter."'>
                                                                                              <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                                          </a>
                                                                                      </li>
                                                                                      <li style='list-style: none; display: inline-block'>
                                                                                          <a  href='".$social_links_linkedin."'>
                                                                                              <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                                          </a>
                                                                                      </li>
                                                                                      <li style='list-style: none; display: inline-block'>
                                                                                          <a href='".$social_links_telephoneinstagram."'>
                                                                                              <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                                          </a>
                                                                                  </ul>
                                                                              </div>
                                                                          </div>
                                                                      <div style='text-align: center'><img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                                          <div style='text-align: center'>
                                                                          <div style='text-align: right; display: inline-block'>
                                                                          </div>
                                                                          <div style='display: inline-block; text-align: left'>
                                                                                <h2>New reference number: <b>EH" . $modelNewLeads->id . "</b></h2><br>
                                                                                <table border='1'>
                                                                                    <tr>
                                                                                        <td>Name</td>
                                                                                        <td>".$modelNewLeads->name."</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Phone number</td>
                                                                                        <td>".$modelNewLeads->phone_number."</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Email</td>
                                                                                        <td>".$modelNewLeads->email."</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Device color</td>
                                                                                        <td>".$modelNewLeads->device_color."</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Booking date</td>
                                                                                        <td>".$modelNewLeads->booking_date."</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Full address</td>
                                                                                        <td>".$modelNewLeads->address."</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Description</td>
                                                                                        <td>".$modelNewLeads->description."</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Model</td>
                                                                                        <td>".$modelModel->name."</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Service</td>
                                                                                        <td>".$modelService->name."</td>
                                                                                    </tr>
                                                                                </table>
                                                                          </div>
                                                                          </div>
                                                                          <div style='display: inline-block; text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                                          <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                                      </body>
                                                                  </html>
                                                                 ")
                                                              ->send()
                                                          ){}

                                        }
                                    }
                                }

                            //admin email end
                            
                            
                            
                            
                            
                            
                            $modelNewLeads = new Leads();
                            $modelNewLeads->scenario = 'booking_service_page';
                    }
//                if(isset($_POST['mailButton'])){
//                    echo 'mail';
//                    exit;
//                }
            }
            }
            
            $branchArray = [];
            foreach(\app\models\Branches::find()->all() as $branch){
                $branchArray[$branch['id']] = strip_tags($branch['address_street']);                                    
            }
            return $this->render('service',[
                'modelModel' => $modelModel,
                'modelRepair' => $modelRepair,
                'modelService' => $modelService,
                'modelModelsService' => $modelModelsService,
                
                'modelNewLeads' => $modelNewLeads,
                'branchArray' => $branchArray,
                
//                'modelNewMail' => $modelNewMail,
            ]); 
            
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    

}
