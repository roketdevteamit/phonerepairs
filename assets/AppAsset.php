<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */


class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/settings.css',
        'css/layers.css',
        'css/navigation.css',
        'css/animate.css',
        'css/carousel/owl.carousel.css',
        'css/carousel/owl.theme.default.css',
        'css/font-awesome.css',
        'css/dropzone.css',
        'css/fonts.css',
        'css/footer.css',
        'css/body.css',
        'css/admin_panel_style.css',
        'css/site.css',
    ];
    public $js = [      
        'js/jquery.themepunch.tools.min.js?rev=5.0',
        'js/jquery.themepunch.revolution.min.js?rev=5.0',
        'js/extensions/revolution.extension.slideanims.min.js',
        'js/extensions/revolution.extension.layeranimation.min.js',
        'js/extensions/revolution.extension.navigation.min.js',
        'js/extensions/revolution.extension.kenburn.min.js',
        'js/owl.carousel.js',
        'js/owl.animate.js',
        'js/dropzone.js',
        'js/bearsthemes.js',
        'js/main.js',
        'js/my_js.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
