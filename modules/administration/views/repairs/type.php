<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('add_repairs')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Repair added!',
    ]);
endif;
if (Yii::$app->session->hasFlash('not_add_repairs')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Repair not added!',
    ]);
endif;

if (Yii::$app->session->hasFlash('update_user')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'User updated!',
    ]);
endif;
if (Yii::$app->session->hasFlash('not_update_user')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'User not updated!',
    ]);
endif;

if (Yii::$app->session->hasFlash('delete_user')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'User deleted!',
    ]);
endif;
if (Yii::$app->session->hasFlash('not_delete_user')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'User not deleted!',
    ]);
endif;
?>

<div style="display:none;">
    <div class="table table-striped previewTemplate">
        <div id="template" class="file-row">
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                 aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
        </div>
    </div>
</div>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu', []); ?>

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa" data-action="show"> </i> Create repair
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewRepairs, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Image</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewRepairs, 'img_src')->hiddenInput()->label(false); ?>
                                    </div>

                                    <div class="col-xs-12 col-sm-8">
                                        <div class='repair_photo' style=""></div>
                                        <div class="show_repair_photo"
                                             style="display: block;position:relative;width:200px;">
                                            <img src="/images/pages/default.jpg" style="width:200px;">
                                            <i class="fa fa-times deleteRepairPhoto"
                                               style="display:none;position:absolute;right: -9px;top: -12px;font-size: 25px;cursor: pointer;"></i>
                                        </div>
                                        <a class="add_repair_photo  pull-left">Add Photo</a>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Sort</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewRepairs, 'sort')->textinput()->label(false); ?>
                                    </div>
                                    <div>
                                        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Repairs
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelRepairs,
                                        'columns' => [
                                            [
                                                'attribute' => '#',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    return $modelModels['id'];
                                                }
                                            ],
                                            [
                                                'attribute' => 'img_src',
                                                'format' => 'html',
                                                'value' => function ($modelRepairs) {
                                                    $img_src = "/images/pages/default.jpg";
                                                    if (($modelRepairs->img_src != '') && ($modelRepairs->img_src != null)) {
                                                        $img_src = "/images/repair/" . $modelRepairs->img_src;
                                                    }
                                                    return '<img src="' . $img_src . '" style="max-width:100px;">';
                                                }
                                            ],
                                            'name',
                                            'sort',
                                            'date_create',
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{show} {update} {delete} ',
                                                'buttons' => [
                                                    'show' => function ($url, $modelRepairs) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-eye-open"></span>',
                                                            'repairshow?id=' . $modelRepairs['id']);
                                                    },
                                                    'update' => function ($url, $modelRepairs) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                                            'repairupdate?id=' . $modelRepairs['id']);
                                                    },
                                                    'delete' => function ($url, $modelRepairs) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>',
                                                            'repairdelete?id=' . $modelRepairs['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>