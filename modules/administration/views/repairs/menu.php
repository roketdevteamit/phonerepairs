<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;
//var_dump($this->context->getRoute());exit;

    $class = ($this->context->getRoute() == 'administration/repairs/type')?'active':''; 
    $class1 = ($this->context->getRoute() == 'administration/repairs/brand')?'active':''; 
    $class2 = ($this->context->getRoute() == 'administration/repairs/services')?'active':''; 
    $class3 = ($this->context->getRoute() == 'administration/repairs/models')?'active':''; 
?>
<style>
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        color: #464a4c;
        background-color: #fff;
        border-color: #ddd #ddd #fff;
    }
</style>
<?php Yii::$app->language = 'en-US'; ?>

<ul class="nav nav-tabs tab_title_admin">
    <li class="nav-item">
        <?= HTML::a('Repairs type',Url::home().'administration/repairs/type', ['class' => 'nav-link '.$class]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Brand',Url::home().'administration/repairs/brand', ['class' => 'nav-link '.$class1]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Services',Url::home().'administration/repairs/services', ['class' => 'nav-link '.$class2]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Models',Url::home().'administration/repairs/models', ['class' => 'nav-link '.$class3]); ?>
    </li>
</ul>
