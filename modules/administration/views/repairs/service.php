<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('add_service')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'service added!',
    ]);
endif;
if (Yii::$app->session->hasFlash('not_add_service')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Service not added!',
    ]);
endif;

if (Yii::$app->session->hasFlash('service_update')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Service updated!',
    ]);
endif;
if (Yii::$app->session->hasFlash('service_not_update')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Service not updated!',
    ]);
endif;

if (Yii::$app->session->hasFlash('service_delete')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Service deleted!',
    ]);
endif;
if (Yii::$app->session->hasFlash('brand_not_delete')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Service not deleted!',
    ]);
endif;
?>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu', []); ?>

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa" data-action="show"> </i> Create Service
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(); ?>
                                    <div class="col-xs-12 col-sm-4 userform">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 userform">
                                        <?= $form->field($modelNewService, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 userform">
                                        <p>Repairs</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 ">
                                        <?= $form->field($modelNewService, 'repairs')->inline(true)->checkboxList($repairsArray)->label(false) ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary pull-right']) ?>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Services
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelService,
                                        'columns' => [
                                            [
                                                'attribute' => '#',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    return $modelModels['id'];
                                                }
                                            ],
                                            'name',
                                            [
                                                'attribute' => 'repairs',
                                                'value' => function ($modelService) {
                                                    return app\widgets\GetServiceRepairsWidget::widget(['service_id' => $modelService['id']]);
                                                }
                                            ],
                                            'date_create',
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{show} {update} {delete} ',
                                                'buttons' => [
                                                    'show' => function ($url, $modelService) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-eye-open"></span>',
                                                            'serviceshow?id=' . $modelService['id']);
                                                    },
                                                    'update' => function ($url, $modelService) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                                            'serviceupdate?id=' . $modelService['id']);
                                                    },
                                                    'delete' => function ($url, $modelService) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>',
                                                            'servicedelete?id=' . $modelService['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>