<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

use app\assets\CreatemodelsAsset;

CreatemodelsAsset::register($this);

?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('models_add')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Models added!',
    ]);
endif;
if (Yii::$app->session->hasFlash('models_not_add')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Models not added!',
    ]);
endif;

if (Yii::$app->session->hasFlash('models_update')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Models updated!',
    ]);
endif;
if (Yii::$app->session->hasFlash('models_not_update')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Models not updated!',
    ]);
endif;

if (Yii::$app->session->hasFlash('models_delete')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Models deleted!',
    ]);
endif;
if (Yii::$app->session->hasFlash('models_not_delete')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Models not deleted!',
    ]);
endif;
?>

<div style="display:none;">
    <div class="table table-striped previewTemplate">
        <div id="template" class="file-row">
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                 aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
        </div>
    </div>
</div>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu', []); ?>

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Create Models
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                    $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewModels, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Repairs</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewModels, 'repairs_id')->dropDownList($repairsArray, ['prompt' => ' -- Please Choose Repairs --'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Brand</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewModels, 'brand_id')->dropDownList([], ['prompt' => ' -- Please Choose Brands --'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Image</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewModels, 'img_src')->hiddenInput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 ">
                                        <div class='models_photo' style=""></div>
                                        <div class="show_models_photo"
                                             style="display: block;position:relative;width:200px;">
                                            <img src="/images/pages/default.jpg" style="width:200px;">
                                            <i class="fa fa-times deleteModelsPhoto"
                                               style="display:none;position:absolute;right: -9px;top: -12px;font-size: 25px;cursor: pointer;"></i>
                                        </div>
                                        <a class="add_models_photo btn btn-primary pull-left">Change photo</a>
                                    </div>

                                    <div class="form-group field-models-repairs_id required table_border">
                                        <label class="control-label" for="models-repairs_id">Services</label>
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Service name</th>
                                                <th>Price for service £</th>
                                            </tr>
                                            </thead>
                                            <tbody class="tableServices">
                                            <tr>
                                                <td colspan="3">No results found.</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Deactive</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewModels, 'status')->dropDownList(['1' => 'Active', '0' => 'Deactive'])->label(false); ?>
                                    </div>
                                    <?php //= $form->field($modelNewBrand, 'repairs')->inline(true)->checkboxList($repairsArray) ?>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Models
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelModels,
                                        'columns' => [
                                            [
                                                'attribute' => '#',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    return $modelModels['id'];
                                                }
                                            ],
                                            'name',
                                            [
                                                'attribute' => 'img_src',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    $img_src = "/images/pages/default.jpg";
                                                    if (($modelModels->img_src != '') && ($modelModels->img_src != null)) {
                                                        $img_src = "/images/models/" . $modelModels->img_src;
                                                    }
                                                    return '<img src="' . $img_src . '" style="max-width:100px;">';
                                                }
                                            ],
                                            [
                                                'attribute' => 'repairs_id',
                                                'value' => function ($modelModels) {
                                                    return app\widgets\GetRepairsNameWidget::widget(['repair_id' => $modelModels['repairs_id']]);
                                                }
                                            ],
                                            [
                                                'attribute' => 'brand_id',
                                                'value' => function ($modelModels) {
                                                    return app\widgets\GetBrandNameWidget::widget(['brand_id' => $modelModels['brand_id']]);
                                                }
                                            ],
                                            'date_create',
                                            [
                                                'attribute' => 'status',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    if ($modelModels->status == 0) {
                                                        return 'Deactive';
                                                    } else {
                                                        return 'Active';
                                                    }

                                                }
                                            ],
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{show} {update} {delete} ',
                                                'buttons' => [
                                                    'show' => function ($url, $modelModels) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-eye-open"></span>',
                                                            'modelsshow?id=' . $modelModels['id']);
                                                    },
                                                    'update' => function ($url, $modelModels) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                                            'modelsupdate?id=' . $modelModels['id']);
                                                    },
                                                    'delete' => function ($url, $modelModels) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>',
                                                            'modelsdelete?id=' . $modelModels['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>