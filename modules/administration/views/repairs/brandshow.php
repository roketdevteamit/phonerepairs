<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;
use yii\grid\GridView;
?>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu',[]); ?>
        
        <a href="<?= Url::home().'administration/repairs/brand'; ?>"class="img_back">
            <img src="/images/admin/back-button-png-image-59238.png">
        </a>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa" data-action="show"> </i>Change brand
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <?php 
                                        $img_src = "/images/pages/default.jpg";
                                        if(($modelBrand->img_src != '') && ($modelBrand->img_src != null)){
                                            $img_src = "/images/brand/".$modelBrand->img_src;
                                        }
                                    ?>
                                    <img src="<?= $img_src; ?>" style="width:200px;">
                                </div>
                                <div class="col-sm-9">
                                    <p style="font-size:30px;"><?= $modelBrand->name; ?></p>
                                    <p>
                                        <?php foreach($brand_repairs_id as $repairs_id){ ?>
                                            <?= \app\widgets\GetRepairsNameWidget::widget(['repair_id' => $repairs_id]).'; '; ?>
                                        <?php } ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa fa-arrow-down" data-action="show"> </i> Models
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelModels,
                                        'columns' => [
                                            [
                                                'attribute' => '#',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    return $modelModels['id'];
                                                }
                                            ],
                                            'name',
                                            [
                                                'attribute' => 'img_src',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    $img_src = "/images/pages/default.jpg";
                                                    if(($modelModels->img_src != '') && ($modelModels->img_src != null)){
                                                        $img_src = "/images/models/".$modelModels->img_src;
                                                    }
                                                    return '<img src="'.$img_src.'" style="max-width:100px;">';
                                                }
                                            ],
                                            [
                                                'attribute' => 'repairs_id',
                                                'value' => function ($modelModels) {
                                                    return app\widgets\GetRepairsNameWidget::widget(['repair_id' => $modelModels['repairs_id']]);
                                                }
                                            ],
                                            [
                                                'attribute' => 'brand_id',
                                                'value' => function ($modelModels) {
                                                    return app\widgets\GetBrandNameWidget::widget(['brand_id' => $modelModels['brand_id']]);
                                                }
                                            ],
                                            'date_create',
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{show} {delete} {update}',
                                                'buttons' => [
                                                    'update' => function ($url,$modelModels) {
                                                            return Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                                            'modelsupdate?id='.$modelModels['id']);
                                                    },
                                                    'delete' => function ($url,$modelModels) {
                                                            return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>', 
                                                            'modelsdelete?id='.$modelModels['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>