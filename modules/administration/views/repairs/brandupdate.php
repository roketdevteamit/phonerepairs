<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;

?>

<div style="display:none;">
    <div class="table table-striped previewTemplate">
        <div id="template" class="file-row">
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                 aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
        </div>
    </div>
</div>
<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu', []); ?>
        <a href="<?= Url::home() . 'administration/repairs/brand'; ?>" class="img_back">
            <img src="/images/admin/back-button-png-image-59238.png">
        </a>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa" data-action="show"> </i>Change brand
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(); ?>
                                    <div class="col-xs-12 col-sm-4 userform">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 userform">
                                        <?= $form->field($modelBrand, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 userform">
                                        <p>Repairs</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 ">
                                        <?= $form->field($modelBrand, 'repairs')->inline(true)->checkboxList($repairsArray)->label(false) ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 userform">
                                        <p>Image</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 ">
                                        <?= $form->field($modelBrand, 'img_src')->hiddenInput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 userform">
                                        <?php
                                        $img_src = "/images/pages/default.jpg";
                                        if (($modelBrand->img_src != '') && ($modelBrand->img_src != null)) {
                                            $img_src = "/images/brand/" . $modelBrand->img_src;
                                        }
                                        ?>
                                        <div class='brand_photo' style=""></div>
                                        <div class="show_brand_photo" style="display: block;position:relative">
                                            <img src="<?= $img_src; ?>" style="width:200px;">
                                            <i class="fa fa-times deleteBrandPhoto"
                                               style="display:none;position:absolute;right: -9px;top: -12px;font-size: 25px;cursor: pointer;"></i>
                                        </div>
                                        <a class="add_brand_photo btn btn-primary pull-left">Change photo</a>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 userform">
                                        <p>Sort</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelBrand, 'sort')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>