<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;

use app\assets\CreatemodelsAsset;

CreatemodelsAsset::register($this);

?>
<div style="display:none;">
    <div class="table table-striped previewTemplate">
        <div id="template" class="file-row">
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                 aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
        </div>
    </div>
</div>
<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu', []); ?>
        <a href="<?= Url::home() . 'administration/repairs/models'; ?>" class="img_back">
            <img src="/images/admin/back-button-png-image-59238.png">
        </a>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa " data-action="show"> </i>Change models
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php
                                    $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelModels, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Repairs</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelModels, 'repairs_id')->dropDownList($repairsArray, ['prompt' => ' -- Pleace choise repairs type --'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Brand</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelModels, 'brand_id')->dropDownList($brandArray)->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Image</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelModels, 'img_src')->hiddenInput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 ">
                                        <?php
                                        $img_src = "/images/pages/default.jpg";
                                        if (($modelModels->img_src != '') && ($modelModels->img_src != null)) {
                                            $img_src = "/images/models/" . $modelModels->img_src;
                                        }
                                        ?>
                                        <div class='models_photo' style=""></div>
                                        <div class="show_models_photo" style="display: block;position:relative">
                                            <img src="<?= $img_src; ?>" style="width:200px;">
                                            <i class="fa fa-times deleteModelsPhoto"
                                               style="display:none;position:absolute;right: -9px;top: -12px;font-size: 25px;cursor: pointer;"></i>
                                        </div>
                                        <a class="add_models_photo btn btn-primary pull-left">Change photo</a>
                                    </div>

                                    <div class="form-group field-models-repairs_id required table_border">
                                        <label class="control-label" for="models-repairs_id">Services</label>
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Service name</th>
                                                <th>Price for service £</th>
                                            </tr>
                                            </thead>
                                            <tbody class="tableServices">
                                            <?php if ($serviceArray) { ?>
                                                <?php foreach ($serviceArray as $service_id => $service_name) { ?>
                                                    <?php if (array_key_exists($service_id, $serviceChoiseArray)) { ?>
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" checked
                                                                       name="Models[services][]"
                                                                       value="<?= $service_id; ?>"></td>
                                                            <td><?= $service_name ?></td>
                                                            <td><input type="number" min="1"
                                                                       value="<?= $serviceChoiseArray[$service_id]; ?>"
                                                                       class="form-control"
                                                                       name="Models[servicesprice][<?= $service_id; ?>]">
                                                            </td>
                                                        </tr>
                                                    <?php } else { ?>
                                                        <tr>
                                                            <td>
                                                                <input type="checkbox" name="Models[services][]"
                                                                       value="<?= $service_id; ?>"></td>
                                                            <td><?= $service_name ?></td>
                                                            <td><input type="number" min="1" value="1"
                                                                       class="form-control"
                                                                       name="Models[servicesprice][<?= $service_id; ?>]">
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <tr>
                                                    <td colspan="3">No results found.</td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Status</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelModels, 'status')->dropDownList(['1' => 'Active', '0' => 'Deactive'])->label(false); ?>
                                    </div>
                                    <?php //= $form->field($modelNewBrand, 'repairs')->inline(true)->checkboxList($repairsArray) ?>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>