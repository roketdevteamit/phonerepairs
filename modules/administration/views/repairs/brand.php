<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('add_brand')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Brand added!',
    ]);
endif;
if (Yii::$app->session->hasFlash('not_add_brand')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Brand not added!',
    ]);
endif;

if (Yii::$app->session->hasFlash('brand_update')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Brand updated!',
    ]);
endif;
if (Yii::$app->session->hasFlash('brand_not_update')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Brand not updated!',
    ]);
endif;

if (Yii::$app->session->hasFlash('brand_delete')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Brand deleted!',
    ]);
endif;
if (Yii::$app->session->hasFlash('brand_not_delete')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Brand not deleted!',
    ]);
endif;
?>

<div style="display:none;">
    <div class="table table-striped previewTemplate">
        <div id="template" class="file-row">
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                 aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
        </div>
    </div>
</div>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu', []); ?>

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Create Brand
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(); ?>
                                    <div class="col-xs-12 col-sm-4 userform">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 userform">
                                        <?= $form->field($modelNewBrand, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 userform">
                                        <p>Repairs</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBrand, 'repairs')->inline(true)->checkboxList($repairsArray)->label(false) ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 userform">
                                        <p>Image</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBrand, 'img_src')->hiddenInput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 userform">
                                        <div class='brand_photo' style=""></div>
                                        <div class="show_brand_photo"
                                             style="display: block;position:relative;width:200px;">
                                            <img src="/images/pages/default.jpg" style="width:200px;">
                                            <i class="fa fa-times deleteBrandPhoto"
                                               style="display:none;position:absolute;right: -9px;top: -12px;font-size: 25px;cursor: pointer;"></i>
                                        </div>
                                        <a class="add_brand_photo btn btn-primary pull-left">Change photo</a>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 userform">
                                        <p>Sort</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBrand, 'sort')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                    <?= Html::submitButton(Yii::t('app', 'Location'), ['class' => 'btn btn-primary pull-right']) ?>
                                    <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa" data-action="show"> </i> Brands
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelBrands,
                                        'columns' => [
                                            [
                                                'attribute' => '#',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    return $modelModels['id'];
                                                }
                                            ],
                                            [
                                                'attribute' => 'img_src',
                                                'format' => 'html',
                                                'value' => function ($modelBrands) {
                                                    $img_src = "/images/pages/default.jpg";
                                                    if (($modelBrands->img_src != '') && ($modelBrands->img_src != null)) {
                                                        $img_src = "/images/brand/" . $modelBrands->img_src;
                                                    }
                                                    return '<img src="' . $img_src . '" style="max-width:100px;">';
                                                }
                                            ],
                                            'name',
                                            [
                                                'attribute' => 'repairs',
                                                'value' => function ($modelBrands) {
                                                    return app\widgets\GetBrandRepairsWidget::widget(['brand_id' => $modelBrands['id']]);
                                                }
                                            ],
                                            'date_create',
                                            'sort',
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{show} {update} {delete} ',
                                                'buttons' => [
                                                    'show' => function ($url, $modelBrands) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-eye-open"></span>',
                                                            'brandshow?id=' . $modelBrands['id']);
                                                    },
                                                    'update' => function ($url, $modelBrands) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                                            'brandupdate?id=' . $modelBrands['id']);
                                                    },
                                                    'delete' => function ($url, $modelBrands) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>',
                                                            'branddelete?id=' . $modelBrands['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>