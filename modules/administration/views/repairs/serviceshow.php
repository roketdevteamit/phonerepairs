<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;
?>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu',[]); ?>
        <a href="<?= Url::home().'administration/repairs/services'; ?>" class="img_back">
            <img src="/images/admin/back-button-png-image-59238.png">
        </a>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa" data-action="show"> </i>Change service
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p style="font-size:30px;"><?= $modelService->name; ?></p>
                                    <p>
                                        <?php foreach($modelService->repairs as $repairs_id){ ?>
                                            <?= \app\widgets\GetRepairsNameWidget::widget(['repair_id' => $repairs_id]).'; '; ?>
                                        <?php } ?>
                                    </p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table_border" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa " data-action="show"> </i>Service models
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-success alert-dismissable successSavedPrice" style="display:none;">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <strong>Success!</strong> Price saved.
                                      </div>
                                   <?php if($serviceChoiseArray){ ?>
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Models name</th>
                                                    <th>Price for service £</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody class="tableServices">
                                                <?php $k = 0;?>
                                                <?php foreach($serviceChoiseArray as  $models_id => $price){ ?>
                                                <?php $k++;?>
                                                    <tr>
                                                        <td><?= $k; ?></td>
                                                        <td><?= app\widgets\GetModelsNameWidget::widget(['models_id' => $models_id]); ?></td>
                                                        <td>
                                                            <input type="number" min="1" class="form-control" id="ModelsService<?= $models_id; ?>" value="<?= $price; ?>">
                                                        </td>
                                                        <td>
                                                            <div class="userform" ">
                                                            <button style="margin: 0px;     padding: 0px 6px 0px;" class="btn btn-primary changeModelServicePrice  " data-service_id="<?= $modelService->id; ?>" data-models_id="<?= $models_id; ?>">Change</button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                   <?php } ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
</div>