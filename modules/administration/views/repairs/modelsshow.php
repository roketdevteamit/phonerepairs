<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;

use app\assets\CreatemodelsAsset;
CreatemodelsAsset::register($this);

?>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu',[]); ?>
        
        <a href="<?= Url::home().'administration/repairs/models'; ?>" class="img_back">
            <img src="/images/admin/back-button-png-image-59238.png">
        </a>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa " data-action="show"> </i>Models show
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    
                                    <table class="table table-striped table-bordered">
                                        <table>
                                            <tr>
                                                <td colspan="2" style="font-size: 30px;text-align: center"><?= $modelModels->name; ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <?php 
                                                        $img_src = "/images/pages/default.jpg";
                                                        if(($modelModels->img_src != '') && ($modelModels->img_src != null)){
                                                            $img_src = "/images/models/".$modelModels->img_src;
                                                        }
                                                    ?>
                                                    <img  src="<?= $img_src; ?>" style="width:200px;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Repair</td>
                                                <td><?= app\widgets\GetRepairsNameWidget::widget(['repair_id' => $modelModels->repairs_id]); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Brand</td>
                                                <td><?= app\widgets\GetBrandNameWidget::widget(['brand_id' => $modelModels->brand_id]); ?></td>
                                            </tr>
                                            <tr>
                                                <td>Status</td>
                                                <td>
                                                    <?php if($modelModels->status == 0){ ?>
                                                        Deactive
                                                    <?php }else{ ?>
                                                            Active
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="table table-striped table-bordered"><thead>
                                                <thead>
                                                    <tr>
                                                        <th>Service name</th>
                                                        <th>Price for service £</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="tableServices">
                                                    <?php if($serviceArray){ ?>
                                                        <?php foreach($serviceArray as $service_id => $service_name){ ?>
                                                            <?php if(array_key_exists($service_id,$serviceChoiseArray)){ ?>
                                                                <tr>
<!--                                                                    <td>
                                                                        <input type="checkbox" checked name="Models[services][]" value="<?php //= $service_id; ?>">
                                                                    </td>-->
                                                                    <td><?= $service_name ?></td>
                                                                    <td>
                                                                        <?= $serviceChoiseArray[$service_id]; ?>
                                                                    </td>
                                                                </tr>
                                                            <?php }else{ ?>
<!--                                                                <tr>
                                                                    <td>
                                                                        <input type="checkbox" name="Models[services][]" value="<?= $service_id; ?>"></td>
                                                                    <td><?php //= $service_name ?></td>
                                                                    <td><input type="number" min="1" value="1" class="form-control" name="Models[servicesprice][]" ></td>
                                                                </tr>-->
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php }else{ ?>
                                                        <tr>
                                                            <td colspan="3">No results found.</td>
                                                        </tr>                                                        
                                                    <?php } ?>
                                                </tbody>
                                            </table>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>