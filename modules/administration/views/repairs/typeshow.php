<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;
use yii\grid\GridView;
?>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu',[]); ?>
        <a href="<?= Url::home().'administration/repairs/type'; ?>" class="img_back">
            <img src="/images/admin/back-button-png-image-59238.png">
        </a>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa  " data-action="show"> </i>Repair
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p style="font-size:30px;text-align: center;"><b><?= $modelRepairs->name; ?></b></p>
                                </div>
                                <?php 
                                    $img_src = "/images/pages/default.jpg";
                                    if(($modelRepairs->img_src != '') && ($modelRepairs->img_src != null)){
                                        $img_src = "/images/repair/".$modelRepairs->img_src;
                                    }
                                ?>
                                    <img src="<?= $img_src; ?>" style="width:200px;">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa" data-action="show"> </i> Brands
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelBrands,
                                        'columns' => [
                                            [
                                                'attribute' => '#',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    return $modelModels['id'];
                                                }
                                            ],
                                            [
                                                'attribute' => 'img_src',
                                                'format' => 'html',
                                                'value' => function ($modelBrands) {
                                                    $img_src = "/images/pages/default.jpg";
                                                    if(($modelBrands['img_src'] != '') && ($modelBrands['img_src'] != null)){
                                                        $img_src = "/images/brand/".$modelBrands['img_src'];
                                                    }
                                                    return '<img src="'.$img_src.'" style="max-width:100px;">';
                                                }
                                            ],
                                            'name',
                                            'date_create',
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{show}{update} {delete} ',
                                                'buttons' => [
                                                    'show' => function ($url,$modelBrands) {
                                                            return Html::a(
                                                            '<span class="glyphicon glyphicon-eye-open"></span>',
                                                            'brandshow?id='.$modelBrands['id']);
                                                    },
                                                    'update' => function ($url,$modelBrands) {
                                                            return Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                                            'brandupdate?id='.$modelBrands['id']);
                                                    },
                                                    'delete' => function ($url,$modelBrands) {
                                                            return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>', 
                                                            'branddelete?id='.$modelBrands['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>