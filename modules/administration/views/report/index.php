<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

use kartik\datetime\DateTimePicker;
?>
<div class="dashboard-container">
    <div class="container padding">
        <h1>Total Price: £<?= $total_price; ?></h1>

        <div class="dashboard-wrapper-lg">
            <div class="container">
                    <div class="well">
                        <?php $form = ActiveForm::begin(['method' => 'get'] ); ?>
                                <div class="row" style="padding:10px;">
                                    <div class="col-sm-6">
                                            <label>Date/Time from</label>;
                                        <?php 
                                            $date_from = '';
                                            if(isset($_GET['date_from'])){
                                                $date_from = $_GET['date_from'];                                                
                                            }
                                            echo DateTimePicker::widget([
                                                'name' => 'date_from',
                                                'options' => ['placeholder' => 'Select date from ...'],
                                                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                                'value' => $date_from,
                                                'pluginOptions' => [
                                                    'autoclose'=>true,
                                                    'format' => 'yyyy-mm-dd hh:ii'
                                                ]
                                            ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6">
                                            <label>Date/Time to</label>;
                                        <?php 
                                            $date_to = '';
                                            if(isset($_GET['date_to'])){
                                                $date_to = $_GET['date_to'];                                                
                                            }
                                           echo DateTimePicker::widget([
                                                'name' => 'date_to',
                                                'options' => ['placeholder' => 'Select date to ...'],
                                                'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
                                                'value' => $date_to,
                                                'pluginOptions' => [
                                                    'autoclose'=>true,
                                                    'format' => 'yyyy-mm-dd hh:ii'
                                                ]
                                            ]);
                                        ?>
                                    </div>
                                </div>

                                <div class="row" style="padding:10px;">
                                    <div class="col-sm-6">
                                        <?php 
                                            $repair_value = '';
                                            if(isset($_GET['repairs_type'])){
                                                $repair_value = $_GET['repairs_type'];
                                            }
                                        ?>
                                        <?= Html::dropDownList('repairs_type', $repair_value, $arrayRepairs,['class' => 'form-control', 'prompt' => '--- Select repairs ---']); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php 
                                            $service_value = '';
                                            if(isset($_GET['service'])){
                                                $service_value = $_GET['service'];
                                            }
                                        ?>
                                        <?= Html::dropDownList('service', $service_value, $arrayService, ['class' => 'form-control', 'prompt' => '--- select service ---']) ?>
                                    </div>
                                </div>
                                <div class="row" style="padding:10px;">
                                    <div class="col-sm-6">
                                        <?php 
                                            $models_value = '';
                                            if(isset($_GET['models'])){
                                                $models_value = $_GET['models'];
                                            }
                                        ?>
                                        <?= Html::dropDownList('models', $models_value, $arrayModels,['class' => 'form-control', 'prompt' => '--- select models ---']) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php 
                                            $branch_value = '';
                                            if(isset($_GET['branch'])){
                                                $branch_value = $_GET['branch'];
                                            }
                                        ?>
                                        <?= Html::dropDownList('branch', $branch_value, $arrayBranch,['class' => 'form-control', 'prompt' => '--- select branch ---']) ?>
                                    </div>
                                </div>
                            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary pull-right']) ?>
                        <?php ActiveForm::end(); ?>
                        <div style="clear:both;"></div>
                    </div>
                
                <div class="col-sm-12 table_block" >
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa" data-action="show"> </i> Leads
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelLeads,
                                        'tableOptions'=>['class'=>'table table-striped table-bordered table_border'],
                                        'columns' => [
                                            [
                                                'attribute' => 'id',
                                                'format' => 'html',
                                                'value' => function ($modelLeads) {
                                                    $src = ''; 
                                                    switch($modelLeads['leads_type']){
                                                        case 0:
                                                            $src = '/administration/leads/enquiriesshow?id='.$modelLeads['id'];
                                                            break;
                                                        case 1:
                                                            $src = '/administration/leads/mail_inshow?id='.$modelLeads['id'];
                                                            break;
                                                        case 2:
                                                            $src = '/administration/leads/bookingsshow?id='.$modelLeads['id']; 
                                                            break;
                                                        case 4:
                                                            $src = '/administration/leads/corporateshow?id='.$modelLeads['id']; 
                                                            break;
                                                    }

                                                    return '<a href="'.$src.'">'.$modelLeads['key'] . '' . $modelLeads['id'].'</a>';
                                                }
                                            ],
                                            'name',
                                            'email',
                                            'price',
                                            'phone_number',
                                            'date_create',
                                            [
                                                'attribute' => 'Service',
                                                'value' => function ($modelLeads) {
                                                    if ($modelLeads['service_id'] != 0) {
                                                        return \app\widgets\GetServiceNameWidget::widget(['service_id' => $modelLeads['service_id']]);
                                                    }
                                                }
                                            ],
                                            [
                                                'attribute' => 'Model',
                                                'value' => function ($modelLeads) {
                                                    if ($modelLeads['model_id'] != 0) {
                                                        return \app\widgets\GetModelsNameWidget::widget(['models_id' => $modelLeads['model_id']]);
                                                    }

                                                }
                                            ],
//                                            [
//                                                'attribute' => 'Source',
//                                                'value' => function ($modelLeads) {
//                                                    if (($modelLeads['hear_us'] != '') && ($modelLeads['hear_us'] != null)) {
//                                                        $hear_us = ['1' => 'Google', '2' => 'Search Engine', '3' => 'External Website', '4' => 'Word of Mouth', '5' => 'Leaflets/Newspaper', '6' => 'Yell.com', '7' => 'Bing', '8' => 'Yahoo'];
//                                                        return $hear_us[(int)$modelLeads['hear_us']];
//                                                    } else {
//                                                        return '';
//                                                    }
//                                                }
//                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>