<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>


<div class="dashboard-container">
    <div class="container">
        
        <?php
            if(Yii::$app->session->hasFlash('delete_booking')):
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-info',
                    ],
                    'body' => 'Deleted',
                ]);
            endif;
        ?>
        
        <?= $this->render('menu', []); ?>

        <div class="dashboard-wrapper-lg">
            <div class="row wrap ">
                <?php
                $branch_count = '';
                switch (count($modelBranch)) {
                    case 1:
                        $branch_count = 'class1';
                        break;
                    case 2:
                        $branch_count = 'class2';
                        break;
                    case 3:
                        $branch_count = 'class3';
                        break;
                    default:
                        $branch_count = 'class4';
                        break;
                }
                ?>
                <div class="booking_button <?= $branch_count; ?>">
                    <?php $class = 'btn btn-default'; ?>
                    <?php if (!isset($_GET['branch'])) { ?>
                        <?php $class = 'btn btn-primary '; ?>
                    <?php } ?>

                    <a href="/administration/leads/bookings" class="<?= $class; ?> ">All</a>
                    <?php foreach ($modelBranch as $branch) { ?>
                        <?php $class = 'btn btn-default '; ?>
                        <?php if (isset($_GET['branch'])) { ?>
                            <?php if ($_GET['branch'] == $branch['id']) { ?>
                                <?php $class = 'btn btn-primary '; ?>
                            <?php } ?>
                        <?php } ?>
                        <a href="/administration/leads/bookings/<?= $branch['id']; ?>"
                           class="<?= $class; ?>"><?= $branch['address']; ?></a>
                    <?php } ?>
                    <?php /* $form = ActiveForm::begin(['method' => 'GET']); ?>
                        <?= $form->field($modelNewLeadmessage, 'content')->textarea(); ?>
                   <?= Html::submitButton(Yii::t('app', 'Sent'), ['name' => 'sent_commect', 'class' => 'btn btn-primary pull-right']) ?>
                <?php ActiveForm::end(); */ ?>
                </div>
                <?php
                    $reference_no = '';
                    if(isset($_GET['reference_no'])){
                        if($_GET['reference_no'] != ''){
                            $reference_no = $_GET['reference_no'];
                        }
                    }
                ?>
                <div class="col-sm-12">
                    <form method="get">
                        <div class="input-group userform">
                            <input type="text" class="form-control" name="reference_no" value="<?= $reference_no; ?>" placeholder="Reference no...">
                                <span class="input-group-btn" data-original-title="" title="">
                                  <button type="submit" class="btn btn-default button_search">Go!</button>
                                </span>
                        </div>
                    </form>
                </div>
                
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa" data-action="show"> </i> Bookings
                            </div>
                           <a class="btn btn-default" href="/administration/leads/create_booking">Create booking</a>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border_gray class_hom_page">
                                    <?php foreach ($arrayBookings as $booking_date => $bookings) { ?>
                                        <p class="title_p"><?= $booking_date; ?>
                                            : <?= date('l', strtotime($booking_date)); ?></p>
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>
                                                    <a href="/administration/leads/bookings">Booking Date</a>
                                                </th>
                                                <th>
                                                    <a href="/administration/leads/bookings">Name</a>
                                                </th>
                                                <th>
                                                    <a href="/administration/leads/bookings">Email</a>
                                                </th>
                                                <th>
                                                    <a href="/administration/leads/bookings">Status</a>
                                                </th>
                                                <th>Branch</th>
                                                <th>
                                                    <a href="/administration/leads/bookings">#</a>
                                                </th>
                                                <th>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($bookings as $booking){ ?>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <?= $booking['booking_date']; ?>
                                                </td>
                                                <td>
                                                    <?= $booking['name']; ?>
                                                </td>
                                                <td>
                                                    <?= $booking['email']; ?>
                                                </td>
                                                <td>
                                                    <?= $booking['status']; ?>
                                                </td>
                                                <td><?php //= \app\widgets\GetBranchAddressWidget::widget(['branch_id' => $booking['branch_id']]); ?></td>
                                                <td>
                                                    <a href="/administration/leads/bookingsshow?id=<?= $booking['id']; ?>"> <?= $booking['key'] . '' . $booking['id']; ?></a>
                                                </td>
                                                <td>
                                                    <?=  Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                                            'bookingsshow?id=' . $booking['id']); ?>
                                                    <?=  Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>',
                                                            'bookingsdelete?id=' . $booking['id']); ?>
                                                </td>
                                            </tr>
                                            </tbody>
                                            <?php } ?>

                                        </table>
                                    <?php } ?>
                                    <?= LinkPager::widget(['pagination' => $pagination]); ?>
                                    <?php /*= GridView::widget([
                                        'dataProvider' => $modelBookings,
                                        'columns' => [
                                            'booking_date',
                                            'name',
                                            'email',
                                            'status',
                                            [
                                                'attribute' => 'Branch',
                                                'value' => function ($modelBookings) {    
                                                    if($modelBookings['branch_id'] != 0){
                                                        return \app\widgets\GetBranchAddressWidget::widget(['branch_id' => $modelBookings['branch_id']]);
                                                    }
                                                }
                                            ],
                                            [
                                                'attribute' => 'id',
                                                'format' => 'html',
                                                'value' => function ($modelBookings) {    
                                                    return '<a href="/administration/leads/bookingsshow?id='.$modelBookings['id'].'">'.$modelBookings['key'].''.$modelBookings['id'].'</a>';
                                                }
                                            ],
//                                            [
//                                                'class' => 'yii\grid\ActionColumn',
//                                                'template' => '{show} {delete} {update}',
//                                                'buttons' => [
//                                                    'show' => function ($url,$modelRepairs) {
//                                                            return Html::a(
//                                                            '<span class="glyphicon glyphicon-eye-open"></span>', 
//                                                            'repairshow?id='.$modelRepairs['id']);
//                                                    },
//                                                    'update' => function ($url,$modelRepairs) {
//                                                            return Html::a(
//                                                            '<span class="glyphicon glyphicon-pencil"></span>', 
//                                                            'repairupdate?id='.$modelRepairs['id']);
//                                                    },
//                                                    'delete' => function ($url,$modelRepairs) {
//                                                            return Html::a(
//                                                            '<span class="glyphicon glyphicon-trash"></span>', 
//                                                            'repairdelete?id='.$modelRepairs['id']);
//                                                    },
//                                                ],
//                                            ],
                                        ],
                                    ]) */ ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>