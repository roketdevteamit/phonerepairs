<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>


<div class="dashboard-container">
    <div class="container">
        <?php
            if(Yii::$app->session->hasFlash('delete_mail')):
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-info',
                    ],
                    'body' => 'Deleted',
                ]);
            endif;
        ?>
        
        <?= $this->render('menu',[]); ?>
        
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height: 0px; padding-top: 10px;">
                                <i class="fa " data-action="show"> </i> Mail in
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border_gray">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelMailin,
                                        'columns' => [
                                            [
                                                'attribute' => '#',
                                                'format' => 'html',
                                                'value' => function ($modelMailin) {    
                                                    return '<a href="/administration/leads/mail_inshow?id='.$modelMailin['id'].'">'.$modelMailin['key'].''.$modelMailin['id'].'</a>';
                                                }
                                            ],
                                            'name',
                                            'address',
                                            'device_model',
                                            'date_create',
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{update} {delete} ',
                                                'buttons' => [
                                                    'update' => function ($url,$modelMailin) {
                                                            return Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>', 
                                                            'mail_inshow?id='.$modelMailin['id']);
                                                    },
                                                    'delete' => function ($url,$modelMailin) {
                                                            return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>', 
                                                            'maildelete?id='.$modelMailin['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>