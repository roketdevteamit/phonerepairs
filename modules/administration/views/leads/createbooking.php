<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu', []); ?>

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-12 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa" data-action="show"> </i>Create bookings
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div style="height: 0px">
                                        <?= $form->field($modelNewBooking, 'key')->hiddenInput(['value' => 'BK'])->label(false); ?>
                                        <?= $form->field($modelNewBooking, 'leads_type')->hiddenInput(['value' => '2'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBooking, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Phone </p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBooking, 'phone_number')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Email</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBooking, 'email')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Device model</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBooking, 'device_model')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Branch</p>

                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBooking, 'branch_id')->dropDownList($arrayBranch, ['prompt' => 'Select brach'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">

                                        <p>Model </p>

                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBooking, 'model_id')->dropDownList($arrayModels, ['prompt' => 'Select model'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">

                                        <p>Service </p>

                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBooking, 'service_id')->dropDownList($arrayService, ['prompt' => 'Select service'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Description</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBooking, 'description')->textarea(['rows' => '6'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Status</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBooking, 'status')->dropDownList(['1' => 'Order received', '2' => 'Order in progress', '3' => 'Awaiting details from client', '4' => 'Order ready for packaging', '5' => 'Order on route to delivery'],['prompt' => 'Select service'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Assigned To</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBooking, 'assigned_id')->dropDownList($arrayAssigned, ['prompt' => 'Select assigned'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform button_merge">
                                        <?= Html::submitButton(Yii::t('app', 'Save'), ['name' => 'update_booking','class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </div>
    </div>
</div>