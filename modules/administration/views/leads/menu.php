<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;
//var_dump($this->context->getRoute());exit;

    $class = (($this->context->getRoute() == 'administration/leads/enquiries') || ($this->context->getRoute() == 'administration/leads/enquiriesshow'))?'active':''; 
    $class1 = (($this->context->getRoute() == 'administration/leads/bookings') || ($this->context->getRoute() == 'administration/leads/bookingsshow'))?'active':''; 
    $class2 = (($this->context->getRoute() == 'administration/leads/mail_in') || ($this->context->getRoute() == 'administration/leads/mail_inshow'))?'active':''; 
    $class3 = ($this->context->getRoute() == 'administration/leads/pick_ups')?'active':''; 
    $class4 = (($this->context->getRoute() == 'administration/leads/corporate') || ($this->context->getRoute() == 'administration/leads/corporateshow'))?'active':''; 
?>
<style>
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        color: #464a4c;
        background-color: #fff;
        border-color: #ddd #ddd #fff;
    }
</style>
<?php Yii::$app->language = 'en-US'; ?>

<ul class="nav nav-tabs tab_title_leads">
    <li class="nav-item">
        <?= HTML::a('Enquiries', Url::home().'administration/leads/enquiries', ['class' => 'nav-link '.$class]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Bookings', Url::home().'administration/leads/bookings', ['class' => 'nav-link '.$class1]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Mail in', Url::home().'administration/leads/mail_in', ['class' => 'nav-link '.$class2]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Pick Ups', Url::home().'administration/leads/pick_ups', ['class' => 'nav-link '.$class3]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Corporate', Url::home().'administration/leads/corporate', ['class' => 'nav-link '.$class4]); ?>
    </li>
</ul>
