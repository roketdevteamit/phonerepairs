<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('message_send')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Sended',
    ]);
endif;
if (Yii::$app->session->hasFlash('message_not_send')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Not sended',
    ]);
endif;
if (Yii::$app->session->hasFlash('lead_updated')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Updated',
    ]);
endif;
if (Yii::$app->session->hasFlash('lead_not_updated')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Not updated',
    ]);
endif;
?>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu', []); ?>

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Mail in
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelMailin, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Phone</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelMailin, 'phone_number')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Email</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelMailin, 'email')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Device model</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelMailin, 'device_model')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Phone password</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelMailin, 'phone_password')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>IMEI</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelMailin, 'IMEI')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Branch </p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelMailin, 'branch_id')->dropDownList($arrayBranch, ['prompt' => 'Select brach'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Model </p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelMailin, 'model_id')->dropDownList($arrayModels, ['prompt' => 'Select model'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Service </p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelMailin, 'service_id')->dropDownList($arrayService, ['prompt' => 'Select service'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Description</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelMailin, 'description')->textarea(['rows' => '6'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform button_merge">
                                        <?= Html::submitButton(Yii::t('app', 'Save'), ['name' => 'update_mailin', 'class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i>  Email quote
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div style="height: 0px">
                                        <?= $form->field($modelNewLeadmessage, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
                                        <?= $form->field($modelNewLeadmessage, 'leads_id')->hiddenInput(['value' => $modelMailin->id])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Content</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewLeadmessage, 'content')->textarea()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform button_merge">
                                        <?= Html::submitButton(Yii::t('app', 'Sent'), ['name' => 'sent_commect', 'class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                                <div class="col-sm-12">
                                    <?php if ($modelLeadmessage) { ?>
                                        <div class="well">
                                            <?php foreach ($modelLeadmessage as $leadmessage) { ?>
                                                <div class="col-sm-12">
                                                    <p>
                                                        <i><b><?= $leadmessage['email'] . ' - ' . $leadmessage['date_create']; ?></b></i>
                                                    </p>
                                                    <p><?= $leadmessage['content']; ?></p>
                                                </div>
                                                <div style="clear: both"></div>
                                                <hr>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>