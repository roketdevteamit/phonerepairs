<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('message_send')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Sended',
    ]);
endif;
if (Yii::$app->session->hasFlash('message_not_send')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Not sended',
    ]);
endif;
?>
<?php
if (Yii::$app->session->hasFlash('lead_updated')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Updated',
    ]);
endif;
if (Yii::$app->session->hasFlash('lead_not_updated')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Not updated',
    ]);
endif;
?>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu', []); ?>

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa fa-arrow-down" data-action="show"> </i> Corporate
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelCorporate, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Company name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelCorporate, 'company_r_name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Phone number</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelCorporate, 'phone_number')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Email</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelCorporate, 'email')->input('email')->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Company_url</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelCorporate, 'company_url')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Description</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelCorporate, 'description')->textarea()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform button_merge">
                                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['name' => 'update_corporate', 'class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa fa-arrow-down" data-action="show"> </i>  Email quote
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div style="height: 0px">
                                    <?= $form->field($modelNewLeadmessage, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
                                    <?= $form->field($modelNewLeadmessage, 'leads_id')->hiddenInput(['value' => $modelCorporate->id])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Content</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                    <?= $form->field($modelNewLeadmessage, 'content')->textarea(); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform button_merge">
                                    <?= Html::submitButton(Yii::t('app', 'Sent'), ['name' => 'sent_commect', 'class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                                <div class="col-sm-12">
                                    <?php if ($modelLeadmessage) { ?>
                                        <div class="well">
                                            <?php foreach ($modelLeadmessage as $leadmessage) { ?>
                                                <div class="col-sm-12">
                                                    <p>
                                                        <i><b><?= $leadmessage['email'] . ' - ' . $leadmessage['date_create']; ?></b></i>
                                                    </p>
                                                    <p><?= $leadmessage['content']; ?></p>
                                                </div>
                                                <div style="clear: both"></div>
                                                <hr>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>