<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;

?>

<div class="dashboard-container">
    <div class="container">
        <a href="<?= Url::home() . 'administration/branches/index'; ?>" class="img_back">
            <img src="/images/admin/back-button-png-image-59238.png">
        </a>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i>Update branch
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Address</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelBranches, 'address')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Address Street</p>
                                    </div>
                                    <div class="col-xs-12  button_seting">
                                        <?= $form->field($modelBranches, 'address_street')->widget(TinyMce::className(), [
                                            'options' => ['rows' => 3],
                                            'language' => 'en_GB',
                                            'clientOptions' => [
                                                'plugins' => [
                                                    //                                                    "advlist autolink lists link charmap print preview anchor",
                                                    //                                                    "searchreplace visualblocks code fullscreen",
                                                    "code",
                                                    //                                                    "insertdatetime media table contextmenu paste"
                                                ],
                                                'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                            ]
                                        ])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Telephone</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelBranches, 'telephone')->textinput()->label('Call branch directly')->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Open</p>
                                    </div>
                                    <div class="col-xs-12 button_seting">
                                        <?= $form->field($modelBranches, 'open')->widget(TinyMce::className(), [
                                            'options' => ['rows' => 3],
                                            'language' => 'en_GB',
                                            'clientOptions' => [
                                                'plugins' => [
                                                    //                                                    "advlist autolink lists link charmap print preview anchor",
                                                    //                                                    "searchreplace visualblocks code fullscreen",
                                                    "code",
                                                    //                                                    "insertdatetime media table contextmenu paste"
                                                ],
                                                'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                            ]
                                        ])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>