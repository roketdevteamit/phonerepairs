<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('add_branch')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Branch added!',
    ]);
endif;
if (Yii::$app->session->hasFlash('not_add_branch')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Branch not added!',
    ]);
endif;

if (Yii::$app->session->hasFlash('update_branch')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Branch updated!',
    ]);
endif;
if (Yii::$app->session->hasFlash('not_update_branch')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Branch not updated!',
    ]);
endif;

if (Yii::$app->session->hasFlash('delete_branch')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Branch deleted!',
    ]);
endif;
if (Yii::$app->session->hasFlash('not_delete_branch')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Branch not deleted!',
    ]);
endif;
?>

<div class="dashboard-container">
    <div class="container">

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Create branch
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Address</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBranches, 'address')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Address Street</p>
                                    </div>
                                    <div class="col-xs-12 button_seting">
                                        <?= $form->field($modelNewBranches, 'address_street')->widget(TinyMce::className(), [
                                            'options' => ['rows' => 3],
                                            'language' => 'en_GB',
                                            'clientOptions' => [
                                                'plugins' => [
                                                    //                                                    "advlist autolink lists link charmap print preview anchor",
                                                    //                                                    "searchreplace visualblocks code fullscreen",
                                                    "code",
                                                    //                                                    "insertdatetime media table contextmenu paste"
                                                ],
                                                'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                            ]
                                        ])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Telephone</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewBranches, 'telephone')->textinput()->label('Call branch directly')->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Open</p>
                                    </div>
                                    <div class="col-xs-12 button_seting">
                                        <?= $form->field($modelNewBranches, 'open')->widget(TinyMce::className(), [
                                            'options' => ['rows' => 3],
                                            'language' => 'en_GB',
                                            'clientOptions' => [
                                                'plugins' => [
                                                    //                                                    "advlist autolink lists link charmap print preview anchor",
                                                    //                                                    "searchreplace visualblocks code fullscreen",
                                                    "code",
                                                    //                                                    "insertdatetime media table contextmenu paste"
                                                ],
                                                'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                            ]
                                        ])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa" data-action="show"> </i> Branches
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelBranches,
                                        'columns' => [
                                            [
                                                'attribute' => '#',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    return $modelModels['id'];
                                                }
                                            ],
                                            'address',
                                            [
                                                'attribute' => 'address_street',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    $value = strip_tags($modelModels->address_street);
                                                    return $value;

                                                }
                                           ],
                                            [
                                                'attribute' => 'open',
                                                'format' => 'html',
                                                'value' => function ($modelModels) {
                                                    $value = strip_tags($modelModels->open);
                                                    return $value;

                                                }
                                           ],
                                            'telephone',
                                            'date_create',
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{delete} {update}',
                                                'buttons' => [
                                                    'update' => function ($url, $modelBranches) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                                            'branchupdate?id=' . $modelBranches['id']);
                                                    },
                                                    'delete' => function ($url, $modelBranches) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>',
                                                            'branchdelete?id=' . $modelBranches['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>