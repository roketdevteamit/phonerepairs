<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('add_user')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'User added!',
    ]);
endif;
if (Yii::$app->session->hasFlash('not_add_user')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'User not added!',
    ]);
endif;
?>

<div class="dashboard-container">
    <div class="container padding">
        <?= $this->render('menu'); ?>
        
        <div class="dashboard-wrapper-lg">
            <div class="container">
                <div class="col-sm-12 table_block" >
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa" data-action="show"> </i> Contact
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelUser,
                                        'columns' => [
                                            'date_create',
                                            'name',
                                            'email',
                                            'status',
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{show} {delete}',
                                                'buttons' => [
                                                    'show' => function ($url, $modelUser) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-eye-open"></span>',
                                                            'contactshow?id=' . $modelUser['id']);
                                                    },
                                                    'delete' => function ($url, $modelUser) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>',
                                                            'contactdelete?id=' . $modelUser['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>