<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;

?>

<?php
if (Yii::$app->session->hasFlash('news_sended')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Your Campaign is sent!',
    ]);
endif;
if (Yii::$app->session->hasFlash('news_not_sended')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Sorry but your Campaign is not sent!!',
    ]);
endif;
?>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu'); ?>
        
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-xs-12 col-sm-8 block_style">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i>Add Newsletter
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                     <?php $form = ActiveForm::begin([
                                         'options' => [
                                     ]]
                                     ); ?>
                                    <?= $form->field($modelNewNewsletter, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
                                    <?= $form->field($modelNewNewsletter, 'content')->widget(TinyMce::className(), [
                                            'options' => ['rows' => 6],
                                            'language' => 'en_GB',
                                            'clientOptions' => [
                                                'plugins' => [
                                                    "code",
                                                ],
                                                'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                            ]
                                        ]);?>
                                    
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                        <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary pull-right']) ?>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-12 table_border">
                    <?= GridView::widget([
                        'dataProvider' => $modelNewsletter,
                        'columns' => [
                            'id',
                            [
                                'attribute' => 'content',
                                'format' => 'html',
                                'value' => function ($modelModels) {
                                    $value = strip_tags($modelModels->content);
                                    return yii\helpers\StringHelper::truncate($value, 50);

                                }
                           ],
                            'date_create',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{show}',
                                'buttons' => [
                                    'show' => function ($url, $modelNewsletter) {
                                        return Html::a(
                                            '<span class="glyphicon glyphicon-eye-open"></span>',
                                            'newsshow?id=' . $modelNewsletter['id']);
                                    },
                                ],
                            ],
                        ],
                    ]) ?>
                </div>
                
            </div>
        </div>
    </div>
</div>