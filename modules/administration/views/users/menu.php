<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
<?php

    $class = ($this->context->getRoute() == 'administration/users/contact')?'active':'';
    $class2 = ($this->context->getRoute() == 'administration/users/newsletter')?'active':'';
    
?>
<ul class="nav nav-tabs tab_title_admin">
    <li class="nav-item">
        <?php echo HTML::a(\Yii::t('app', 'Contact'), Url::home().'administration/users/contact',['class' => 'nav-link '.$class]); ?>
    </li>
    <li class="nav-item">
        <?php echo HTML::a(\Yii::t('app', 'Newsletter'), Url::home().'administration/users/newsletter', ['class' => 'nav-link '.$class2]); ?>
    </li>
</ul>
