<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;

?>

<div class="dashboard-container">
    <div class="container">
        <a href="<?= Url::home().'administration/users/contact'; ?>" class="img_back">
        <img src="/images/admin/back-button-png-image-59238.png">
        </a>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-xs-12 col-sm-8 block_style">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i>Update page
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                     <?php $form = ActiveForm::begin([
                                         'options' => [
                                         'class' => 'userform'
                                     ]]
                                     ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Username</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelUser, 'username')->textinput()->label(false); ?>
                                    </div>
                                        <div class="col-xs-12 col-sm-4">
                                            <p>Email</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelUser, 'email')->input('email')->label(false); ?>
                                        </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Type</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelUser, 'type')->dropDownList(['user' => 'User', 'manager' => 'Manager', 'admin' => 'Admin'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary pull-right']) ?>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12">
                   <?= GridView::widget([
                        'dataProvider' => $modelLeads,
                        'tableOptions'=>['class'=>'table table-striped table-bordered table_border'],
                        'columns' => [
                            [
                                'attribute' => 'id',
                                'format' => 'html',
                                'value' => function ($modelLeads) {
                                    $src = ''; 
                                    switch($modelLeads['leads_type']){
                                        case 0:
                                            $src = '/administration/leads/enquiriesshow?id='.$modelLeads['id'];
                                            break;
                                        case 1:
                                            $src = '/administration/leads/mail_inshow?id='.$modelLeads['id'];
                                            break;
                                        case 2:
                                            $src = '/administration/leads/bookingsshow?id='.$modelLeads['id']; 
                                            break;
                                        case 4:
                                            $src = '/administration/leads/corporateshow?id='.$modelLeads['id']; 
                                            break;
                                    }
                                    
                                    return '<a href="'.$src.'">'.$modelLeads['key'] . '' . $modelLeads['id'].'</a>';
                                }
                            ],
                            'name',
                            'email',
                            'phone_number',
                            'date_create',
                            [
                                'attribute' => 'Service',
                                'value' => function ($modelLeads) {
                                    if ($modelLeads['service_id'] != 0) {
                                        return \app\widgets\GetServiceNameWidget::widget(['service_id' => $modelLeads['service_id']]);
                                    }
                                }
                            ],
                            [
                                'attribute' => 'Model',
                                'value' => function ($modelLeads) {
                                    if ($modelLeads['model_id'] != 0) {
                                        return \app\widgets\GetModelsNameWidget::widget(['models_id' => $modelLeads['model_id']]);
                                    }

                                }
                            ],
                            [
                                'attribute' => 'Source',
                                'value' => function ($modelLeads) {
                                    if (($modelLeads['hear_us'] != '') && ($modelLeads['hear_us'] != null)) {
                                        $hear_us = ['1' => 'Google', '2' => 'Search Engine', '3' => 'External Website', '4' => 'Word of Mouth', '5' => 'Leaflets/Newspaper', '6' => 'Yell.com', '7' => 'Bing', '8' => 'Yahoo'];
                                        return $hear_us[(int)$modelLeads['hear_us']];
                                    } else {
                                        return '';
                                    }
                                }
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>