<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;

?>

<div class="dashboard-container">
    <div class="container">
        <a href="<?= Url::home().'administration/users/index'; ?>" class="img_back">
        <img src="/images/admin/back-button-png-image-59238.png">
        </a>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-xs-12 col-sm-8 block_style">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i>Update page
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                     <?php $form = ActiveForm::begin([
                                         'options' => [
                                         'class' => 'userform'
                                     ]]
                                     ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Username</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelUser, 'username')->textinput()->label(false); ?>
                                    </div>
                                        <div class="col-xs-12 col-sm-4">
                                            <p>Email</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelUser, 'email')->input('email')->label(false); ?>
                                        </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Password</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelUser, 'password')->input('password')->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Password Repeat</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelUser, 'password_repeat')->input('password')->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Type</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelUser, 'type')->dropDownList(['user' => 'User', 'manager' => 'Manager', 'admin' => 'Admin'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary pull-right']) ?>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>