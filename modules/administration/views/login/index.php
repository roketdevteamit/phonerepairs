<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;

?>
<?php Yii::$app->language = 'en-US'; ?>
<div class="container">
    <div class="col-sm-6" style="margin:0 auto;margin-top:50px;float:none;">
        <div class="widget">
            <div class="widget-header">
                <div class="title">
                    Admin Sign In
                </div>
            </div>
            <div class=" widget-body">
                <div class="row">
                    <div class="col-sm-12 ">
                        <?php $form = ActiveForm::begin([
//                            'id' => 'login-form',
//                            'layout' => 'horizontal',
//                            'fieldConfig' => [
//                                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
//                                'labelOptions' => ['class' => 'col-lg-1 control-label'],
//                            ],
                        ]); 
                        ?>

                            <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder'=>'Enter your email','class'=>'form-control form_admin'])->label(false) ?>

                            <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Enter your password','class'=>'form-control form_admin'])->label(false) ?>

<!--                            --><?php //= $form->field($model, 'rememberMe')->checkbox([
//                                'template' => "<div>{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
//                            ]) ?>

                            <div class="form-group">
                                    <?= Html::submitButton('Sign In', ['class' => 'btn btn-primary sign_in_button', 'name' => 'login-button']) ?>
                            </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>