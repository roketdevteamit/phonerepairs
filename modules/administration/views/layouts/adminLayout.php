<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\widgets\LangWidget;

use app\assets\AppAsset;

AppAsset::register($this);

use app\assets\AdminappAsset;

AdminappAsset::register($this);
Yii::$app->language = 'en-US';
$configData = yii\helpers\ArrayHelper::map(app\models\Setting::find()->all(), 'key', 'value');
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="PhoneRepairs4u offers same day service on iPhones and Samsung devices, we also offer repair services for almost all smart phone repairs. Our repair services span from iPhone 3g,4,4s,5,5c,5s, Samsung S3,S4,S5" />
        <meta name="keywords" content=""/>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/images/icons/favicon.ico?v1" type="image/x-icon" />
        <link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <?php
    NavBar::begin(['options' => ['style' => 'display:none;']]);
    NavBar::end();
    ?>
    <?php $repairsArray = app\models\Repairs::find()->asArray()->orderBy('sort ASC')->all(); ?>
    <?php $modelPages = \app\models\Pages::find()->where(['status' => 1])->asArray()->orderBy('sort ASC')->all(); ?>
    <div class="">
        <div class="header_style">
            <div class="col-xs-12 col-sm-4 header_style_text">
                <div>
                    <a href="<?= Url::home(); ?>administration/home">
                        <img src="<?= Url::home(); ?>images/main-logo-2.svg">
                    </a>

                    <div class="clearfix"></div>
                    <h3>SAME DAY REPAIR CENTRE</h3>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 logo_style">
                <img src="<?= Url::home(); ?>images/same-day-logo.svg">
            </div>
            <div class="col-xs-12 col-sm-4 right_blocks_header ">
                <div>

                    <h2 class="text-white text-right small-only-text-center right_text_style">
                        <?php
                        if (isset($configData['telephone'])) {
                        ?>
                        <i class="fa fa-phone" aria-hidden="true"></i>
                        <a class="style_telephone_header"
                           href="tel:02033022930">
                            <?= strip_tags($configData['telephone']) ?>
                            <?php } else {
                            } ?>
                        </a>
                        <div class="clearfix height_blocks"></div>
                        <p class="text-right small-only-text-center ">
                            <?php
                            if (isset($configData['open_hours'])) {
                                ?>
                                <?= strip_tags($configData['open_hours']) ?>
                            <?php } else {
                            } ?>
                        </p>
                        <?php if (isset($configData['location'])){ ?>
                        <p class="text-right small-only-text-center">Our
                            Locations:
                            <?= strip_tags($configData['location']); ?>
                            <?php } else { ?>
                            <?php } ?>
                        </p>
                    </h2>
                    <ul class="nav navbar-nav  social_links">
                        <?php
                        if (isset($configData['facebook'])) {
                            ?>
                            <li>
                                <a target="_blank" href="<?= strip_tags($configData['facebook']) ?>">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>

                            </li>
                        <?php } else {
                        } ?>
                        <?php
                        if (isset($configData['twitter'])) {
                            ?>
                            <li><a target="_blank" href="<?= strip_tags($configData['twitter']) ?>"><i
                                            class="fa fa-twitter"
                                            aria-hidden="true"></i></a>
                            </li>
                        <?php } else {
                        } ?>
                        <?php
                        if (isset($configData['linkedin'])) {
                            ?>
                            <li><a target="_blank" href="<?= strip_tags($configData['linkedin']) ?>"><i
                                            class="fa fa-instagram"
                                            aria-hidden="true"></i>
                                </a>
                            </li>
                        <?php } else {
                        } ?>
                        <?php
                        if (isset($configData['linkedin'])) {
                            ?>
                            <li><a target="_blank" href="<?= strip_tags($configData['telephoneinstagram']) ?>"><i
                                            class="fa fa-linkedin"
                                            aria-hidden="true"></i>
                                </a></li>
                        <?php } else {
                        } ?>
                    </ul>
                </div>
            </div>
            <div style="clear:both;"></div>
        </div>
    </div>

    <?php if (!\Yii::$app->user->isGuest) { ?>

        <nav class="navbar navbar-default admin_menu_header">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed admin_menu_mob" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <p class="block_menu admin_block_menu_text">MENU</p>
                        <div class="block_menu">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar" style="background-color: black"></span>
                            <span class="icon-bar" style="background-color: black"></span>
                            <span class="icon-bar" style="background-color: black"></span>
                        </div>
                    </button>

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse admin_style_mob_menu" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav" style="width: 100%">
                        <li><a href="<?= Url::home(); ?>administration/home">Admin Home</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Leads
                                <span class="after_style"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Enquiries'), Url::home() . 'administration/leads/enquiries'); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Bookings'), Url::home() . 'administration/leads/bookings'); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Mail in'), Url::home() . 'administration/leads/mail_in'); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Pick Ups'), Url::home() . 'administration/leads/pick_ups'); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Corporate'), Url::home() . 'administration/leads/corporate'); ?></a>
                                </li>
                            </ul>
                        </li>
                        <li><?php echo HTML::a('Users', Url::home() . 'administration/users/index'); ?></li>
                        <li><?php echo HTML::a('Branches', Url::home().'administration/branches/index'); ?></li>
                        <li><?php echo HTML::a('Contact', Url::home().'administration/users/contact'); ?></li>
                        <li><?php echo HTML::a('Report', Url::home().'administration/report/index'); ?></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Repairs Pages
                                <span class="after_style"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Repair type'), Url::home() . 'administration/repairs/type'); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Brand'), Url::home() . 'administration/repairs/brand'); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Services'), Url::home() . 'administration/repairs/services'); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Models'), Url::home() . 'administration/repairs/models'); ?></a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false"><i class="fa"></i> Manage site
                                <span class="after_style"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Site information'), Url::home() . 'administration/default/setting'); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Pages'), Url::home() . 'administration/default/pages'); ?></a>
                                </li>
                                <li>
                                    <a href="#"><?php echo HTML::a(\Yii::t('app', 'Slider home'), Url::home() . 'administration/default/slider'); ?></a>
                                </li>
                            </ul>
                        </li>

                        <?php if (!Yii::$app->user->isGuest) {
                            $str = strpos(Yii::$app->user->identity->email, "@");
                            $username = substr(Yii::$app->user->identity->email, 0, $str); ?>
                            <li class="dropdown  user_login">
                                <a class="dropdown-toggle admin_name" data-toggle="dropdown" href="#"><span
                                            class="glyphicon glyphicon-user padding_logout"></span><?= $username; ?>
                                    <span class="after_style"></span></a>
                                <ul class="dropdown-menu button_logout">
                                    <li>
                                        <?= Html::beginForm(['/site/logout'], 'post') ?>
                                        <?= Html::submitButton(
                                            'Logout') ?>
                                        <?= Html::endForm() ?>
                                    </li>
                                </ul>
                            </li>
                        <?php } else { ?>
                            <!--                    <li><a href="--><? //= Url::home() . 'site/signup'; ?><!--"><span class="glyphicon glyphicon-user"></span>-->
                            <!--                            Sign Up</a></li>-->
                            <li><a href="<?= Url::home() . 'site/login'; ?>"><span
                                            class="glyphicon glyphicon-log-in"></span>
                                    Login</a></li>
                        <?php } ?>
                </div>
                </ul>

            </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

    <?php } ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
    </div>

    <div class="border_style"></div>
    <div class="container container_style">
        <div class="col-xs-12 product_style">
            <?php foreach ($repairsArray as $repairs) { ?>
                <?php
                $img_src = "/images/pages/default.jpg";
                if (($repairs['img_src'] != '') && ($repairs['img_src'] != null)) {
                    $img_src = "/images/repair/" . $repairs['img_src'];
                }
                ?>
                <div class="col-xs-6 col-sm-3 mob1">
                    <a href="<?= Url::home() . 'repairs/' . $repairs['slug']; ?>">
                        <img src="<?= $img_src; ?>">
                        <p>
                            <?= $repairs['name']; ?>
                        </p>
                    </a>
                </div>
            <?php } ?>
            <div class="col-xs-6 col-sm-3 mob1">
                <a href="<?= Url::home(); ?>makeenquiry">
                                        <img src="<?= Url::home() . 'images/repair/'; ?>data-recovery.jpg">
                                        <p>
                                                Data Recovery
                                           </p>
                                    </a>
                           </div>
        </div>
    </div>
    <div class="border_style"></div>

    <div class="container container_style padding_0">
    <div class="col-sm-12 img_black padding_0">
        <img class="grow col-xs-4 col-sm-2 black1" src="/images/brand/brand-asus.png">
        <img class="grow col-xs-4 col-sm-2 black2" src="/images/brand/brand-acer.png">
        <img class="grow col-xs-4 col-sm-2 black3" src="/images/brand/brand-samsung.png">
        <img class="grow col-xs-4 col-sm-2 black4" src="/images/brand/brand-apple.png">
        <img class="grow col-xs-4 col-sm-2 black5" src="/images/brand/brand-nokia.png">
        <img class="grow col-xs-4 col-sm-2 black6" src="/images/brand/brand-sony.png">
        <!--                    <img class="grow" src="http://static.phonerepairs4u.co.uk/assets/images/brand-hp.png">-->

    </div>
</div>

    <footer class="footer">

    <div class="container footer_body">
        <div class="footer_menu1 col-xs-12 col-sm-2">
            <p><a href="/page/laptop">Laptop Repair</a></p>
            <p><a href="/page/book-repair">Book Your Repair!</a></p>
        </div>
        <div class="footer_menu2 col-xs-12 col-sm-3">
            <p><a href="/page/mail-your-device">Mail Your Device</a></p>
            <p><a href="/page/corporate">Corporate Repairs</a></p>
            <p><a href="/page/contact-us">Contact Us</a></p>
        </div>
        <div class="footer_adres col-xs-12 col-sm-7">
            <p class="add">
                <?php
                if (isset($configData['location'])) {
                    ?>
                    <?= strip_tags($configData['location']) ?>
                <?php } else {
                } ?> </p>
            
            <?php if (isset($configData['location_address'])) { ?>
                <?= $configData['location_address'] ?>
            <?php } ?>
            
            <p class="tel">
                T.
            <?php if (isset($configData['telephone'])) { ?>
                <?= strip_tags($configData['telephone']); ?>
            <?php } ?>
            </p>
        </div>
        <div class="foter_style_height col-sm-12"></div>
        <div class="footer_information col-xs-12 col-sm-6">
            <p class="hover_style"><a style="color: white" href="<?= Url::home(); ?>page/terms-conditions">Terms & Conditions</a></p>
            <p>Copyright 2017 © PhoneRepairs4u.co.uk</p>
        </div>
        <div class="footer_mob col-xs-12 col-sm-6">
            <a class="style_telephone_header" href="tel:02033022930">
                <?php if (isset($configData['telephone'])) {
                    ?>
                    <p>Call now on:</p>
                    <?= strip_tags($configData['telephone']) ?>
                <?php } else {
                } ?>
            </a>
        </div>
        <div class="social_style col-xs-12 col-sm-12">
            <ul class="nav navbar-nav  social_links">
                <?php
                if (isset($configData['facebook'])) {
                    ?>
                    <li><a target="_blank" href="<?= strip_tags($configData['facebook']) ?>"><i
                                    class="fa fa-facebook"
                                    aria-hidden="true"></i></a>
                    </li>
                <?php } else { ?>
                <?php } ?>
                <?php
                if (isset($configData['twitter'])) {
                    ?>
                    <li><a target="_blank" href="<?= strip_tags($configData['twitter']) ?>"><i class="fa fa-twitter"
                                                                                               aria-hidden="true"></i></a>
                    </li>
                <?php } else {
                } ?>
                <?php if (isset($configData['linkedin'])) { ?>
                    <li><a target="_blank" href="<?= strip_tags($configData['linkedin']) ?>"><i
                                    class="fa fa-instagram"
                                    aria-hidden="true"></i>
                        </a>
                    </li>
                <?php } ?>
                <?php
                if (isset($configData['telephoneinstagram'])) {
                    ?>
                    <li><a target="_blank" href="<?= strip_tags($configData['telephoneinstagram']) ?>"><i
                                    class="fa fa-linkedin"
                                    aria-hidden="true"></i>
                        </a></li>
                <?php } else {
                } ?>
            </ul>
        </div>
    </div>
</footer>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>