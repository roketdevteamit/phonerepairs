<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;

$hear_us = ['1' => 'Google', '2' => 'Search Engine', '3' => 'External Website', '4' => 'Word of Mouth', '5' => 'Leaflets/Newspaper', '6' => 'Yell.com', '7' => 'Bing', '8' => 'Yahoo'];
?>
<div class="container class_hom_page">
    <?php if ($modelTodayEnquiries->getModels()) { ?>
        <p class="title_p">Today Enquiries: <?= $enquiries_count; ?></p>
        <?php if($hear_us_e){ ?>
            <?php foreach($hear_us_e as $hear_id => $hear_array){ ?>
                    <p><b><?= $hear_us[$hear_id]; ?>:<?= count($hear_array); ?></b></p>
            <?php } ?>
        <?php } ?>
        <?= GridView::widget([
            'dataProvider' => $modelTodayEnquiries,
            'tableOptions'=>['class'=>'table table-striped table-bordered table_border'],
            'columns' => [
                [
                    'attribute' => '#',
                    'format' => 'html',
                    'value' => function ($modelTodayEnquiries) {    
                        return '<a href="/administration/leads/enquiriesshow?id='.$modelTodayEnquiries['id'].'">'.$modelTodayEnquiries['key'].''.$modelTodayEnquiries['id'].'</a>';
                    }
                ],
                'name',
                'email',
                'phone_number',
                'date_create',
                [
                    'attribute' => 'Source',
                    'value' => function ($modelTodayEnquiries) {
                        if (($modelTodayEnquiries['hear_us'] != '') && ($modelTodayEnquiries['hear_us'] != null)) {
                            $hear_us = ['1' => 'Google', '2' => 'Search Engine', '3' => 'External Website', '4' => 'Word of Mouth', '5' => 'Leaflets/Newspaper', '6' => 'Yell.com', '7' => 'Bing', '8' => 'Yahoo'];
                            return $hear_us[(int)$modelTodayEnquiries['hear_us']];
                        } else {
                            return '';
                        }
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete} ',
                    'buttons' => [
                        'update' => function ($url, $modelTodayEnquiries) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>',
                                'leads/enquiriesshow?id=' . $modelTodayEnquiries['id']);
                        },
                        'delete' => function ($url, $modelTodayEnquiries) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>',
                                'leads/enquiriesdelete?id=' . $modelTodayEnquiries['id']);
                        },
                    ],
                ],
            ],
        ]) ?>
    <?php } else { ?>
        <p class="title_p">There are currently no new enquiries to view.</p>
        <a href="/administration/leads/enquiries">
            If you would like to view existing enquiries, just visit the enquiries page.
        </a>
    <?php } ?>


    <?php if ($modelTodayBooking->getModels()) { ?>
        <p class="title_p">Today Bookings: <?= $booking_count; ?></p>
        <?php if($hear_us_b){ ?>
            <?php foreach($hear_us_b as $hear_id => $hear_array){ ?>
                    <p><b><?= $hear_us[$hear_id]; ?>:<?= count($hear_array); ?></b></p>
            <?php } ?>
        <?php } ?>
        <?= GridView::widget([
            'dataProvider' => $modelTodayBooking,
            'tableOptions'=>['class'=>'table table-striped table-bordered table_border'],
            'columns' => [
                [
                    'attribute' => '#',
                    'format' => 'html',
                    'value' => function ($modelTodayBooking) {    
                        return '<a href="/administration/leads/bookingsshow?id='.$modelTodayBooking['id'].'">'.$modelTodayBooking['key'].''.$modelTodayBooking['id'].'</a>';
                    }
                ],
                'name',
                'email',
                'phone_number',
                [
                    'attribute' => 'Service',
                    'value' => function ($modelTodayBooking) {
                        if ($modelTodayBooking['service_id'] != 0) {
                            return \app\widgets\GetServiceNameWidget::widget(['service_id' => $modelTodayBooking['service_id']]);
                        }
                    }
                ],
                [
                    'attribute' => 'Model',
                    'value' => function ($modelTodayBooking) {
                        if ($modelTodayBooking['model_id'] != 0) {
                            return \app\widgets\GetModelsNameWidget::widget(['models_id' => $modelTodayBooking['model_id']]);
                        }

                    }
                ],
                'date_create',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete} ',
                    'buttons' => [
                        'update' => function ($url,$modelTodayBooking) {
                                return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>', 
                                'leads/bookingsshow?id='.$modelTodayBooking['id']);
                        },
                        'delete' => function ($url,$modelTodayBooking) {
                                return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>', 
                                'leads/bookingsdelete?id='.$modelTodayBooking['id']);
                        },
                    ],
                ],
            ],
        ]) ?>
    <?php } else { ?>
        <p class="title_p">There are currently no new bookings to view.</p>
        <a href="/administration/leads/bookings">
            If you would like to view existing bookings, just visit the bookings page.
        </a>
    <?php } ?>


    <?php if ($modelTodayMailin->getModels()) { ?>
        <p class="title_p">Today Mail in: <?= $mailin_count; ?></p>
        <?php if($hear_us_m){ ?>
            <?php foreach($hear_us_m as $hear_id => $hear_array){ ?>
                    <p><b><?= $hear_us[$hear_id]; ?>:<?= count($hear_array); ?></b></p>
            <?php } ?>
        <?php } ?>
        <?= GridView::widget([
            'dataProvider' => $modelTodayMailin,
            'tableOptions'=>['class'=>'table table-striped table-bordered table_border'],
            'columns' => [
                [
                    'attribute' => '#',
                    'format' => 'html',
                    'value' => function ($modelTodayMailin) {    
                        return '<a href="/administration/leads/mail_inshow?id='.$modelTodayMailin['id'].'">'.$modelTodayMailin['key'].''.$modelTodayMailin['id'].'</a>';
                    }
                ],
                'name',
                'email',
                'phone_number',
                'date_create',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete} ',
                    'buttons' => [
                        'update' => function ($url,$modelTodayMailin) {
                                return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>', 
                                'leads/mail_inshow?id='.$modelTodayMailin['id']);
                        },
                        'delete' => function ($url,$modelTodayMailin) {
                                return Html::a(
                                '<span class="glyphicon glyphicon-trash"></span>', 
                                'leads/maildelete?id='.$modelTodayMailin['id']);
                        },
                    ],
                ],
            ],
        ]) ?>
    <?php } else { ?>
        <p class="title_p">
            There are currently no new contact message to view.
        </p>
        <a href="/administration/leads/mail_in">
            If you would like to view existing contact messages, just visit the contact messages page.
        </a>
    <?php } ?>
</div>
