<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>
<div class="dashboard-container">
    <?php
        if(Yii::$app->session->hasFlash('update_setting')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Information updated',
                ]);
        endif;
        if(Yii::$app->session->hasFlash('add_setting')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Information added!',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('not_add_setting')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Information not added!',
                ]);
        endif; 
        
        
        if(Yii::$app->session->hasFlash('delete_setting')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-info',
                        ],
                        'body' => 'Information deleted!',
                ]);
        endif; 
        if(Yii::$app->session->hasFlash('not_delete_setting')):
                echo Alert::widget([
                        'options' => [
                                'class' => 'alert-error',
                        ],
                        'body' => 'Information not updated!',
                ]);
        endif; 
    ?>
    <div class="container">
        
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-xs-12 col-sm-8 block_style">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Create
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Key</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewSetting, 'key')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewSetting, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Value</p>
                                    </div>
                                    <div class="col-xs-12   button_seting">
                                        <?= $form->field($modelNewSetting, 'value')->widget(TinyMce::className(), [
                                            'options' => ['rows' => 6],
                                            'language' => 'en_GB',
                                            'clientOptions' => [
                                                'plugins' => [
//                                                    "advlist autolink lists link charmap print preview anchor",
//                                                    "searchreplace visualblocks code fullscreen",
                                                    "code",
//                                                    "insertdatetime media table contextmenu paste"
                                                ],
                                                'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                            ]
                                        ])->label(false);?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-info']) ?>
                                    </div>
                                        <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-12 table_block" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Change
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelSetting,
                                        'columns' => [
                                            'name',
                                            'key',
                                            [
                                                'attribute' => 'value',
                                                'value' => function ($modelSetting) {
                                                    $value = strip_tags($modelSetting->value);
                                                    return StringHelper::truncate($value, 50);
                                                }
                                            ],
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{delete} {update}',
                                                'buttons' => [
                                                    'update' => function ($url,$modelSetting) {
                                                            return Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>', 
                                                            'settingupdate?id='.$modelSetting['id']);
                                                    },
                                                    'delete' => function ($url,$modelSetting) {
                                                            return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>', 
                                                            'settingdelete?id='.$modelSetting['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>