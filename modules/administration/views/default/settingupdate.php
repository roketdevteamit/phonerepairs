<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
?>
<div class="dashboard-container">
    <div class="container">
        <a href="<?= Url::home(); ?>administration/default/setting" class="img_back">
            <img src="/web/images/admin/back-button-png-image-59238.png">
        </a>
        
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                    <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                        <div class="widget">
                            <div class="widget-header">
                                <div class="title title_form" style="height:40px;">
                                    <i class="fa " data-action="show"> </i>Update
                                </div>
                            </div>
                            <div class=" widget-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <?php $form = ActiveForm::begin(
                                            [
                                                'options' => [
                                                    'class' => 'userform'
                                                ]]
                                        ); ?>
                                        <div class="col-xs-12 col-sm-4">
                                            <p>Key</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-8">
                                            <?= $form->field($modelSetting, 'key')->textinput()->label(false); ?>
                                        </div>
                                        <div class="col-xs-12 col-sm-4">
                                            <p>Name</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-8">
                                            <?= $form->field($modelSetting, 'name')->textinput()->label(false); ?>
                                        </div>
                                        <div class="col-xs-12 col-sm-4">
                                            <p>Value</p>
                                        </div>
                                        <div class="col-xs-12 button_seting">
                                            <?= $form->field($modelSetting, 'value')->widget(TinyMce::className(), [
                                            'options' => ['rows' => 6],
                                            'language' => 'en_GB',
                                            'clientOptions' => [
                                                'plugins' => [
//                                                    "advlist autolink lists link charmap print preview anchor",
//                                                    "searchreplace visualblocks code fullscreen",
                                                    "code",
//                                                    "insertdatetime media table contextmenu paste"
                                                ],
                                                'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                            ]
                                        ])->label(false);?>
                                        </div>
                                        <div class="col-xs-12 col-sm-8 button_width userform">
                                            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary']) ?>
                                        </div>
                                            <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                
            </div>
        </div>
    </div>
</div>