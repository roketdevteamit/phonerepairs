<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('slider_changed')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Slider Changed!',
    ]);
endif;

if (Yii::$app->session->hasFlash('slider_not_changed')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Slider not changed!',
    ]);
endif;

if (Yii::$app->session->hasFlash('update_slider')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Slider image updated!',
    ]);
endif;

if (Yii::$app->session->hasFlash('not_update_slider')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Slider image not update!',
    ]);
endif;


if (Yii::$app->session->hasFlash('delete_slider')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Slider image delete!',
    ]);
endif;

if (Yii::$app->session->hasFlash('not_delete_slider')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Slider image not deleted!',
    ]);
endif;
?>

<div style="display:none;">
    <div class="table table-striped previewTemplateS">
        <div id="template" class="file-row">
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                 aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
        </div>
    </div>
</div>

<div class="dashboard-container">
    <div class="container">

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Create page
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Some_note</p>
                                    </div>
                                    <div class="col-xs-12 button_seting">
                                        <?= $form->field($modelNewSlider, 'some_note')->widget(TinyMce::className(), [
                                            'options' => ['rows' => 6],
                                            'language' => 'en_GB',
                                            'clientOptions' => [
                                                'plugins' => [
                                                    //                                                    "advlist autolink lists link charmap print preview anchor",
                                                    //                                                    "searchreplace visualblocks code fullscreen",
                                                    "code",
                                                    //                                                    "insertdatetime media table contextmenu paste"
                                                ],
                                                'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                            ]
                                        ])->label(false);  ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Image</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewSlider, 'img_src')->hiddeninput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <div class='slider_photo' style=""></div>

                                        <div class="show_slider_photo"
                                             style="display: block;background-image: url('/images/pages/default.jpg');width: 100%;padding: 7em 0 4em;background-repeat: no-repeat;background-position: center center;background-size: cover;position:relative;">
                                            <i class="fa fa-times deleteSliderUploadPhoto"
                                               style="display:none;position:absolute;right: -9px;top: -12px;font-size: 25px;cursor: pointer;"></i>
                                        </div>

                                        <a class="add_slider_photo pull-left">Add Photo</a>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Sort</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewSlider, 'sort')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 table_block" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Pages
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?php Yii::$app->language = 'en-US'; ?>
                                    <?= GridView::widget([
                                        'dataProvider' => $modelSlider,
                                        'columns' => [
                                            [
                                                'attribute' => 'some_note',
                                                'value' => function ($modelSlider) {
                                                    $content = strip_tags($modelSlider->some_note);
                                                    return StringHelper::truncate($content, 60);
                                                }
                                            ],
                                            [
                                                'attribute' => 'img_src',
                                                'format' => 'html',
                                                'value' => function ($modelSlider) {
                                                    if (($modelSlider->img_src != '') && ($modelSlider->img_src != null)) {
                                                        return '<div style="background-image:url(/images/slider/' . $modelSlider->img_src . ');width:100%;height:100px;    background-repeat: no-repeat;background-position: center;background-size: cover;">';
                                                    } else {
                                                        return '';
                                                    }
                                                }
                                            ],
                                            'sort',
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{delete} {update}',
                                                'buttons' => [
                                                    'update' => function ($url, $modelSlider) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                                            'sliderupdate?id=' . $modelSlider['id']);
                                                    },
                                                    'delete' => function ($url, $modelSlider) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-trash"></span>',
                                                            'sliderdelete?id=' . $modelSlider['id']);
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>