<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use dosamigos\ckeditor\CKEditor;
use dosamigos\tinymce\TinyMce;

    if(Yii::$app->session->hasFlash('updated')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Настройки изменены',
        ]);
    endif;
if(Yii::$app->session->hasFlash('not_updated')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'настройки не изменены',
    ]);
endif;
?>

<div style="display:none;">
    <div class="table table-striped previewTemplate" >
        <div id="template" class="file-row">
          <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
            <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
          </div>
        </div>
    </div>
</div>

<div class="dashboard-container">
    <div class="container">
        <a href="<?= Url::home().'administration/default/pages'; ?>" class="img_back">
            <img src="/web/images/admin/back-button-png-image-59238.png">
        </a>
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa" data-action="show"> </i>Change page
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelPage, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Image</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelPage, 'header_img')->hiddeninput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                            <?php 
                                                $background = "background-image: url('/images/pages/default.jpg');";
                                                if(($modelPage->header_img != '') && ($modelPage->header_img != null)){
                                                    $background = "background-image: url('/images/pages/".$modelPage->header_img."');";
                                                }
                                            ?>
                                            <div style="">
                                                <div class='header_photo' style=""></div>                                                
                                                <div class="show_header_photo" style="<?= $background; ?>display: block;width: 100%;padding: 7em 0 4em;background-repeat: no-repeat;background-position: center center;background-size: cover;position:relative;">
                                                    <i class="fa fa-times deleteUploadPhoto" style="display:none;position:absolute;right: -9px;top: -12px;font-size: 25px;cursor: pointer;"></i>
                                                </div>

                                                <a class="add_header_photo pull-left">Add Photo</a>
                                            </div>
                                    </div>
                                    <div class="col-xs-12   button_seting">
                                            <?= $form->field($modelPage, 'content')->widget(TinyMce::className(), [
                                                'options' => ['rows' => 6],
                                                'language' => 'en_GB',
                                                'clientOptions' => [
                                                    'plugins' => [
    //                                                    "advlist autolink lists link charmap print preview anchor",
    //                                                    "searchreplace visualblocks code fullscreen",
                                                        "code",
    //                                                    "insertdatetime media table contextmenu paste"
                                                    ],
                                                    'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                                ]
                                            ]);?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Sort</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                            <?= $form->field($modelPage, 'sort')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Status</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                            <?= $form->field($modelPage, 'status')->dropDownList(['1' => 'Active', '0' => 'Deactive'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                        <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                        <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>