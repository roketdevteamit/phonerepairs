<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('added_page')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Page saved!',
    ]);
endif;
if (Yii::$app->session->hasFlash('delete_pages')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Page deleted!',
    ]);
endif;
if (Yii::$app->session->hasFlash('update_page')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Page changed!',
    ]);
endif;
if (Yii::$app->session->hasFlash('not_update_page')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Page not changed!',
    ]);
endif;
?>

<div style="display:none;">
    <div class="table table-striped previewTemplate">
        <div id="template" class="file-row">
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                 aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
            </div>
        </div>
    </div>
</div>

<div class="dashboard-container">
    <div class="container">

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8 block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Create page
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewPage, 'name')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Image</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewPage, 'header_img')->hiddeninput()->label(false); ?>
                                    </div>

                                    <div class="col-xs-12 col-sm-8">
                                        <div class='header_photo' style=""></div>

                                        <div class="show_header_photo"
                                             style="display: block;background-image: url('/images/pages/default.jpg');width: 100%;padding: 7em 0 4em;background-repeat: no-repeat;background-position: center center;background-size: cover;position:relative;">
                                            <i class="fa fa-times deleteUploadPhoto"
                                               style="display:none;position:absolute;right: -9px;top: -12px;font-size: 25px;cursor: pointer;"></i>
                                        </div>

                                        <a class="add_header_photo pull-left ">Add Photo</a>
                                    </div>
                                    <div class="col-xs-12   button_seting">
                                        <?= $form->field($modelNewPage, 'content')->widget(TinyMce::className(), [
                                            'options' => ['rows' => 6],
                                            'language' => 'en_GB',
                                            'clientOptions' => [
                                                'plugins' => [
                                                    //                                                    "advlist autolink lists link charmap print preview anchor",
                                                    //                                                    "searchreplace visualblocks code fullscreen",
                                                    "code",
                                                    //                                                    "insertdatetime media table contextmenu paste"
                                                ],
                                                'toolbar' => "undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                                            ]
                                        ]); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Sort</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewPage, 'sort')->textinput()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Status</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewPage, 'status')->dropDownList(['1' => 'Active', '0' => 'Deactive'])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform">
                                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 table_block" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Pages
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelPages,
                                        'columns' => [
                                            'name',
                                            [
                                                'attribute' => 'content',
                                                'value' => function ($modelPages) {
                                                    $content = strip_tags($modelPages->content);
                                                    return StringHelper::truncate($content, 60);
                                                }
                                            ],
                                            'sort',
                                            [
                                                'attribute' => 'status',
                                                'value' => function ($modelPages) {
                                                    if ($modelPages->status == 1) {
                                                        return 'Active';
                                                    } else {
                                                        return 'Deactive';
                                                    }
                                                }
                                            ],
                                            [
                                                'class' => 'yii\grid\ActionColumn',
                                                'template' => '{delete} {update}',
                                                'buttons' => [
                                                    'update' => function ($url, $modelPages) {
                                                        return Html::a(
                                                            '<span class="glyphicon glyphicon-pencil"></span>',
                                                            'pageupdate?id=' . $modelPages['id']);
                                                    },
                                                    'delete' => function ($url, $modelPages) {
                                                        if ($modelPages->type != 1) {
                                                            return Html::a(
                                                                '<span class="glyphicon glyphicon-trash"></span>',
                                                                'pagedelete?id=' . $modelPages['id']);
                                                        }
                                                    },
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>