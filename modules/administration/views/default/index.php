<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;

?>
<div class="container">
    <div class="col-sm-5" style="margin:0 auto;margin-top:50px;float:none;">
        <div class="widget">
            <div class="widget-header">
                <div class="title" style="height:40px;">
                    Логин
                </div>
            </div>
            <div class=" widget-body">
                <div class="row">
                    <div class="col-sm-12">
                        <?php $form = ActiveForm::begin([
//                            'id' => 'login-form',
//                            'layout' => 'horizontal',
//                            'fieldConfig' => [
//                                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
//                                'labelOptions' => ['class' => 'col-lg-1 control-label'],
//                            ],
                        ]); 
                        ?>

                            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                            <?= $form->field($model, 'password')->passwordInput() ?>

                            <?= $form->field($model, 'rememberMe')->checkbox([
                                'template' => "<div>{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                            ]) ?>

                            <div class="form-group">
                                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>