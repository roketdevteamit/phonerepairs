<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>
<?php
    $class = ($this->context->getRoute() == 'administration/default/setting')?'active':'';
    $class2 = ($this->context->getRoute() == 'administration/default/pages')?'active':'';
?>

<ul>
    <>
    <li class="<?= $class; ?>">
        <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-cogs"></i>Настройки'), Url::home().'administration/default/setting'); ?>
    </li>
    <li class="<?= $class2; ?>">
        <?php echo HTML::a(\Yii::t('app', '<i class="fa fa-cogs"></i>Страници'), Url::home().'administration/default/pages'); ?>
    </li>
</ul>
            
            
            
