<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Pages;
use app\models\Setting;
use app\models\Slider;
use app\models\Leads;
use app\modules\administration\models\LoginForm;
use yii\data\ActiveDataProvider;
/**
 * Default controller for the `administration` module
 */
class DefaultController extends Controller
{
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        $this->layout = 'adminLayout';
       // var_dump(Yii::$app->user->identity->type);exit;
        if((!\Yii::$app->user->isGuest) && ((Yii::$app->user->identity->type == 'admin') || (Yii::$app->user->identity->type == 'manager'))){
            return parent::beforeAction($action);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionIndex()
    {
        if(\Yii::$app->user->isGuest){
            
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                if(Yii::$app->user->identity->type == 'admin'){
                    return $this->redirect('/administration/home');
                }else{
                    return $this->redirect('/');
                }
            }
            return $this->render('index', [
                'model' => $model,
            ]);
            
        }else{
           return $this->redirect('administration/home'); 
        }
        
    }
    
    public function actionHome()
    {        
        $dateNow = date('Y-m-d');
        $queryTodayEnquiries = Leads::find()->where(['leads_type' => 0])->andWhere(['like', 'date_create', $dateNow])->orderBy('date_create DESC');
        $enquiries_count = $queryTodayEnquiries->count();
        $modelTodayEnquiries = new ActiveDataProvider(['query' => $queryTodayEnquiries, 'pagination' => ['pageSize' => 10]]);
        $hear_us_e = [];
        foreach($modelTodayEnquiries->getModels() as $enquiries){
            if(($enquiries['hear_us'] != '') && ($enquiries['hear_us'] != 0)){
                $hear_us_e[$enquiries['hear_us']][] = '';                
            }
        }
        
        $queryTodayBooking = Leads::find()->where(['leads_type' => 2])->andWhere(['like', 'date_create', $dateNow])->orderBy('date_create DESC');;
        $booking_count = $queryTodayBooking->count();
        $modelTodayBooking = new ActiveDataProvider(['query' => $queryTodayBooking, 'pagination' => ['pageSize' => 10]]);
        $hear_us_b = [];
        foreach($modelTodayBooking->getModels() as $booking){
            if(($booking['hear_us'] != '') && ($booking['hear_us'] != 0)){
                $hear_us_b[$booking['hear_us']][] = '';                
            }
        }
        
        $queryTodayMailin = Leads::find()->where(['leads_type' => 1])->andWhere(['like', 'date_create', $dateNow])->orderBy('date_create DESC');;
        $mailin_count = $queryTodayMailin->count();
        $modelTodayMailin = new ActiveDataProvider(['query' => $queryTodayMailin, 'pagination' => ['pageSize' => 10]]);
        $hear_us_m = [];
        foreach($modelTodayMailin->getModels() as $mailin){
            if(($mailin['hear_us'] != '') && ($mailin['hear_us'] != 0)){
                $hear_us_m[$mailin['hear_us']][] = '';                
            }
        }
        
        
        return $this->render('home',[
            'modelTodayEnquiries' => $modelTodayEnquiries,
            'enquiries_count' => $enquiries_count,
            'hear_us_e' => $hear_us_e,
            
            'modelTodayBooking' => $modelTodayBooking,
            'booking_count' => $booking_count,
            'hear_us_b' => $hear_us_b,
            
            'modelTodayMailin' => $modelTodayMailin,
            'mailin_count' => $mailin_count,
            'hear_us_m' => $hear_us_m,
        ]);
    }
    
    public function actionSetting()
    {
        $modelNewSetting = new Setting();
        $modelNewSetting->scenario = 'add_setting';
        if($modelNewSetting->load(Yii::$app->request->post())){
            if($modelNewSetting->save()){
                Yii::$app->session->setFlash('add_setting');
            }else{
                Yii::$app->session->setFlash('not_add_setting');
            }
        }
        $querySetting = Setting::find();
        $modelSetting = new ActiveDataProvider(['query' => $querySetting, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('setting',[
            'modelSetting' => $modelSetting,
            'modelNewSetting' => $modelNewSetting
        ]);
    }

    public function actionSettingdelete($id=null)
    {
        
        $modelSetting = Setting::find()->where(['id' => $id])->one();
        if($modelSetting->delete()){
            Yii::$app->session->setFlash('delete_setting');
            return $this->redirect(Url::home().'administration/default/setting');
        }else{
            Yii::$app->session->setFlash('not_delete_setting');
            return $this->redirect(Url::home().'administration/default/setting');
        }
    }
    
    public function actionSettingupdate($id=null)
    {
        $modelSetting = Setting::find()->where(['id' => $id])->one();
        $modelSetting->scenario = 'update_setting';
        if($modelSetting->load(Yii::$app->request->post())){
            if($modelSetting->save()){
                Yii::$app->session->setFlash('update_setting');
                return $this->redirect(Url::home().'administration/default/setting');
            }else{
                Yii::$app->session->setFlash('not_update_setting');
            }
        }
        return $this->render('settingupdate', [
            'modelSetting' => $modelSetting,
        ]);
    }
    
    //pages begin
    public function actionPages(){  
        $modelNewPage = new Pages();
        $modelNewPage->scenario = 'add';
        if($_POST){
            if($modelNewPage->load(Yii::$app->request->post())){
                if($modelNewPage->save()){
                    Yii::$app->session->setFlash('added_page');
                    return $this->redirect('/administration/default/pages');
                }else{
                    Yii::$app->session->setFlash('not_added_page');
                    return $this->redirect('/administration/default/pages');
                }
            }
        }
        
        $queryPages = Pages::find();
        $modelPages = new ActiveDataProvider(['query' => $queryPages, 'pagination' => ['pageSize' => 10]]);
        return $this->render('pages', [
            'modelPages' => $modelPages,
            'modelNewPage' => $modelNewPage,
        ]);
    }
    
    public function actionPageupdate($id=null){
        
        $modelPage = Pages::find()->where(['id' => $id])->one();
        $modelPage->scenario = 'update';
        if($_POST){
            if($modelPage->load(Yii::$app->request->post())){
                if($modelPage->save()){
                    Yii::$app->session->setFlash('update_page');
                    return $this->redirect('/administration/default/pages');
                }else{
                    Yii::$app->session->setFlash('not_update_page');
                    return $this->redirect('/administration/default/pages');
                }
            }
        }
        
        return $this->render('pageupdate', [
            'modelPage' => $modelPage,
        ]);
    }
        
    public function actionPagedelete($id=null)
    {
        
        $modelPages = Pages::find()->where(['id' => $id])->one();
        if($modelPages->delete()){
            Yii::$app->session->setFlash('delete_pages');
            return $this->redirect(Url::home().'administration/default/pages');
        }else{
            Yii::$app->session->setFlash('not_delete_pages');
            return $this->redirect(Url::home().'administration/default/pages');
        }
    }
    //pages end
    
    //slider begin
    public function actionSlider(){
        $modelNewSlider = new Slider();
        $modelNewSlider->scenario = 'add';
        if($_POST){
            if($modelNewSlider->load(Yii::$app->request->post())){
                if($modelNewSlider->save()){
                    Yii::$app->session->setFlash('slider_changed');
                }else{
                    Yii::$app->session->setFlash('slider_not_changed');
                }
            }
        }
        $querylider = Slider::find();
        $modelSlider = new ActiveDataProvider(['query' => $querylider, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('slider', [
            'modelNewSlider' => $modelNewSlider,
            'modelSlider' => $modelSlider,
        ]);
    }
    
    public function actionSliderupdate($id=null){
        
        $modelSlider = Slider::find()->where(['id' => $id])->one();
        $modelSlider->scenario = 'update';
        if($_POST){
            if($modelSlider->load(Yii::$app->request->post())){
                if($modelSlider->save()){
                    Yii::$app->session->setFlash('update_slider');
                    return $this->redirect('/administration/default/slider');
                }else{
                    Yii::$app->session->setFlash('not_update_slider');
                    return $this->redirect('/administration/default/slider');
                }
            }
        }
        
        return $this->render('sliderupdate', [
            'modelSlider' => $modelSlider,
        ]);
    }
        
    public function actionSliderdelete($id=null)
    {        
        $modelSlider = Slider::find()->where(['id' => $id])->one();
        if($modelSlider->delete()){
            Yii::$app->session->setFlash('delete_slider');
            return $this->redirect(Url::home().'administration/default/slider');
        }else{
            Yii::$app->session->setFlash('not_delete_slider');
            return $this->redirect(Url::home().'administration/default/slider');
        }
    }
    //slider end
    
    
    public function actionSavepageheaderphoto(){
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/pages';   //2
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
              $tempFile = $_FILES['file']['tmp_name'];          //3

              $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

              $for_name = time();

              $targetFile =  $targetPath.$for_name.'.jpg';  //5

              move_uploaded_file($tempFile,$targetFile); //6
              return \Yii::$app->user->id.'/'.$for_name.'.jpg';
        }
    }
    
    public function actionSavesliderphoto(){
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/slider';   //2
        if (!empty($_FILES)) {
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
              $tempFile = $_FILES['file']['tmp_name'];          //3

              $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

              $for_name = time();

              $targetFile =  $targetPath.$for_name.'.jpg';  //5

              move_uploaded_file($tempFile,$targetFile); //6
              return \Yii::$app->user->id.'/'.$for_name.'.jpg';
        }
    }
    
    
}
