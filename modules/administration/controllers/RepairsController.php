<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\models\Pages;
use app\models\Setting;
use app\models\Repairs;
use app\models\Brand;
use app\models\Brandrepairs;
use app\models\Service;
use app\models\Models;
use app\models\Modelsservice;
use app\models\Servicerepairs;
use app\models\Slider;
use app\modules\administration\models\LoginForm;
use yii\data\ActiveDataProvider;
/**
 * Default controller for the `administration` module
 */
class RepairsController extends Controller
{
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        $this->layout = 'adminLayout';
       // var_dump(Yii::$app->user->identity->type);exit;
        if((!\Yii::$app->user->isGuest) && ((Yii::$app->user->identity->type == 'admin') || (Yii::$app->user->identity->type == 'manager'))){
            return parent::beforeAction($action);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    //repeirs begin
    public function actionType()
    {
        $modelNewRepairs = new Repairs();
        $modelNewRepairs->scenario = 'add';
        if($modelNewRepairs->load(Yii::$app->request->post())){
            if($modelNewRepairs->save()){
                Yii::$app->session->setFlash('add_repairs');
                $modelNewRepairs = new Repairs();
                $modelNewRepairs->scenario = 'add';
            }else{
                Yii::$app->session->setFlash('not_add_repairs');
            }
        }
        $queryRepairs = Repairs::find();
        $modelRepairs = new ActiveDataProvider(['query' => $queryRepairs, 'pagination' => ['pageSize' => 10]]);

        return $this->render('type',[
            'modelRepairs' => $modelRepairs,
            'modelNewRepairs' => $modelNewRepairs
        ]);
    }    
    
    
    public function actionRepairdelete($id=null)
    {
        
        $modelRepairs = Repairs::find()->where(['id' => $id])->one();
        if($modelRepairs->delete()){
            Brandrepairs::deleteAll(['repairs_id' => $id]);
            Servicerepairs::deleteAll(['repairs_id' => $id]);
            Yii::$app->session->setFlash('delete_repairs');
            return $this->redirect(Url::home().'administration/repairs/type');
        }else{
            Yii::$app->session->setFlash('not_delete_repairs');
            return $this->redirect(Url::home().'administration/repairs/type');
        }
    }
    
    public function actionRepairupdate($id=null)
    {
        $modelRepairs = Repairs::find()->where(['id' => $id])->one();
        $modelRepairs->scenario = 'update';
        if($modelRepairs->load(Yii::$app->request->post())){
            if($modelRepairs->save()){
                Yii::$app->session->setFlash('update_repairs');
                return $this->redirect(Url::home().'administration/repairs/type');
            }else{
                Yii::$app->session->setFlash('not_update_repairs');
            }
        }
        return $this->render('typeupdate', [
            'modelRepairs' => $modelRepairs,
        ]);
    }
    
    public function actionRepairshow($id=null)
    {
        $modelRepairs = Repairs::find()->where(['id' => $id])->one();
        $modelRepairs->scenario = 'update';
        
        $queryBrands = new Query();
        $queryBrands->select(['*', 'id' => '{{brand}}.id'])
                        ->from('{{brand_repairs}}')
                        ->join('LEFT JOIN',
                                    '{{brand}}',
                                    '{{brand}}.id = {{brand_repairs}}.brand_id')
                ->where(['{{brand_repairs}}.repairs_id' => $id]);
        
        $modelBrands = new ActiveDataProvider(['query' => $queryBrands, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('typeshow', [
            'modelRepairs' => $modelRepairs,
            'modelBrands' => $modelBrands,
        ]);
    }
    //repeirs end
    
    
    
    //brand begin
    public function actionBrand()
    {
        $modelNewBrand = new Brand();
        $modelNewBrand->scenario = 'add';
        if($modelNewBrand->load(Yii::$app->request->post())){
            if($modelNewBrand->save()){
                Yii::$app->session->setFlash('add_brand');
                foreach($modelNewBrand->repairs as $repairs_c){
                    $modelNewBrandRepairs = new Brandrepairs();
                    $modelNewBrandRepairs->scenario = 'add';
                    $modelNewBrandRepairs->brand_id = $modelNewBrand->id;
                    $modelNewBrandRepairs->repairs_id = $repairs_c;
                    $modelNewBrandRepairs->save();
                }
                $modelNewBrand = new Brand();
                $modelNewBrand->scenario = 'add';
            }else{
                Yii::$app->session->setFlash('not_add_brand');
            }
        }
        $queryBrand = Brand::find();
        $modelBrands = new ActiveDataProvider(['query' => $queryBrand, 'pagination' => ['pageSize' => 10]]);
        
        $repairsArray = ArrayHelper::map(Repairs::find()->all(), 'id', 'name');
        return $this->render('brand',[
            'modelBrands' => $modelBrands,
            'modelNewBrand' => $modelNewBrand,
            'repairsArray' => $repairsArray,
        ]);
    }    
    
    
    public function actionBranddelete($id=null)
    {        
        $modelBrand = Brand::find()->where(['id' => $id])->one();
        if($modelBrand->delete()){
            Brandrepairs::deleteAll(['brand_id' => $id]);
            Yii::$app->session->setFlash('brand_delete');
            return $this->redirect(Url::home().'administration/repairs/brand');
        }else{
            Yii::$app->session->setFlash('brand_not_delete');
            return $this->redirect(Url::home().'administration/repairs/brand');
        }
    }
    
    public function actionBrandshow($id=null)
    {
        $modelBrand = Brand::find()->where(['id' => $id])->one();
        $brandRepairs = Brandrepairs::find()->where(['brand_id' => $id])->asArray()->all();
        $brand_repairs_id = [];
        foreach($brandRepairs as $brand_repairs){
            $brand_repairs_id[] = $brand_repairs['repairs_id'];            
        }
        
        $queryModels = Models::find()->where(['brand_id' => $id]);
        $modelModels = new ActiveDataProvider(['query' => $queryModels, 'pagination' => ['pageSize' => 10]]);
        
        $repairsArray = ArrayHelper::map(Repairs::find()->all(), 'id', 'name');
        return $this->render('brandshow', [
            'modelBrand' => $modelBrand,
            'modelModels' => $modelModels,
            'repairsArray' => $repairsArray,
            'brand_repairs_id' => $brand_repairs_id,
        ]);
    }
    
    public function actionBrandupdate($id=null)
    {
        $modelBrand = Brand::find()->where(['id' => $id])->one();
        $modelBrand->scenario = 'update';
        if($modelBrand->load(Yii::$app->request->post())){
            if($modelBrand->save()){
                Yii::$app->session->setFlash('brand_update');
                Brandrepairs::deleteAll(['brand_id' => $id]);
                foreach($modelBrand->repairs as $repairs_c){
                    $modelNewBrandRepairs = new Brandrepairs();
                    $modelNewBrandRepairs->scenario = 'add';
                    $modelNewBrandRepairs->brand_id = $modelBrand->id;
                    $modelNewBrandRepairs->repairs_id = $repairs_c;
                    $modelNewBrandRepairs->save();
                }
                return $this->redirect(Url::home().'administration/repairs/brand');
            }else{
                Yii::$app->session->setFlash('brand_not_update');
                return $this->redirect(Url::home().'administration/repairs/brand');
            }
        }
        
        $brandRepairs = Brandrepairs::find()->where(['brand_id' => $id])->asArray()->all();
        $brand_repairs_id = [];
        foreach($brandRepairs as $brand_repairs){
            $brand_repairs_id[] = $brand_repairs['repairs_id'];            
        }
        $modelBrand->repairs = $brand_repairs_id;
        
        $repairsArray = ArrayHelper::map(Repairs::find()->all(), 'id', 'name');
        return $this->render('brandupdate', [
            'modelBrand' => $modelBrand,
            'repairsArray' => $repairsArray,
        ]);
    }
    //brand end
    
    
    //service begin
    public function actionServices()
    {
        $modelNewService = new Service();
        $modelNewService->scenario = 'add';

        if($modelNewService->load(Yii::$app->request->post())){
            
            if($modelNewService->save()){
                Yii::$app->session->setFlash('add_service');
                if($modelNewService->repairs){
                    foreach($modelNewService->repairs as $repairs_c){
                        $modelNewServiceRepairs = new Servicerepairs();
                        $modelNewServiceRepairs->scenario = 'add';
                        $modelNewServiceRepairs->service_id = $modelNewService->id;
                        $modelNewServiceRepairs->repairs_id = $repairs_c;
                        $modelNewServiceRepairs->save();
                    }                    
                }
                $modelNewService = new Service();
                $modelNewService->scenario = 'add';
            }else{
                Yii::$app->session->setFlash('not_add_service');
            }
        }
        $queryService = Service::find();
        $modelService = new ActiveDataProvider(['query' => $queryService, 'pagination' => ['pageSize' => 10]]);
        
        $repairsArray = ArrayHelper::map(Repairs::find()->all(), 'id', 'name');
        return $this->render('service',[
            'modelService' => $modelService,
            'modelNewService' => $modelNewService,
            'repairsArray' => $repairsArray,
        ]);
    }    
    
    
    public function actionServicedelete($id=null)
    {        
        $modelService = Service::find()->where(['id' => $id])->one();
        if($modelService->delete()){
            Servicerepairs::deleteAll(['service_id' => $id]);
            Yii::$app->session->setFlash('service_delete');
            return $this->redirect(Url::home().'administration/repairs/services');
        }else{
            Yii::$app->session->setFlash('service_not_delete');
            return $this->redirect(Url::home().'administration/repairs/services');
        }
    }
    
    public function actionServiceupdate($id=null)
    {
        $modelService = Service::find()->where(['id' => $id])->one();
        $modelService->scenario = 'update';
        if($modelService->load(Yii::$app->request->post())){
            if($modelService->save()){
                Yii::$app->session->setFlash('service_update');
                Servicerepairs::deleteAll(['service_id' => $id]);
                if($modelService->repairs){
                    foreach($modelService->repairs as $repairs_c){
                        $modelNewServiceRepairs = new Servicerepairs();
                        $modelNewServiceRepairs->scenario = 'add';
                        $modelNewServiceRepairs->service_id = $modelService->id;
                        $modelNewServiceRepairs->repairs_id = $repairs_c;
                        $modelNewServiceRepairs->save();
                    }                    
                }
                return $this->redirect(Url::home().'administration/repairs/services');
            }else{
                Yii::$app->session->setFlash('service_not_update');
                return $this->redirect(Url::home().'administration/repairs/services');
            }
        }
        
        $serviceRepairs = Servicerepairs::find()->where(['service_id' => $id])->asArray()->all();
        $service_repairs_id = [];
        foreach($serviceRepairs as $service_repairs){
            $service_repairs_id[] = $service_repairs['repairs_id'];            
        }
        $modelService->repairs = $service_repairs_id;
        $repairsArray = ArrayHelper::map(Repairs::find()->all(), 'id', 'name');
        return $this->render('serviceupdate', [
            'modelService' => $modelService,
            'repairsArray' => $repairsArray,
        ]);
    }
    
    public function actionServiceshow($id=null)
    {
        $modelService = Service::find()->where(['id' => $id])->one();
        
        $serviceRepairs = Servicerepairs::find()->where(['service_id' => $id])->asArray()->all();
        $service_repairs_id = [];
        foreach($serviceRepairs as $service_repairs){
            $service_repairs_id[] = $service_repairs['repairs_id'];            
        }
        $modelService->repairs = $service_repairs_id;
        
        
        $serviceChoiseArray = ArrayHelper::map(Modelsservice::find()->where(['service_id' => $id])->all(), 'models_id', 'price');
        
        return $this->render('serviceshow', [
            'modelService' => $modelService,
            'serviceChoiseArray' => $serviceChoiseArray,
        ]);
    }
    
    public function actionChangeserviceprice($id=null)
    {
        $models_id = $_POST['models_id'];
        $new_service_price = $_POST['new_service_price'];
        $service_id = $_POST['service_id'];
        $result = [];
        $modeModelsService = Modelsservice::find()->where(['models_id' => $models_id, 'service_id' => $service_id])->one();
        if($modeModelsService){
            $modeModelsService->scenario = 'update_price';
            $modeModelsService->price = $new_service_price;
            if($modeModelsService->save()){
                $result['status'] = 'success';
            }else{
                $result['status'] = 'error';            
            }
        }else{
            $result['status'] = 'error';            
        }
        echo json_encode($result);
    }
    
    //service end
    
    
    //models begin
    public function actionModels()
    {
        $modelNewModels = new Models();
        $modelNewModels->scenario = 'add';
        if($_POST){
            if($modelNewModels->load(Yii::$app->request->post())){
//                    var_dump($modelNewModels);exit;
                if($modelNewModels->save()){
                    Yii::$app->session->setFlash('models_add');
                    if($modelNewModels->services != null){
                        foreach($modelNewModels->services as $key => $service_id){
                            $modeNewModelsService = new Modelsservice();
                            $modeNewModelsService->scenario = 'add';
                            $modeNewModelsService->service_id = $service_id;
                            $modeNewModelsService->models_id = $modelNewModels->id;
                            $modeNewModelsService->price = $modelNewModels->servicesprice[$service_id];
                            $modeNewModelsService->save();
                        }
                    }                
                    $modelNewModels = new Models();
                    $modelNewModels->scenario = 'add';
                }else{
                    Yii::$app->session->setFlash('models_not_add');                
                }
            }            
        }
        
        $queryModels = Models::find()->orderBy('date_create DESC');
        $modelModels = new ActiveDataProvider(['query' => $queryModels, 'pagination' => ['pageSize' => 20]]);
        
        $repairsArray = ArrayHelper::map(Repairs::find()->all(), 'id', 'name');
        
        return $this->render('models',[
            'modelNewModels' => $modelNewModels,
            'modelModels' => $modelModels,
            'repairsArray' => $repairsArray,
        ]);
    }    
          
    public function actionModelsupdate($id=null)
    {
        $modelModels = Models::find()->where(['id' => $id])->one();
        $modelModels->scenario = 'update';
        if($_POST){
            if($modelModels->load(Yii::$app->request->post())){
                if($modelModels->save()){
                    Yii::$app->session->setFlash('models_update');
                    Modelsservice::deleteAll(['models_id' => $modelModels->id]);
                    if($modelModels->services != null){
                        foreach($modelModels->services as $key => $service_id){
                            $modeNewModelsService = new Modelsservice();
                            $modeNewModelsService->scenario = 'add';
                            $modeNewModelsService->service_id = $service_id;
                            $modeNewModelsService->models_id = $modelModels->id;
                            $modeNewModelsService->price = $modelModels->servicesprice[$service_id];
                            $modeNewModelsService->save();
                        }
                    }                
                    return $this->redirect(Url::home().'administration/repairs/models');
                }else{
                    Yii::$app->session->setFlash('models_not_update');                
                    return $this->redirect(Url::home().'administration/repairs/models');
                }
            }            
        }
        $repairsArray = ArrayHelper::map(Repairs::find()->all(), 'id', 'name');
        
        $queryBrand = new Query();
        $queryBrand->select(['*', 'id' => '{{brand_repairs}}.id'])
                        ->from('{{brand_repairs}}')
                        ->join('LEFT JOIN',
                                    '{{brand}}',
                                    '{{brand}}.id = {{brand_repairs}}.brand_id')
                ->where(['{{brand_repairs}}.repairs_id' => $modelModels->repairs_id]);
        $command = $queryBrand->createCommand();
        $modelBrand = $command->queryAll();
        $brandArray = ArrayHelper::map($modelBrand, 'brand_id', 'name');
        
        $queryService = new Query();
        $queryService->select(['*', 'id' => '{{service_repairs}}.id'])
                        ->from('{{service_repairs}}')
                        ->join('LEFT JOIN',
                                    '{{service}}',
                                    '{{service}}.id = {{service_repairs}}.service_id')
                ->where(['{{service_repairs}}.repairs_id' => $modelModels->repairs_id]);
        
        $command = $queryService->createCommand();
        $queryService = $command->queryAll();
        $serviceArray = ArrayHelper::map($queryService, 'service_id', 'name');
        
        $serviceChoiseArray = ArrayHelper::map(Modelsservice::find()->where(['models_id' => $id])->all(), 'service_id', 'price');
        
        return $this->render('modelsupdate',[
            'modelModels' => $modelModels,
            'repairsArray' => $repairsArray,
            'brandArray' => $brandArray,
            'serviceArray' => $serviceArray,
            'serviceChoiseArray' => $serviceChoiseArray,
        ]);
        
    }
    
    
    public function actionModelsshow($id=null)
    {
        $modelModels = Models::find()->where(['id' => $id])->one();
        $queryService = new Query();
        $queryService->select(['*', 'id' => '{{service_repairs}}.id'])
                        ->from('{{service_repairs}}')
                        ->join('LEFT JOIN',
                                    '{{service}}',
                                    '{{service}}.id = {{service_repairs}}.service_id')
                ->where(['{{service_repairs}}.repairs_id' => $modelModels->repairs_id]);
        
        $command = $queryService->createCommand();
        $queryService = $command->queryAll();
        $serviceArray = ArrayHelper::map($queryService, 'service_id', 'name');
        
        $serviceChoiseArray = ArrayHelper::map(Modelsservice::find()->where(['models_id' => $id])->all(), 'service_id', 'price');
        
        return $this->render('modelsshow',[
            'modelModels' => $modelModels,
            'serviceArray' => $serviceArray,
            'serviceChoiseArray' => $serviceChoiseArray,
        ]);
        
    }
    
    
    public function actionModelsdelete($id=null)
    {        
        $modelModels = Models::find()->where(['id' => $id])->one();
        if($modelModels->delete()){
            Modelsservice::deleteAll(['models_id' => $id]);
            Yii::$app->session->setFlash('models_delete');
            return $this->redirect(Url::home().'administration/repairs/models');
        }else{
            Yii::$app->session->setFlash('models_not_delete');
            return $this->redirect(Url::home().'administration/repairs/models');
        }
    }    
    
    public function actionDataformodelcreate(){
        $result = [];
        $repairs_id = $_POST['repairs_id'];
                
        $queryBrand = new Query();
        $queryBrand->select(['*', 'id' => '{{brand_repairs}}.id'])
                        ->from('{{brand_repairs}}')
                        ->join('LEFT JOIN',
                                    '{{brand}}',
                                    '{{brand}}.id = {{brand_repairs}}.brand_id')
                ->where(['{{brand_repairs}}.repairs_id' => $repairs_id]);        
        $command = $queryBrand->createCommand();
        $modelBrand = $command->queryAll();
        $brandArray = ArrayHelper::map($modelBrand, 'brand_id', 'name');
        $result['brands'] = $brandArray;
                
        $queryService = new Query();
        $queryService->select(['*', 'id' => '{{service_repairs}}.id'])
                        ->from('{{service_repairs}}')
                        ->join('LEFT JOIN',
                                    '{{service}}',
                                    '{{service}}.id = {{service_repairs}}.service_id')
                ->where(['{{service_repairs}}.repairs_id' => $repairs_id]);
        
        $command = $queryService->createCommand();
        $modelBrand = $command->queryAll();
        $serviceArray = ArrayHelper::map($modelBrand, 'service_id', 'name');
        $result['services'] = $serviceArray;
        
        echo json_encode($result); 
    }
    //models end
    
    
    public function actionSavemodelsphoto()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/models';   //2
        if (!empty($_FILES)){
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];          //3

            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

            $for_name = time();

            $targetFile =  $targetPath.$for_name.'.jpg';  //5

            move_uploaded_file($tempFile,$targetFile); //6
            return \Yii::$app->user->id.'/'.$for_name.'.jpg';
        }
    } 
    
    public function actionSavebrandphoto()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/brand';   //2
        if (!empty($_FILES)){
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];          //3

            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

            $for_name = time();

            $targetFile =  $targetPath.$for_name.'.jpg';  //5

            move_uploaded_file($tempFile,$targetFile); //6
            return \Yii::$app->user->id.'/'.$for_name.'.jpg';
        }
    } 
    
    public function actionSaverepairphoto()
    {
        $ds          = DIRECTORY_SEPARATOR;  //1
        $storeFolder = \Yii::getAlias('@webroot').'/images/repair';   //2
        if (!empty($_FILES)){
            if(!is_dir($storeFolder.$ds.Yii::$app->user->id)){
              mkdir($storeFolder.$ds.Yii::$app->user->id, 0777, true);
            }
            $tempFile = $_FILES['file']['tmp_name'];          //3

            $targetPath =  $storeFolder . $ds.Yii::$app->user->id.$ds;  //4

            $for_name = time();

            $targetFile =  $targetPath.$for_name.'.jpg';  //5

            move_uploaded_file($tempFile,$targetFile); //6
            return \Yii::$app->user->id.'/'.$for_name.'.jpg';
        }
    } 
    
}
