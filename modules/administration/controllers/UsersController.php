<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\User;
use app\models\Pages;
use app\models\Leads;
use app\models\Newsletter;
use app\models\Setting;
use app\modules\administration\models\LoginForm;
use yii\data\ActiveDataProvider;
set_time_limit(5000);

class UsersController extends Controller
{
     public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        $this->layout = 'adminLayout';
        if((!\Yii::$app->user->isGuest) && ((Yii::$app->user->identity->type == 'admin') || (Yii::$app->user->identity->type == 'manager'))){
            return parent::beforeAction($action);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionIndex()
    {
        $modelNewUser = new User;
        $modelNewUser->scenario = 'add';
        //'signup' => [ 'password', 'password_hash', 'email', 'auth_key'],
        
        if($modelNewUser->load(Yii::$app->request->post())){
            $modelNewUser->password_hash = \Yii::$app->security->generatePasswordHash($modelNewUser->password);
            $modelNewUser->auth_key = 'key';
            if ($modelNewUser->save()) {
                Yii::$app->session->setFlash('add_user');                        
            }else{
                Yii::$app->session->setFlash('not_add_user');                    
            }
        }
        $queryUser = User::find()->where(['type' => 'admin'])->orWhere(['type' => 'manager']);
        $modelUser = new ActiveDataProvider(['query' => $queryUser, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('users',[
            'modelUser' => $modelUser,
            'modelNewUser' => $modelNewUser
        ]);
    }
    
    public function actionUserupdate($id=null)
    {
        $modelUser = User::find()->where(['id' => $id])->one();
        $modelUser->scenario = 'add';
        if($modelUser->load(Yii::$app->request->post())){
            $modelUser->password_hash = \Yii::$app->security->generatePasswordHash($modelUser->password);
            $modelUser->auth_key = 'key';
            if ($modelUser->save()) {
                Yii::$app->session->setFlash('update_user');                        
                return $this->redirect(Url::home().'administration/users/index');
            }else{
                Yii::$app->session->setFlash('not_update_user');                    
                return $this->redirect(Url::home().'administration/users/index');
            }
        }
        
        return $this->render('userupdate', [
            'modelUser' => $modelUser,
        ]);
    }
    
    public function actionUserdelete($id=null)
    {
        $modelUser = User::find()->where(['id' => $id])->one();
        if($modelUser->delete()){
            Yii::$app->session->setFlash('delete_user');
            return $this->redirect(Url::home().'administration/users/index');
        }else{
            Yii::$app->session->setFlash('not_delete_user');
            return $this->redirect(Url::home().'administration/users/index');
        }
    }
    
    public function actionContact()
    {
        $queryUser = User::find()->where(['type' => ''])->orWhere(['type' => 'users'])->orWhere(['type' => null]);
        $modelUser = new ActiveDataProvider(['query' => $queryUser, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('contact',[
            'modelUser' => $modelUser,
        ]);
    }
    
    public function actionContactshow($id)
    {        
        $modelUser = User::find()->where(['id' => $id])->one();
        $queryLeads = Leads::find()->where(['user_id' => $id]);
        $modelLeads = new ActiveDataProvider(['query' => $queryLeads, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('contactshow',[
            'modelUser' => $modelUser,
            'modelLeads' => $modelLeads,
        ]);
    }
    
    public function actionContactdelete($id=null)
    {
        $modelUser = User::find()->where(['id' => $id])->one();
        if($modelUser->delete()){
            Yii::$app->session->setFlash('delete_user');
            return $this->redirect(Url::home().'administration/users/contact');
        }else{
            Yii::$app->session->setFlash('not_delete_user');
            return $this->redirect(Url::home().'administration/users/contact');
        }
    }
    
    public function actionNewsletter()
    {
        $modelNewNewsletter = new Newsletter();
        $modelNewNewsletter->scenario = 'add';
        
        if($_POST){
            if($modelNewNewsletter->load(Yii::$app->request->post())){
                if($modelNewNewsletter->save()){
                    $modelUser = User::find()->where(['type' => ''])->orWhere(['type' => 'users'])->orWhere(['type' => null])->asArray()->all();
                    $arrayEmail = [];
                    foreach($modelUser as $user){
                        if(($user['email'] != '') && ($user['email'] != null)){
                            //$arrayEmail[] = $user['email'];                            
                            if (\Yii::$app->mailer->compose()
                                ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                                ->setTo($user['email'])
                                ->setSubject('Phonerepairs news')
                                ->setHtmlBody($modelNewNewsletter->content)->send()){

                                }
                        }                        
                    }
                    
                    Yii::$app->session->setFlash('news_sended');
                }else{
                    Yii::$app->session->setFlash('news_not_sended');
                }
            }
        }
        
        $queryNewsletter = Newsletter::find()->orderBy('id DESC');
        $modelNewsletter = new ActiveDataProvider(['query' => $queryNewsletter, 'pagination' => ['pageSize' => 10]]);
        return $this->render('newsletter',[
            'modelNewNewsletter' => $modelNewNewsletter,
            'modelNewsletter' => $modelNewsletter,
        ]);
    }
    
    public function actionNewsshow($id)
    {
        $modelNewsletter = Newsletter::find()->where(['id' => $id])->one();
        return $this->render('newsshow',[
            'modelNewsletter' => $modelNewsletter
        ]);        
    }
    
}
