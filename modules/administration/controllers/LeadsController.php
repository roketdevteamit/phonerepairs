<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\models\Pages;
use app\models\Setting;
use app\models\Repairs;
use app\models\Brand;
use app\models\Brandrepairs;
use app\models\Service;
use app\models\User;
use app\models\Models;
use app\models\Modelsservice;
use app\models\Servicerepairs;
use app\models\Slider;
use app\models\Leadsmessage;
use app\models\Leads;
use app\models\Branches;
use app\modules\administration\models\LoginForm;
use yii\data\ActiveDataProvider;
/**
 * Default controller for the `administration` module
 */
class LeadsController extends Controller
{
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        $this->layout = 'adminLayout';
       // var_dump(Yii::$app->user->identity->type);exit;
        if((!\Yii::$app->user->isGuest) && ((Yii::$app->user->identity->type == 'admin') || (Yii::$app->user->identity->type == 'manager'))){
            return parent::beforeAction($action);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    //Leads tupe Enquiry begin
    public function actionEnquiries()
    {
        $queryEnquiry = Leads::find()->where(['leads_type' => 0])->orderBy('date_create DESC');
        $modelEnquiry = new ActiveDataProvider(['query' => $queryEnquiry, 'pagination' => ['pageSize' => 10]]);

        return $this->render('enquiry',[
            'modelEnquiry' => $modelEnquiry,
        ]);
    }
    
    public function actionEnquiriesshow($id)
    {
            
        $modelEnquiry = Leads::find()->where(['id' => $id])->one();
        $modelEnquiry->scenario = 'all';
        if($modelEnquiry){
            $modelNewLeadmessage = new Leadsmessage();
            $modelNewLeadmessage->scenario = 'add';
            
            
            if($_POST){
                if(isset($_POST['sent_commect'])){
                    if($modelNewLeadmessage->load(Yii::$app->request->post())){
                        if($modelNewLeadmessage->save()){
                            
                            \Yii::$app->mailer->compose()
                            ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                            ->setTo($modelEnquiry->email)
                            ->setSubject('New comment for Reference: ('.$modelEnquiry->key.$modelEnquiry->id.')')
                            ->setHtmlBody("You have new comment for Reference: (".$modelEnquiry->key.$modelEnquiry->id.") 
                                    <html>
                                                  <head>
                                                          <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                                                  </head>
                                                       <body>
                                                         <div style='text-align: center'>
                                                              <div style='display: inline-block; width: 50%; text-align: right'>
                                                                      <ul>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_facebook."'>
                                                                                  <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_twitter."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a  href='".$social_links_linkedin."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_telephoneinstagram."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                              </a>
                                                                      </ul>
                                                                  </div>
                                                              </div>
                                                            <div style='text-align: center'>
                                                                <img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                                <br>
                                                                <div style='text-align: center'>
                                                                    <div style='display: inline-block; text-align: left'>
                                                                            ''".$modelNewLeadmessage->content."'' 
                                                                        <br>
                                                                         <br>

                                                                        <div style=' text-align: left'>
                                                                            Thank you for contacting Phone Repairs 4U
                                                                        </div>
                                                                        <br>
                                                                        <br>
                                                                        <br>

                                                                        <div style=' text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                                        <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                                    </div>                                                              
                                                                </div>
                                                            </div>
                                                    </body>
                                        </html>")
                            ->send();
                            
                            Yii::$app->session->setFlash('message_send');
                        }else{
                            Yii::$app->session->setFlash('message_not_send');  
                        }
                    }
                }
                if(isset($_POST['update_enquiry'])){
                    if($modelEnquiry->load(Yii::$app->request->post())){
                        if($modelEnquiry->save()){
                            Yii::$app->session->setFlash('lead_updated');
                        }else{
                            Yii::$app->session->setFlash('lead_not_updated');  
                        }
                    }                    
                }  
                
            }
            $queryLeadmessage = new Query();
            $queryLeadmessage->select(['*', 'id' => '{{leads_message}}.id', 'date_create' => '{{leads_message}}.date_create'])
                            ->from('{{leads_message}}')
                            ->join('LEFT JOIN',
                                        '{{users}}',
                                        '{{users}}.id = {{leads_message}}.user_id')
                    ->where(['{{leads_message}}.leads_id' => $id]);
            $command = $queryLeadmessage->createCommand();
            $modelLeadmessage = $command->queryAll();

            $arrayBranch = [];
            foreach(\app\models\Branches::find()->all() as $branch){
                $arrayBranch[$branch['id']] = strip_tags($branch['address_street']);                                    
            }
            $arrayModels = ArrayHelper::map(Models::find()->asArray()->all(), 'id', 'name');
            $arrayService = ArrayHelper::map(Service::find()->asArray()->all(), 'id', 'name');
            $arrayAssigned = ArrayHelper::map(User::find()->where(['type' => 'admin'])->where(['type' => 'manager'])->asArray()->all(), 'id', 'name');
            
            return $this->render('enquiryshow',[
                'modelNewLeadmessage' => $modelNewLeadmessage,
                'modelEnquiry' => $modelEnquiry,
                'modelLeadmessage' => $modelLeadmessage,
                
                'arrayBranch' => $arrayBranch,
                'arrayModels' => $arrayModels,
                'arrayService' => $arrayService,
                'arrayAssigned' => $arrayAssigned,
            ]);            
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
        
    public function actionEnquiriesdelete($id)
    {
        $modelEnquiry = Leads::find()->where(['id' => $id])->one();
        if($modelEnquiry->delete()){
            Leadsmessage::deleteAll(['leads_id' => $id]);
            
            Yii::$app->session->setFlash('delete_enquiries');
            return $this->redirect(Url::home().'administration/leads/enquiries');
        }else{
            Yii::$app->session->setFlash('not_delete_enquiries');
            return $this->redirect(Url::home().'administration/leads/enquiries');
        }
    }
    
    public function actionBookingsdelete($id)
    {
        $modelEnquiry = Leads::find()->where(['id' => $id])->one();
        if($modelEnquiry->delete()){
            Leadsmessage::deleteAll(['leads_id' => $id]);
            
            Yii::$app->session->setFlash('delete_booking');
            return $this->redirect(Url::home().'administration/leads/bookings');
        }else{
            Yii::$app->session->setFlash('not_delete_booking');
            return $this->redirect(Url::home().'administration/leads/bookings');
        }
    }
    
    public function actionCorporatedelete($id)
    {
        $modelCorporate = Leads::find()->where(['id' => $id])->one();
        if($modelCorporate->delete()){
            Leadsmessage::deleteAll(['leads_id' => $id]);
            
            Yii::$app->session->setFlash('delete_corporate');
            return $this->redirect(Url::home().'administration/leads/corporate');
        }else{
            Yii::$app->session->setFlash('not_delete_corporate');
            return $this->redirect(Url::home().'administration/leads/corporate');
        }
    }
    
    public function actionMaildelete($id)
    {
        $modelMain = Leads::find()->where(['id' => $id])->one();
        if($modelMain->delete()){
            Leadsmessage::deleteAll(['leads_id' => $id]);
            
            Yii::$app->session->setFlash('delete_mail');
            return $this->redirect(Url::home().'administration/leads/mail_in');
        }else{
            Yii::$app->session->setFlash('not_delete_corporate');
            return $this->redirect(Url::home().'administration/leads/mail_in');
        }
    }
    
    public function actionCreatebooking($id)
    {
        $modelEnquiry = Leads::find()->where(['id' => $id])->one();
        if($modelEnquiry){
            $modelEnquiry->scenario = 'change_type';
            $modelEnquiry->status = 1;
            $modelEnquiry->leads_type = 2;
            $modelEnquiry->booking_date = date("Y-m-d H:i:s");
            if($modelEnquiry->save()){
                Yii::$app->session->setFlash('booking_created');
                return $this->redirect(Url::home().'administration/leads/bookingsshow?id='.$modelEnquiry->id);
            }else{
                Yii::$app->session->setFlash('booking_not_created');
            }
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
        
    }
    
    public function actionBookings($branch = null)
    {
        $queryBookings = Leads::find()->where(['leads_type' => 2])->orderBy('booking_date DESC');
        if($branch != null){
            $queryBookings->andWhere(['branch_id' => $branch]);
        }
        if($_GET){
            if(isset($_GET['reference_no'])){
                if($_GET['reference_no'] != ''){
                    $queryBookings->andWhere(['like' , 'identification_id', $_GET['reference_no']]);                
                }
            }
        }
        $modelBookings = new ActiveDataProvider(['query' => $queryBookings, 'pagination' => ['pageSize' => 50]]);
        $arrayBookings = [];
        foreach($modelBookings->getModels() as $bookings){
            $booking_date = date("d-m-Y",strtotime($bookings->booking_date));
            $arrayBookings[$booking_date][] = $bookings;
        }
        
        $modelBranch = Branches::find()->asArray()->all();
        
        return $this->render('bookings',[
            'modelBookings' => $modelBookings->getModels(),
            'pagination' => $modelBookings->pagination,
            'count' => $modelBookings->pagination->totalCount,
            'arrayBookings' => $arrayBookings,
            'modelBranch' => $modelBranch,
        ]);
    }
    
    public function actionCreate_booking()
    {
        $modelNewBooking = new Leads();
        $modelNewBooking->scenario = 'all';
        if($_POST){
            if(isset($_POST['update_booking'])){
                if($modelNewBooking->load(Yii::$app->request->post())){
                    $modelNewBooking->booking_date = date("Y-m-d H:i:s");
                    if($modelNewBooking->save()){
                        
                        $modelNewBooking->scenario = 'add_identification_id';
                        $modelNewBooking->identification_id = $modelNewBooking->key.$modelNewBooking->id;
                        $modelNewBooking->save();
                        
                        
                            $modelUser = User::find()->where(['email' => $modelNewBooking->email])->one();
                            if (!$modelUser) {
                                $userpassword = substr(str_shuffle(str_repeat("0123456789abcdefghij!_klmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
                                $modelNewUser = new User();
                                $modelNewUser->scenario = 'signup_t';
                                $modelNewUser->email = $modelNewBooking->email;
                                $modelNewUser->password_hash = \Yii::$app->security->generatePasswordHash($userpassword);
                                $modelNewUser->auth_key = 'key';
                                if ($modelNewUser->save()) {
                                    $modelNewBooking->scenario = 'add_user';
                                    $modelNewBooking->user_id = $modelNewUser->id;
                                    $modelNewBooking->save();
                                }
                            } else {
                                $modelNewBooking->scenario = 'add_user';
                                $modelNewBooking->user_id = $modelUser->id;
                                $modelNewBooking->save();
                            }
                            Yii::$app->session->setFlash('lead_added');
                            return $this->redirect('/administration/leads/bookingsshow?id='.$modelNewBooking->id);
                    }else{
                        Yii::$app->session->setFlash('lead_not_added');
                    }
                }
            }
        }
        
            $arrayBranch = [];
            foreach(\app\models\Branches::find()->all() as $branch){
                $arrayBranch[$branch['id']] = strip_tags($branch['address_street']);                                    
            }
            $arrayModels = ArrayHelper::map(Models::find()->asArray()->all(), 'id', 'name');
            $arrayService = ArrayHelper::map(Service::find()->asArray()->all(), 'id', 'name');
            $modelAssigned = User::find()->where(['type' => 'admin'])->orWhere(['type' => 'manager'])->asArray()->all();
            
            $arrayAssigned = [];
            foreach($modelAssigned as $assigned){
                if(($assigned['name'] != '') && ($assigned['name'] != null)){
                    $arrayAssigned[$assigned['id']] = $assigned['name'];
                }else{
                    $arrayAssigned[$assigned['id']] = $assigned['email'];                    
                }
            }

            return $this->render('createbooking',[
                'modelNewBooking' => $modelNewBooking,
                'arrayBranch' => $arrayBranch,
                'arrayModels' => $arrayModels,
                'arrayService' => $arrayService,
                'arrayAssigned' => $arrayAssigned,
            ]); 
            
    }   
    
    public function actionBookingsshow($id)
    {            
        $modelBooking = Leads::find()->where(['id' => $id])->one();
        $modelBooking->scenario = 'all';
        if($modelBooking){
            $modelNewLeadmessage = new Leadsmessage();
            $modelNewLeadmessage->scenario = 'add';
            
            
            if($_POST){
                if(isset($_POST['sent_commect'])){
                    if($modelNewLeadmessage->load(Yii::$app->request->post())){
                        if($modelNewLeadmessage->save()){
                            
                            \Yii::$app->mailer->compose()
                            ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                            ->setTo($modelBooking->email)
                            ->setSubject('New comment for Reference: ('.$modelBooking->key.$modelBooking->id.')')
                            ->setHtmlBody("You have new comment for Reference: (".$modelBooking->key.$modelBooking->id.") 
                                    <html>
                                                  <head>
                                                          <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                                                  </head>
                                                       <body>
                                                         <div style='text-align: center'>
                                                              <div style='display: inline-block; width: 50%; text-align: right'>
                                                                      <ul>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_facebook."'>
                                                                                  <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_twitter."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a  href='".$social_links_linkedin."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_telephoneinstagram."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                              </a>
                                                                      </ul>
                                                                  </div>
                                                              </div>
                                                            <div style='text-align: center'>
                                                                <img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                                <br>
                                                                <div style='text-align: center'>
                                                                    <div style='display: inline-block; text-align: left'>
                                                                            ''".$modelNewLeadmessage->content."'' 
                                                                        <br>
                                                                         <br>

                                                                        <div style=' text-align: left'>
                                                                            Thank you for contacting Phone Repairs 4U
                                                                        </div>
                                                                        <br>
                                                                        <br>
                                                                        <br>

                                                                        <div style=' text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                                        <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                                    </div>                                                              
                                                                </div>
                                                            </div>
                                                    </body>
                                        </html>")
                            ->send();
                            
                            Yii::$app->session->setFlash('message_send');
                        }else{
                            Yii::$app->session->setFlash('message_not_send');  
                        }
                    }
                }
                
                if(isset($_POST['update_booking'])){
                    if($modelBooking->load(Yii::$app->request->post())){
                        if($modelBooking->save()){
                            Yii::$app->session->setFlash('lead_updated');
                        }else{
                            Yii::$app->session->setFlash('lead_not_updated');  
                        }
                    }                    
                }  
                
            }
            $queryLeadmessage = new Query();
            $queryLeadmessage->select(['*', 'id' => '{{leads_message}}.id', 'date_create' => '{{leads_message}}.date_create'])
                            ->from('{{leads_message}}')
                            ->join('LEFT JOIN',
                                        '{{users}}',
                                        '{{users}}.id = {{leads_message}}.user_id')
                    ->where(['{{leads_message}}.leads_id' => $id]);
            $command = $queryLeadmessage->createCommand();
            $modelLeadmessage = $command->queryAll();
            
            $arrayBranch = [];
            foreach(\app\models\Branches::find()->all() as $branch){
                $arrayBranch[$branch['id']] = strip_tags($branch['address_street']);                                    
            }
            $arrayModels = ArrayHelper::map(Models::find()->asArray()->all(), 'id', 'name');
            $arrayService = ArrayHelper::map(Service::find()->asArray()->all(), 'id', 'name');
            $modelAssigned = User::find()->where(['type' => 'admin'])->orWhere(['type' => 'manager'])->asArray()->all();
            
            $arrayAssigned = [];
            foreach($modelAssigned as $assigned){
                if(($assigned['name'] != '') && ($assigned['name'] != null)){
                    $arrayAssigned[$assigned['id']] = $assigned['name'];
                }else{
                    $arrayAssigned[$assigned['id']] = $assigned['email'];                    
                }
            }

            return $this->render('bookingsshow',[
                'modelNewLeadmessage' => $modelNewLeadmessage,
                'modelBooking' => $modelBooking,
                'modelLeadmessage' => $modelLeadmessage,
                'arrayBranch' => $arrayBranch,
                'arrayModels' => $arrayModels,
                'arrayService' => $arrayService,
                'arrayAssigned' => $arrayAssigned,
            ]);            
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    
     public function actionMail_inshow($id)
    {            
        $modelMailin = Leads::find()->where(['id' => $id])->one();
        $modelMailin->scenario = 'all';
        if($modelMailin){
            $modelNewLeadmessage = new Leadsmessage();
            $modelNewLeadmessage->scenario = 'add';
            
            
            if($_POST){
                if(isset($_POST['sent_commect'])){
                    if($modelNewLeadmessage->load(Yii::$app->request->post())){
                        if($modelNewLeadmessage->save()){
                            
                            \Yii::$app->mailer->compose()
                            ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                            ->setTo($modelMailin->email)
                            ->setSubject('New comment for Reference: ('.$modelMailin->key.$modelMailin->id.')')
                            ->setHtmlBody("You have new comment for Reference: (".$modelMailin->key.$modelMailin->id.") 
                                    <html>
                                                  <head>
                                                          <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                                                  </head>
                                                       <body>
                                                         <div style='text-align: center'>
                                                              <div style='display: inline-block; width: 50%; text-align: right'>
                                                                      <ul>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_facebook."'>
                                                                                  <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_twitter."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a  href='".$social_links_linkedin."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_telephoneinstagram."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                              </a>
                                                                      </ul>
                                                                  </div>
                                                              </div>
                                                            <div style='text-align: center'>
                                                                <img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                                <br>
                                                                <div style='text-align: center'>
                                                                    <div style='display: inline-block; text-align: left'>
                                                                            ''".$modelNewLeadmessage->content."'' 
                                                                        <br>
                                                                         <br>

                                                                        <div style=' text-align: left'>
                                                                            Thank you for contacting Phone Repairs 4U
                                                                        </div>
                                                                        <br>
                                                                        <br>
                                                                        <br>

                                                                        <div style=' text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                                        <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                                    </div>                                                              
                                                                </div>
                                                            </div>
                                                    </body>
                                        </html>")
                            ->send();
                            
                            Yii::$app->session->setFlash('message_send');
                        }else{
                            Yii::$app->session->setFlash('message_not_send');  
                        }
                    }
                }                
                if(isset($_POST['update_mailin'])){
                    if($modelMailin->load(Yii::$app->request->post())){
                        if($modelMailin->save()){
                            Yii::$app->session->setFlash('lead_updated');
                        }else{
                            Yii::$app->session->setFlash('lead_not_updated');  
                        }
                    }                    
                }                
                
            }
            $queryLeadmessage = new Query();
            $queryLeadmessage->select(['*', 'id' => '{{leads_message}}.id', 'date_create' => '{{leads_message}}.date_create'])
                            ->from('{{leads_message}}')
                            ->join('LEFT JOIN',
                                        '{{users}}',
                                        '{{users}}.id = {{leads_message}}.user_id')
                    ->where(['{{leads_message}}.leads_id' => $id]);
            $command = $queryLeadmessage->createCommand();
            $modelLeadmessage = $command->queryAll();
            
            $arrayBranch = [];
            foreach(\app\models\Branches::find()->all() as $branch){
                $arrayBranch[$branch['id']] = strip_tags($branch['address_street']);                                    
            }
            $arrayModels = ArrayHelper::map(Models::find()->asArray()->all(), 'id', 'name');
            $arrayService = ArrayHelper::map(Service::find()->asArray()->all(), 'id', 'name');
            $arrayAssigned = ArrayHelper::map(User::find()->where(['type' => 'admin'])->where(['type' => 'manager'])->asArray()->all(), 'id', 'name');
            
            return $this->render('mail_inshow',[
                'modelNewLeadmessage' => $modelNewLeadmessage,
                'modelLeadmessage' => $modelLeadmessage,
                'arrayAssigned' => $arrayAssigned,
                'arrayService' => $arrayService,
                'modelMailin' => $modelMailin,
                'arrayBranch' => $arrayBranch,
                'arrayModels' => $arrayModels,
            ]);            
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    
    public function actionMail_in()
    {
        $queryMailin = Leads::find()->where(['leads_type' => 1])->orderBy('date_create DESC');
        $modelMailin = new ActiveDataProvider(['query' => $queryMailin, 'pagination' => ['pageSize' => 10]]);

        return $this->render('mail_in',[
            'modelMailin' => $modelMailin,
        ]);
    }
    
    public function actionPick_ups()
    {
        $queryPickups = Leads::find()->where(['leads_type' => 3]);
        $modelPickups = new ActiveDataProvider(['query' => $queryPickups, 'pagination' => ['pageSize' => 10]]);

        return $this->render('pick_ups',[
            'modelPickups' => $modelPickups,
        ]);
    }
    
    public function actionCorporate()
    {
        $queryCorporate = Leads::find()->where(['leads_type' => 4])->orderBy('date_create DESC');
        $modelCorporate = new ActiveDataProvider(['query' => $queryCorporate, 'pagination' => ['pageSize' => 10]]);

        return $this->render('corporate',[
            'modelCorporate' => $modelCorporate,
        ]);
    }
    
    public function actionCorporateshow($id)
    {
        $modelCorporate = Leads::find()->where(['id' => $id])->one();
        $modelCorporate->scenario = 'all';
        if($modelCorporate){
            $modelNewLeadmessage = new Leadsmessage();
            $modelNewLeadmessage->scenario = 'add';
            
            
            if($_POST){
                if(isset($_POST['sent_commect'])){
                    if($modelNewLeadmessage->load(Yii::$app->request->post())){
                        if($modelNewLeadmessage->save()){

                            \Yii::$app->mailer->compose()
                            ->setFrom('support@phonerepairs.com')->setFrom([\Yii::$app->params['supportEmail'] => 'Phone Repairs 4u'])
                            ->setTo($modelCorporate->email)
                            ->setSubject('New comment for Reference: ('.$modelCorporate->key.$modelCorporate->id.')')
                            ->setHtmlBody("You have new comment for Reference: (".$modelCorporate->key.$modelCorporate->id.") 
                                    <html>
                                                  <head>
                                                          <link rel='stylesheet' href=''css='http://phonerepairs4u.co.uk/fonts.css'>
                                                  </head>
                                                       <body>
                                                         <div style='text-align: center'>
                                                              <div style='display: inline-block; width: 50%; text-align: right'>
                                                                      <ul>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_facebook."'>
                                                                                  <img  src='http://phonerepairs4u.co.uk/images/social_links/facebook.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_twitter."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/tvinter.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a  href='".$social_links_linkedin."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/telephoneinstagram.png'>
                                                                              </a>
                                                                          </li>
                                                                          <li style='list-style: none; display: inline-block'>
                                                                              <a href='".$social_links_telephoneinstagram."'>
                                                                                  <img src='http://phonerepairs4u.co.uk/images/social_links/linkedin.png'>
                                                                              </a>
                                                                      </ul>
                                                                  </div>
                                                              </div>
                                                            <div style='text-align: center'>
                                                                <img src='http://phonerepairs4u.co.uk/images/logotyp.png'></div>
                                                                <br>
                                                                <div style='text-align: center'>
                                                                    <div style='display: inline-block; text-align: left'>
                                                                            ''".$modelNewLeadmessage->content."'' 
                                                                        <br>
                                                                         <br>

                                                                        <div style=' text-align: left'>
                                                                            Thank you for contacting Phone Repairs 4U
                                                                        </div>
                                                                        <br>
                                                                        <br>
                                                                        <br>

                                                                        <div style=' text-align: left'>Have you had a good service ? Please <a href='https://www.freeindex.co.uk/write-review.htm?id=586241'>Review</a> on our Phones Repair4U website.</div>

                                                                        <div style='text-align: center'> <a href='http://phonerepairs4u.co.uk' style='text-decoration: none'>phonerepairs4u.co.uk <br> 020 3302 2930 </a></div>
                                                                    </div>                                                              
                                                                </div>
                                                            </div>
                                                    </body>
                                        </html>")
                            ->send();
                            
                            Yii::$app->session->setFlash('message_send');
                        }else{
                            Yii::$app->session->setFlash('message_not_send');  
                        }
                    }
                }                
                if(isset($_POST['update_corporate'])){
                    if($modelCorporate->load(Yii::$app->request->post())){
                        if($modelCorporate->save()){
                            Yii::$app->session->setFlash('lead_updated');
                        }else{
                            Yii::$app->session->setFlash('lead_not_updated');  
                        }
                    }                    
                }                
                
            }
            $queryLeadmessage = new Query();
            $queryLeadmessage->select(['*', 'id' => '{{leads_message}}.id', 'date_create' => '{{leads_message}}.date_create'])
                            ->from('{{leads_message}}')
                            ->join('LEFT JOIN',
                                        '{{users}}',
                                        '{{users}}.id = {{leads_message}}.user_id')
                    ->where(['{{leads_message}}.leads_id' => $id]);
            $command = $queryLeadmessage->createCommand();
            $modelLeadmessage = $command->queryAll();
            
//            $arrayBranch = ArrayHelper::map(Branches::find()->asArray()->all(), 'id', 'address');
//            $arrayModels = ArrayHelper::map(Models::find()->asArray()->all(), 'id', 'name');
//            $arrayService = ArrayHelper::map(Service::find()->asArray()->all(), 'id', 'name');
//            $arrayAssigned = ArrayHelper::map(User::find()->where(['type' => 'admin'])->where(['type' => 'manager'])->asArray()->all(), 'id', 'name');
            
            return $this->render('corporateshow',[
                'modelNewLeadmessage' => $modelNewLeadmessage,
                'modelLeadmessage' => $modelLeadmessage,
                'modelCorporate' => $modelCorporate,
//                'arrayAssigned' => $arrayAssigned,
//                'arrayService' => $arrayService,
//                'arrayBranch' => $arrayBranch,
//                'arrayModels' => $arrayModels,
            ]);            
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
}
