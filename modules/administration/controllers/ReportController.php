<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\User;
use app\models\Pages;
use app\models\Leads;
use app\models\Newsletter;
use app\models\Setting;
use yii\db\Query;
use app\modules\administration\models\LoginForm;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class ReportController extends Controller
{
     public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        $this->layout = 'adminLayout';
        if((!\Yii::$app->user->isGuest) && ((Yii::$app->user->identity->type == 'admin') || (Yii::$app->user->identity->type == 'manager'))){
            return parent::beforeAction($action);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionIndex()
    {
        
        $queryLeads = new Query();
        $queryLeads->select(['*', 'id' => '{{leads}}.id', 'name' => '{{leads}}.name', 'date_create' => '{{leads}}.date_create', 'model_id' => '{{leads}}.model_id',  'service_id' => '{{leads}}.service_id'])
                        ->from('{{leads}}')
                        ->join('LEFT JOIN',
                                    '{{models}}',
                                    '{{models}}.id = {{leads}}.model_id')
                        ->join('LEFT JOIN',
                                    '{{models_service}}',
                                    '{{models_service}}.models_id = {{leads}}.model_id AND {{models_service}}.service_id = {{leads}}.service_id')
                                    ->where(['not in','{{leads}}.user_id', 'NULL']);
        if($_GET){
//            var_dump($_GET);exit;
            if(isset($_GET['date_from'])){
                if(($_GET['date_from'] != '') && (($_GET['date_from'] != '0000-00-00 00:00'))){
                    $queryLeads->andWhere("{{leads}}.date_create > '".$_GET['date_from']."'");
                }
            }
            
            if(isset($_GET['date_to'])){
                if(($_GET['date_to'] != '') && (($_GET['date_to'] != '0000-00-00 00:00'))){
                    $queryLeads->andWhere("{{leads}}.date_create < '".$_GET['date_to']."'");
                }
            }
            
            if(isset($_GET['repairs_type'])){
                if($_GET['repairs_type'] != ''){
                    $queryLeads->andWhere(['{{models}}.repairs_id' => (int)$_GET['repairs_type']]);
                }
            }
            
            if(isset($_GET['service'])){
                if($_GET['service'] != ''){
                    $queryLeads->andWhere(['{{leads}}.service_id' => (int)$_GET['service']]);
                }
            }
            
            if(isset($_GET['models'])){
                if($_GET['models'] != ''){
                    $queryLeads->andWhere(['{{leads}}.model_id' => (int)$_GET['models']]);
                }
            }
            if(isset($_GET['branch'])){
                if($_GET['branch'] != ''){
                    $queryLeads->andWhere(['{{leads}}.branch_id' => (int)$_GET['branch']]);
                }
            }
        }
        $total_price = ($queryLeads->sum('{{models_service}}.price'));
        $queryLeads->orderBy('{{leads}}.date_create DESC');
        
        $arrayRepairs = ArrayHelper::map(\app\models\Repairs::find()->all(), 'id', 'name');
        $arrayModels = ArrayHelper::map(\app\models\Models::find()->all(), 'id', 'name');
        $arrayService = ArrayHelper::map(\app\models\Service::find()->all(), 'id', 'name');
        $arrayBranch = [];
        foreach(\app\models\Branches::find()->all() as $branch){
            $arrayBranch[$branch['id']] = strip_tags($branch['address_street']);                                    
        }
        
        $modelLeads = new ActiveDataProvider(['query' => $queryLeads, 'pagination' => ['pageSize' => 50]]);        
        return $this->render('index',[
            'modelLeads' => $modelLeads,
            'total_price' => $total_price,
            
            'arrayRepairs' => $arrayRepairs,
            'arrayModels' => $arrayModels,
            'arrayBranch' => $arrayBranch,
            'arrayService' => $arrayService,
        ]);
    }
    
}
