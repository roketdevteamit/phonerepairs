<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Pages;
use app\models\Setting;
use app\models\Slider;
use app\modules\administration\models\LoginForm;
use yii\data\ActiveDataProvider;
/**
 * Default controller for the `administration` module
 */
class LoginController extends Controller
{
    
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        $this->layout = 'adminLayout';
        return parent::beforeAction($action);
    }
    
    public function actionIndex()
    {
        if(\Yii::$app->user->isGuest){
            
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                if(Yii::$app->user->identity->type == 'admin'){
                    return $this->redirect('/administration/home');
                }else{
                    return $this->redirect('/');
                }
            }
            return $this->render('index', [
                'model' => $model,
            ]);
            
        }else{
           return $this->redirect('administration/home'); 
        }
        
    }
        
}
