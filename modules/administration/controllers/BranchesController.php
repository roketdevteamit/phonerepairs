<?php

namespace app\modules\administration\controllers;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Branches;
use app\models\Pages;
use app\models\Setting;
use app\modules\administration\models\LoginForm;
use yii\data\ActiveDataProvider;

class BranchesController extends Controller
{
     public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        $this->layout = 'adminLayout';
        if((!\Yii::$app->user->isGuest) && ((Yii::$app->user->identity->type == 'admin') || (Yii::$app->user->identity->type == 'manager'))){
            return parent::beforeAction($action);
        }else{
            throw new \yii\web\NotFoundHttpException();
        }
    }
    
    public function actionIndex()
    {
        $modelNewBranches = new Branches;
        $modelNewBranches->scenario = 'add';
        //'signup' => [ 'password', 'password_hash', 'email', 'auth_key'],
        
        if($modelNewBranches->load(Yii::$app->request->post())){
            if ($modelNewBranches->save()) {
                Yii::$app->session->setFlash('add_branch');                        
            }else{
                Yii::$app->session->setFlash('not_add_branch');                    
            }
        }
        $queryBranches = Branches::find();
        $modelBranches = new ActiveDataProvider(['query' => $queryBranches, 'pagination' => ['pageSize' => 10]]);
        
        return $this->render('branches',[
            'modelBranches' => $modelBranches,
            'modelNewBranches' => $modelNewBranches
        ]);
    }
    
    public function actionBranchupdate($id=null)
    {
        $modelBranches = Branches::find()->where(['id' => $id])->one();
        $modelBranches->scenario = 'add';
        if($modelBranches->load(Yii::$app->request->post())){
            if ($modelBranches->save()) {
                Yii::$app->session->setFlash('update_branch');                        
                return $this->redirect(Url::home().'administration/branches/index');
            }else{
                Yii::$app->session->setFlash('not_update_branch');                    
                return $this->redirect(Url::home().'administration/branches/index');
            }
        }
        
        return $this->render('branchupdate', [
            'modelBranches' => $modelBranches,
        ]);
    }
    
    public function actionBranchdelete($id=null)
    {
        $modelBranches = Branches::find()->where(['id' => $id])->one();
        if($modelBranches->delete()){
            Yii::$app->session->setFlash('delete_branch');
            return $this->redirect(Url::home().'administration/branches/index');
        }else{
            Yii::$app->session->setFlash('not_delete_branch');
            return $this->redirect(Url::home().'administration/branches/index');
        }
    }
    
    
}
