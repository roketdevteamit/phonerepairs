<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'repairs@phonerepairs4u.co.uk',
    'user.passwordResetTokenExpire' => '259200',
];
