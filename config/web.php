<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
            'allowedIPs' => [
                '127.0.0.1', '::1',
            ],
            'on beforeAction' => function(){
                \Yii::$app->response->format = 'html';
            },
        ],

        'gii' => 'yii\gii\Module',
        
        'administration' => [
            'class' => 'app\modules\administration\Module',
        ],
    ],
    'components' => [
//        'language'=> 'en-US',
//        'i18n' => [
//            'translations' => [
//                '*' => [
//                    'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@webroot/lang',
//                    'sourceLanguage' => 'en',
//                    'fileMap' => [
//                        'main' => 'main.php',
//                    ],
//                ],
//                'eauth' => [
//                    'class' => 'yii\i18n\PhpMessageSource',
//                    'basePath' => '@eauth/messages',
//                ],
//            ],
//        ],
        'eauth' => [
            'class' => 'nodge\eauth\EAuth',
            'popup' => true, // Use the popup window instead of redirecting.
            'cache' => false, // Cache component name or false to disable cache. Defaults to 'cache' on production environments.
            'cacheExpire' => 0, // Cache lifetime. Defaults to 0 - means unlimited.
            'httpClient' => [
                // uncomment this to use streams in safe_mode
                //'useStreamsFallback' => true,
            ],
            'services' => [ // You can change the providers and their classes.
//                'google' => [
//                    // register your app here: https://code.google.com/apis/console/
//                    'class' => 'nodge\eauth\services\GoogleOAuth2Service',
//                    'clientId' => '...',
//                    'clientSecret' => '...',
//                    'title' => 'Google',
//                ],
//                'twitter' => [
//                    // register your app here: https://dev.twitter.com/apps/new
//                    'class' => 'nodge\eauth\services\TwitterOAuth1Service',
//                    'key' => '...',
//                    'secret' => '...',
//                ],
//                'yandex' => [
//                    // register your app here: https://oauth.yandex.ru/client/my
//                    'class' => 'nodge\eauth\services\YandexOAuth2Service',
//                    'clientId' => '...',
//                    'clientSecret' => '...',
//                    'title' => 'Yandex',
                //],
                'facebook' => [
                    // register your app here: https://developers.facebook.com/apps/
                    'class' => 'nodge\eauth\services\FacebookOAuth2Service',
                    'clientId' => '675126769341501',
                    'clientSecret' => '974bbc44791e323f22e24b556096056d',
                ],
//                'yahoo' => [
//                    'class' => 'nodge\eauth\services\YahooOpenIDService',
//                    //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
//                ],
//                'linkedin' => [
//                    // register your app here: https://www.linkedin.com/secure/developer
//                    'class' => 'nodge\eauth\services\LinkedinOAuth1Service',
//                    'key' => '...',
//                    'secret' => '...',
//                    'title' => 'LinkedIn (OAuth1)',
//                ],
//                'linkedin_oauth2' => [
//                    // register your app here: https://www.linkedin.com/secure/developer
//                    'class' => 'nodge\eauth\services\LinkedinOAuth2Service',
//                    'clientId' => '...',
//                    'clientSecret' => '...',
//                    'title' => 'LinkedIn (OAuth2)',
//                ],
//                'github' => [
//                    // register your app here: https://github.com/settings/applications
//                    'class' => 'nodge\eauth\services\GitHubOAuth2Service',
//                    'clientId' => '...',
//                    'clientSecret' => '...',
//                ],
//                'live' => [
//                    // register your app here: https://account.live.com/developers/applications/index
//                    'class' => 'nodge\eauth\services\LiveOAuth2Service',
//                    'clientId' => '...',
//                    'clientSecret' => '...',
//                ],
//                'steam' => [
//                    'class' => 'nodge\eauth\services\SteamOpenIDService',
//                    //'realm' => '*.example.org', // your domain, can be with wildcard to authenticate on subdomains.
//                    'apiKey' => '...', // Optional. You can get it here: https://steamcommunity.com/dev/apikey
//                ],
//                'instagram' => [
//                    // register your app here: https://instagram.com/developer/register/
//                    'class' => 'nodge\eauth\services\InstagramOAuth2Service',
//                    'clientId' => '...',
//                    'clientSecret' => '...',
//                ],
//                'vkontakte' => [
//                    // register your app here: https://vk.com/editapp?act=create&site=1
//                    'class' => 'nodge\eauth\services\VKontakteOAuth2Service',
//                    'clientId' => '...',
//                    'clientSecret' => '...',
//                ],
//                'mailru' => [
//                    // register your app here: http://api.mail.ru/sites/my/add
//                    'class' => 'nodge\eauth\services\MailruOAuth2Service',
//                    'clientId' => '...',
//                    'clientSecret' => '...',
//                ],
//                'odnoklassniki' => [
//                    // register your app here: http://dev.odnoklassniki.ru/wiki/pages/viewpage.action?pageId=13992188
//                    // ... or here: http://www.odnoklassniki.ru/dk?st.cmd=appsInfoMyDevList&st._aid=Apps_Info_MyDev
//                    'class' => 'nodge\eauth\services\OdnoklassnikiOAuth2Service',
//                    'clientId' => '...',
//                    'clientSecret' => '...',
//                    'clientPublic' => '...',
//                    'title' => 'Odnoklas.',
//                    ],
            ],
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
           // 'class'=>'app\components\LangUrlManager',
            'rules'=>[
                '/' => 'site/index',
                'repairs/<repair_slug>'=>'models/repairs',
                'page/<page_slug>'=>'pages/page',
                'repairs/<repair_slug>/<model_slug>'=>'models/models',
                'repairs/<repair_slug>/<model_slug>/<service_slug>'=>'models/service',
                'search'=>'models/search',
                'login' => 'site/login',
                'makeenquiry' => 'site/makeenquiry',
                'login/<service:google|facebook|etc>' => 'site/login',
                '<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
                'administration' => 'administration/login/index',
                'administration/home' => 'administration/default/home',
                'administration/default/pageupdate/<id:\d+>' => 'administration/default/pageupdate',
                'administration/leads/bookings/<branch:\d+>' => 'administration/leads/bookings',
            ]
        ],
        'request' => [
                'baseUrl' => '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '8v_9fJLT4jEUOiOIfuhoGe17l9o_Dzjg',
            //'class' => 'app\components\LangRequest'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
              'class' => 'Swift_SmtpTransport',
              'host' => 'mail.phonerepairs4u.co.uk',
              'username' => 'repairs@phonerepairs4u.co.uk',
              'password' => 'OW^T@KnOlfTS',
              'port' => '25',
              'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/eauth.log',
                    'categories' => ['nodge\eauth\*'],
                    'logVars' => [],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
