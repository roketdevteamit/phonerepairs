<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

class Leadsmessage extends \yii\db\ActiveRecord
{
    public $services;
    public $servicesprice;
    
    public static function tableName()
    {
        return '{{leads_message}}';
    }

//    public function behaviors()
//    {
//        return [
//            'slug' => [
//                'class'         => 'app\behaviors\Slug',
//                'in_attribute'  => 'name',
//                'out_attribute' => 'slug',
//                'translit'      => true
//            ]
//        ];
//    }
    
    public function scenarios()
    {
        return [
            'add' => ['leads_id', 'user_id', 'content'],
        ];
    }
    
    public function rules()
    {
        return [
            [['content'], 'required'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
    //        'id' => Yii::t('app', 'Номер'),
        ];
    }
    
}
