<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
//leads_type: 0-Enquiry, 1-Mail In, 2-Booking, 3-Pick ups 4-Corporate

//status: Order received
//Order in progress
//Awaiting details from client
//Order ready for packaging 
//Order on route to delivery
class Leads extends \yii\db\ActiveRecord
{
    public $repairs;
     
    public static function tableName()
    {
        return '{{leads}}';
    }

    public function scenarios()
    {
        return [
            'all' => ['name', 'key', 'phone_number', 'email', 'device_model', 'device_color',
                'device_fault', 'address', 'description', 'company_r_name', 'phone_password', 'company_url',
                'hear_us', 'status', 'assigned_id', 'branch_id', 'booking_date', 'IMEI', 'quote', 'leads_type', 'service_id','model_id', 'user_id', 'key'],
            'add_user' => ['user_id'],
            
            'enquiry_home_page' => ['key', 'name', 'phone_number', 'email', 'device_model', 'description', 'hear_us', 'user_id','leads_type'],
            'booking_service_page' => ['key', 'name', 'booking_date', 'phone_number', 'email','device_color', 'description', 'phone_password', 'service_id','model_id', 'branch_id', 'user_id','leads_type'],
            'mail_service_page' => ['key', 'name', 'phone_number', 'email','description', 'phone_password','service_id','model_id', 'address', 'user_id','leads_type'],
            
            'booking_book_page' => ['booking_date','key', 'name', 'phone_number', 'email','device_color','model_id', 'service_id', 'device_fault', 'device_model', 'branch_id', 'user_id', 'status', 'leads_type'],
            
            'mail_page' => ['key', 'name', 'address', 'email', 'phone_number','device_model', 'phone_password', 'quote',  'IMEI', 'description', 'user_id','leads_type'],
            
            'corporate_page' => ['key', 'name', 'company_r_name', 'email', 'phone_number', 'company_url', 'description', 'user_id','leads_type'],
            
            'change_type' => ['booking_date', 'leads_type','status'],
            'add_identification_id' => ['identification_id'],
            
            'update' => ['name','repairs'],
        ];
    }
    
    public function rules()
    {
        return [
            [['name', 'phone_number', 'email'], 'required'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
    //        'id' => Yii::t('app', 'Номер'),
        ];
    }
    
}
