<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

class Branches extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{branches}}';
    }

    public function scenarios()
    {
        return [
            'add' => ['address', 'address_street', 'telephone', 'open'],
            'update' => ['address', 'address_street', 'telephone', 'open'],
        ];
    }
    
    public function rules()
    {
        return [
            //[['name'], 'required'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
    //        'id' => Yii::t('app', 'Номер'),
        ];
    }

}
