<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Brandrepairs;

class Brand extends \yii\db\ActiveRecord
{
    public $repairs;
     
    public static function tableName()
    {
        return '{{brand}}';
    }

    public function scenarios()
    {
        return [
            'add' => ['name','repairs', 'img_src', 'sort'],
            'update' => ['name','repairs', 'img_src', 'sort'],
        ];
    }
    
    public function rules()
    {
        return [
            [['name'], 'required'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
    //        'id' => Yii::t('app', 'Номер'),
        ];
    }

    public function getRepairs()
    {
        return $this->hasMany(Brandrepairs::className(), ['repairs_id' => 'id']);
    }
    
}
