<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

//status: 1 - продается, 2 - Продано, 3 - в архиве
class Pages extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%pages}}';
    }
    
    public function behaviors()
    {
        return [
            'slug' => [
                'class'         => 'app\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }
    
    public function scenarios()
    {
        return [
            'add' => ['name', 'header_img', 'content','sort','status'],
            'update' => ['name', 'header_img', 'content', 'sort', 'status'],
        ];
    }
    
    public function rules()
    {
        return [
            [['name'], 'required'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
    //        'id' => Yii::t('app', 'Номер'),
        ];
    }

}
