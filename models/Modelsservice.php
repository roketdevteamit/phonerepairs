<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

class Modelsservice extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{models_service}}';
    }

    public function scenarios()
    {
        return [
            'add' => ['service_id', 'models_id', 'price'],
            'update' => ['service_id', 'models_id', 'price'],
            'update_price' => ['price'],
        ];
    }
    
    public function rules()
    {
        return [
           // [['name'], 'required'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
    //        'id' => Yii::t('app', 'Номер'),
        ];
    }

}
