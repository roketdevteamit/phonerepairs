<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

class Models extends \yii\db\ActiveRecord
{
    public $services;
    public $servicesprice;
    
    public static function tableName()
    {
        return '{{models}}';
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class'         => 'app\behaviors\Slug',
                'in_attribute'  => 'name',
                'out_attribute' => 'slug',
                'translit'      => true
            ]
        ];
    }
    
    public function scenarios()
    {
        return [
            'add' => ['name', 'brand_id', 'repairs_id','memory', 'color','repairs', 'servicesprice', 'status', 'services', 'img_src', 'note'],
            'update' => ['name', 'brand_id', 'repairs_id','memory', 'color','repairs', 'servicesprice', 'status', 'services', 'img_src', 'note'],
            'change_status' => ['status']
        ];
    }
    
    public function rules()
    {
        return [
            [['name','brand_id','repairs_id'], 'required'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
            'repairs_id' => Yii::t('app', 'Please Choose Repairs'),
            'brand_id' => Yii::t('app', 'Please Choose Brands'),
        ];
    }
    
}
