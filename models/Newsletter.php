<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

class Newsletter extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{newsletter}}';
    }

    public function scenarios()
    {
        return [
            'add' => ['content', 'user_id']
        ];
    }
    
    public function rules()
    {
        return [
            [['content', 'user_id'], 'required'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
    //        'id' => Yii::t('app', 'Номер'),
        ];
    }
    
}
