<?php

namespace app\models;

use Yii;
use app\components\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

//status: 1 - продается, 2 - Продано, 3 - в архиве
class Slider extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
        return '{{%home_slider}}';
    }

    public function scenarios()
    {
        return [
            'add' => ['img_src', 'some_note', 'sort'],
            'update' => ['img_src', 'some_note', 'sort'],
        ];
    }
    
    public function rules()
    {
        return [
            [['img_src'], 'required'],
        ];
    }
    
    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->date_create = date("Y-m-d H:i:s");
        }

        return parent::beforeSave($insert);
    }

    public function attributeLabels()
    {
        return [
//            'id' => Yii::t('app', 'i'),
        ];
    }

}
