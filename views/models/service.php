<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use kartik\datetime\DateTimePicker;

$this->title = $modelModel->name.' '.$modelService->name.' | Phones Repair 4u';

?>
<div class="container" style="background: #fafafa;">
    <div class="row style_broken_class">
        <h1><?= $modelModel->name; ?> <?= $modelService->name; ?></h1>
        <h2>£<?= $modelModelsService->price; ?></h2>

    </div>
    <div class="">
        <?php
        if (Yii::$app->session->hasFlash('form_sended')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => 'Thank you for contacting us!',
            ]);
        endif;
        if (Yii::$app->session->hasFlash('form_not_sended')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-error',
                ],
                'body' => 'Some error!',
            ]);
        endif;
        ?>
        <ul class="nav nav-tabs tab_title">
            <li class="active"><a data-toggle="tab" href="#booking">Click here to book your repair</a></li>
            <li><a data-toggle="tab" href="#mail">Click here to mail your device</a></li>
        </ul>
        <div class="tab-content">
            <div id="booking" class="tab-pane fade in active class_b">
                <p class="p1">Book your repair</p>
                <p class="p2"><b>£</b>5 discount on repairs when you book online!</p>
                <?php $form = ActiveForm::begin(
                    [
                        'options' => [
                            'class' => 'userform'
                        ]]
                ); ?>
                <div class="col-xs-12" style="padding: 0px">
                    <div class="col-xs-12 col-sm-6 " style="padding: 0px">
                        <div style="height: 0px">
                            <?= $form->field($modelNewLeads, 'key')->hiddenInput(['value' => 'BK'])->label(false); ?>
                            <?= $form->field($modelNewLeads, 'model_id')->hiddenInput(['value' => $modelModel->id])->label(false); ?>
                            <?= $form->field($modelNewLeads, 'service_id')->hiddenInput(['value' => $modelService->id])->label(false); ?>
                            <?= $form->field($modelNewLeads, 'leads_type')->hiddenInput(['value' => '2'])->label(false); ?>
                        </div>
                        <div class="col-xs-12">
                            <p>Name</p>
                            <?= $form->field($modelNewLeads, 'name')->textinput(['placeholder' => 'Contact Name'])->label(false); ?>
                        </div>
                        <div class="col-xs-12">
                            <p>Email</p>
                            <?= $form->field($modelNewLeads, 'email')->input('email', ['placeholder' => 'Contact Email Address'])->label(false); ?>
                        </div>
                        <div class="col-xs-12">
                            <p>Phone Number</p>
                            <?= $form->field($modelNewLeads, 'phone_number')->textinput(['placeholder' => 'Contact Number'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 " style="padding: 0px">
                        <div class="col-xs-12">
                            <p>Device Color</p>
                            <?= $form->field($modelNewLeads, 'device_color')->textinput(['placeholder' => 'Device Colour'])->label(false); ?>
                        </div>
                        <div class="col-xs-12">
                            <p>Booking Date / Time</p>
                            
                            <?= $form->field($modelNewLeads, 'booking_date')->widget(DateTimePicker::classname(), [
                                            'options' => ['placeholder' => date('Y-m-d')],
                                            'type' => DateTimePicker::TYPE_INPUT,
                                            'pluginOptions' => [
                                                    'todayHighlight' => true
                                            ]
                                    ])->label(false); ?>
                            
                        </div>
                        <div class="col-xs-12">
                            <p>Branch</p>
                            <?= $form->field($modelNewLeads, 'branch_id')->dropDownList($branchArray)->label(false); ?>
                        </div>
                        <div class="col-xs-12  button_width userform">
                            <?= Html::submitButton(Yii::t('app', 'Submit'), ['name' => 'bookButton', 'class' => 'btn btn-primary pull-right', 'style' => 'margin: 0px']) ?>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                    <div style="clear: both"></div>
                    <div class="userform line_height_style text-center" style="padding-top: 10px">
                        <p>Book an appointment for your <?= $modelModel->name; ?> <?= $modelService->name; ?> through our booking form above.</p>
                    </div>
                </div>
            </div>
            <div id="mail" class="tab-pane fade class_b">
                <p class="p1">Mail in your device</p>
                <?php $form = ActiveForm::begin(
                    [
                        'options' => [
                            'class' => 'userform'
                        ]]
                ); ?>
                <div class="col-xs-12" style="padding: 0px">
                    <div class="col-xs-12 col-sm-6 " style="padding: 0px">
                        <div style="height: 0px">
                            <?= $form->field($modelNewLeads, 'key')->hiddenInput(['value' => 'ML'])->label(false); ?>
                            <?= $form->field($modelNewLeads, 'model_id')->hiddenInput(['value' => $modelModel->id])->label(false); ?>
                            <?= $form->field($modelNewLeads, 'service_id')->hiddenInput(['value' => $modelService->id])->label(false); ?>
                            <?= $form->field($modelNewLeads, 'leads_type')->hiddenInput(['value' => '1'])->label(false); ?>
                        </div>
                        <div class="col-xs-12">
                            <p>Name:</p>
                            <?= $form->field($modelNewLeads, 'name')->textinput(['placeholder' => 'Contact Name'])->label(false); ?>
                        </div>
                        <div class="col-xs-12">
                            <p>Email:</p>
                            <?= $form->field($modelNewLeads, 'email')->input('email', ['placeholder' => 'Contact Email Address'])->label(false); ?>
                        </div>
                        <div class="col-xs-12">
                            <p>Phone Password:</p>
                            <?= $form->field($modelNewLeads, 'phone_password')->textinput(['placeholder' => 'Phone Password'])->label(false); ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 " style="padding: 0px">
                        <div class="col-xs-12">
                            <p>Phone Number:</p>
                            <?= $form->field($modelNewLeads, 'phone_number')->textinput(['placeholder' => 'Phone Number'])->label(false); ?>
                        </div>
                        <div class="col-xs-12">
                            <p>Full Address:</p>
                            <?= $form->field($modelNewLeads, 'address')->textinput(['placeholder' => 'Full UK Postal Address'])->label(false); ?>
                        </div>
                        <div class="col-xs-12 text_povn" style="padding: 0px">
                            <p>If you live far away from our London branches or you don't have the time during the day
                                to make a trip down to one of our repair centres this is probably the best option for
                                you.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <p>Fault Description</p>
                        <?= $form->field($modelNewLeads, 'description')->textarea(['rows' => '6', 'placeholder' => 'Fault Description'])->label(false); ?>
                    </div>
                    <div class="col-xs-12 button_device">
                        <?= Html::submitButton(Yii::t('app', 'Submit'), ['name' => 'mailButton', 'class' => 'btn btn-primary pull-right']) ?>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="col-xs-12 text_povn_mob" style= "padding: 20px 0;">
                        <p>If you live far away from our London branches or you don't have the time during the day
                            to make a trip down to one of our repair centres this is probably the best option for
                            you.
                        </p>
                    </div>
                </div>
                <div style="clear: both"></div>
                <div class="userform line_height_style" style="padding-top: 10px">
                    <p>All you need to do is simply print out our online repair form, fill it in with your details and
                        information about the faulty device and send in your device.</p>
                    <br>
                    <p> On receiving your device, it will be booked in for a free inspection so we can identify the
                        problem
                        (if unsure) and we will get back to you with a quote (if not already been given).</p>
                    <br>
                    <p> Our mail in service has a maximum turnaround time of 48 hours. Which means from the time you
                        send in
                        your device, you should receive it back repaired within 48 hours (two days).</p>
                </div>
            </div>
        </div>
    </div>
</div>
