<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
$this->title = $modelRepair->name.' | Phones Repair 4u';

?>
<div style="height: 30px;"></div>
<div class="container">
    <div class="row">
        <form method="get">
            <div class="userform">
                    <div class="col-sm-6">
                        <?php 
                            $models_name = '';
                            if(isset($_GET['models_name'])){
                                $models_name = $_GET['models_name'];
                            }
                        ?>
                        <input type="text" class="form-control" name="models_name" value="<?= $models_name; ?>" placeholder="Search for...">
                    </div>
                    <div class="col-sm-5">
                        <?php 
                            $brand_value = '';
                            if(isset($_GET['models_brand'])){
                                $brand_value = $_GET['models_brand'];
                            }
                        ?>
                        <?= Html::dropDownList('models_brand', $brand_value, $arrayBrands, ['class' => 'form-control', 'prompt' => 'View all']); ?>
                    </div>
                    <div class="col-xs-12 col-sm-1">
                          <button type="submit" class="btn btn-default button_search">Go!</button>
                    </div>
            </div>
        </form>
        <?php if($modelModels){ ?>
        <?php foreach($modelModels as $models){ ?>
            <div class="col-sm-2 all_product height_all_product">
                <a href="<?= Url::home().'repairs/'.$modelRepair->slug.'/'.$models['slug']; ?>" >
                    <?php 
                        $img_src = "/images/pages/default.jpg";
                        if(($models['img_src'] != '') && ($models['img_src'] != null)){
                            $img_src = "/images/models/".$models['img_src'];
                        }
                    ?>
                    <img src="<?= $img_src; ?>">
                    <p  ><?= $models['name']; ?></p>
                </a>
            </div>
        <?php } ?>
        <?php }else{ ?>
            <div style="text-align:center;">
                <p>No results</p>
            </div>
        <?php } ?>
    </div>
    <div class="row">
        <?= LinkPager::widget(['pagination'=>$pagination]);  ?>
    </div>
</div>