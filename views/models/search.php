<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

$this->title = 'Search models | Phones Repair 4u';

?>
<div class="container">
    <div class="row">
        
        <div class="col-sm-12 class_search_pading_top">
            <form action="/search" method="get">
                <div class="input-group userform">
                        <input type="text" class="form-control" name="search" placeholder="Search for..." value="<?= $search_value; ?>">
                        <span class="input-group-btn ">
                          <button type="submit" class="btn btn-default button_search" ">Go!</button>
                        </span>
                </div>
            </form>
        </div>
        <div style="height: 30px;"></div>
        <?php if($modelModels){ ?>
            <?php foreach($modelModels as $models){ ?>

                <div class="col-sm-2 all_product height_all_product">
                    <a href="<?= Url::home().'repairs/'.$models['repairs_slug'].'/'.$models['models_slug']; ?>" >
                        <?php
                            $img_src = "/images/pages/default.jpg";
                            if(($models['img_src'] != '') && ($models['img_src'] != null)){
                                $img_src = "/images/models/".$models['img_src'];
                            }
                        ?>
                        <img src="<?= $img_src; ?>">
                        <p><?= $models['name']; ?></p>
                    </a>
                </div>
            <?php } ?>
        <?php }else{ ?>
            no search results
        <?php } ?>
    </div>
    <div class="row">
        <?= LinkPager::widget(['pagination'=>$pagination]);  ?>
    </div>
</div>