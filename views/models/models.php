<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
$this->title = $modelModel->name.' Repairs | Phones Repair 4u';
?>
<div class="container">
    <div class="row style_text_models">
        <h1><?= $modelModel->name; ?> repairs</h1>
        <p style="text-align: center">If you are not sure which repair your device(s) needs, feel free to use our
            enquiry form or give us a call on 020 3897 2299!</p>
        <div class="col-sm-9 col_style">
            <div class="red_information">We do a range of <?= $modelModel->name; ?>, click a fault below to fill out the form.</div>
        </div>
    </div>
    <div class="col-sm-10 col_style">
        <div class="col-sm-6 table_class">
            <table style="width:80%;">
                <?php foreach ($modelServices as $service) { ?>
                    <tr>
                        <td style="padding:5px;text-align: left;font-size: 20px">
                            <a href="<?= Url::home() . 'repairs/' . $modelRepair->slug . '/' . $modelModel->slug . '/' . $service['slug']; ?>"><?= $service['name']; ?></a>
                        </td>
                        <td style="padding:5px;font-weight: 800;color: #ff0000;text-align: right;font-size: 20px">
                            £<?= $service['price']; ?></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <div class="col-sm-4 pull-right">
            <?php
            $img_src = "/images/pages/default.jpg";
            if (($modelModel->img_src != '') && ($modelModel->img_src != null)) {
                $img_src = "/images/models/" . $modelModel->img_src;
                ?>
                <img src="<?= $img_src; ?>" style="width:100%;">
           <?php } ?>

        </div>
    </div>
</div>