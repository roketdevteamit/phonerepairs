<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
$this->title = 'Login | Phones Repair 4u';
?>
<?php
if (Yii::$app->getSession()->hasFlash('error')) {
    echo '<div class="alert alert-danger">' . Yii::$app->getSession()->getFlash('error') . '</div>';
}
?>
<div class="container">
    <div class="col-sm-6" style="margin:0 auto;margin-top:50px;float:none;">
        <div class="site-login ">
            <h1 class="title">Login</h1>
            <div class="col-sm-12 text_logout">

                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n<div >{input}</div>\n<div >{error}</div>",
                        'labelOptions' => ['class' => ' control-label'],
                    ],
                ]); ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true,'placeholder'=>'Enter your email', 'class' => 'form_admin'])->label('Email')->label(false) ?>

                <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Enter your password','class' => 'form_admin'])->label(false) ?>


                <div class="forgor_password">
                <?= HTML::a('Forgot password?', Url::home() . 'site/request-password-reset'); ?>
                </div>
                <div class="form-group">
                    <div class="button_login">
                        <?= Html::submitButton('Login', ['class' => 'btn btn-primary ', 'name' => 'login-button']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<!--<p class="lead">Do you already have an account on one of these sites? Click the logo to log in with it here:</p>-->
<?php //echo \nodge\eauth\Widget::widget(['action' => 'site/login']); ?>
