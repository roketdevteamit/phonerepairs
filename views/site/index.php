<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\bootstrap\ActiveForm;

$this->title = 'Same Day iPhone & Samsung Device Repair Centre in London';
?>
<?php Yii::$app->language = 'en-US'; ?>

<div class="rev_slider_wrapper">
    <div id="slider1" class="rev_slider" data-version="5.0">
            <ul>
                <?php foreach ($modelSlider as $value) { ?>
                    <li data-transition="fade">
                    <!-- MAIN IMAGE --> <img width="1920" src="/images/slider/<?= $value['img_src'] ?>" height="330" data-duration="30000" data-ease="Power3.ease" data-scalestart="100" data-scaleend="120" data-bgposition="center center"  class="rev-slidebg">
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption tp-resizeme" data-x="left" data-hoffset="40" data-y="top" data-voffset="105" data-transform_in="x:800px;opacity:0;s:1500;e:Power4.easeInOut;" data-transform_out="o:0" data-whitespace="nowrap" data-start="1200">
                            <div class="text-sl-mid img_design_slider">
                                    <?= $value['some_note'] ?>
                            </div>
                    </div>
                    </li>
                <?php } ?>
            </ul>
    </div>
     <!-- END REVOLUTION SLIDER -->
</div>
<!--
<div class="slider">
    <div class="owl-carousel owl-theme ">
        <?php
        foreach ($modelSlider as $value) {
//var_dump($value)
            ?>

            <div class="item  img_design_slider"
                     style="background-image: url('/images/slider/<?= $value['img_src'] ?>'); box-shadow: 0 0 0 1600px rgba(0, 0, 0, 0.33) inset;">
                    <p> <?= $value['some_note'] ?></p>
                <img src="/images/slider/2/5-off.png">
            </div>
        <?php } ?>
    </div>
</div>-->
<div class=" container padding-top_20" >
    <div class="col-xs-12 col-sm-6 left_panel">
        <h1>ABOUT US</h1>
        <?= $configData['about']; ?>
    </div>
    <div class="col-xs-12 col-sm-6 style_right_panel">
        <?php
        if (Yii::$app->session->hasFlash('form_sended')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => 'Thank you for contacting us!',
            ]);
        endif;
        if (Yii::$app->session->hasFlash('form_not_sended')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-error',
                ],
                'body' => 'Some error!',
            ]);
        endif;
        ?>
        <p class="title_right_panel">GET A FREE QUOTE</p>
        <?php $form = ActiveForm::begin(); ?>
        <div class="form_group" style="height: 0px;">
            <?= $form->field($modelNewLeads, 'key')->hiddenInput(['value' => 'EN'])->label(false); ?>
            <?= $form->field($modelNewLeads, 'leads_type')->hiddenInput(['value' => 0])->label(false); ?>
        </div>
        <div class="col-sm-6">
            <p class="phone_p">Name:</p>
            <?= $form->field($modelNewLeads, 'name')->textinput()->label()->label(false); ?>
        </div>
        <div class="col-sm-6">
            <p class="phone_p">Phone:</p>
            <?= $form->field($modelNewLeads, 'phone_number')->textinput()->label(false); ?>
        </div>
        <div class="col-sm-6">
            <p class="phone_p">Email:</p>
            <?= $form->field($modelNewLeads, 'email')->input('email')->label(false); ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelNewLeads, 'device_model')->textinput(); ?>
        </div>
        <div class="col-sm-12">
            <p class="phone_p">Description</p>
            <?= $form->field($modelNewLeads, 'description')->textarea(['rows' => '6'])->label(false); ?>
        </div>
        <div class="col-sm-12">
            <div class="form_height"></div>
            <?= $form->field($modelNewLeads, 'hear_us')->dropDownList([
                '1' => 'Google',
                '2' => 'Search Engine',
                '3' => 'External Website',
                '4' => 'Word of Mouth',
                '5' => 'Leaflets/Newspaper',
                '6' => 'Yell.com',
                '7' => 'Bing',
                '8' => 'Yahoo'
            ], ['prompt' => 'Please select'])->label('Where did you hear about us?'); ?>
        </div>
        <div class="col-sm-12">
            <input type="text" name="name-email" class="hide">
        </div>
        <div class="col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary pull-right button_right_menu']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-xs-12">
        <div class="col-sm-12">
        <h1 style="font-family: 'Poppins_Bold', sans-serif;
    font-style: normal;
    color: #222222;
    text-rendering: optimizeLegibility;
    margin-top: 0.2rem;
    margin-bottom: 1.5rem;
    font-size: 28px;">Our Feedback</h1>
        <hr>
        </div>
        <div class="col-sm-6" style="text-align: center">
            <script type="text/JavaScript" src="https://www.freeindex.co.uk/widgets/fiwidget.asp?lid=586241%26tit%3DOur%2520latest%2520reviews%2520on%2520FreeIndex%2Eco%2Euk%26wid%3D100PC%26agt%3D1%26rak%3D1%26dis%3D0%26wri%3D1%26nrv%3D2%26rts%3DM%26theme%3Dlight"></script>
            <a href="http://www.freeindex.co.uk/profile(phonerepairs4u)_586241.htm">Visit PhoneRepairs4u - iPhone Repair Centre on FreeIndex</a>
        </div>
    </div>
</div>