<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Alert;
use yii\bootstrap\ActiveForm;

$this->title = 'Phone Repairs 4u';
?>
<?php Yii::$app->language = 'en-US'; ?>

<div class=" container">
    <div class="col-xs-12 col-sm-12 style_right_panel padding-top_20">
        <?php
        if (Yii::$app->session->hasFlash('form_sended')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-info',
                ],
                'body' => 'Thank you for contacting us!',
            ]);
        endif;
        if (Yii::$app->session->hasFlash('form_not_sended')):
            echo Alert::widget([
                'options' => [
                    'class' => 'alert-error',
                ],
                'body' => 'Some error!',
            ]);
        endif;
        ?>
        <p class="title_right_panel">GET A FREE QUOTE</p>
        <?php $form = ActiveForm::begin(); ?>
        <div class="form_group" style="height: 0px;">
            <?= $form->field($modelNewLeads, 'key')->hiddenInput(['value' => 'EN'])->label(false); ?>
            <?= $form->field($modelNewLeads, 'leads_type')->hiddenInput(['value' => 0])->label(false); ?>
        </div>
        <div class="col-sm-6">
            <p class="phone_p">Name:</p>
            <?= $form->field($modelNewLeads, 'name')->textinput()->label()->label(false); ?>
        </div>
        <div class="col-sm-6">
            <p class="phone_p">Phone:</p>
            <?= $form->field($modelNewLeads, 'phone_number')->textinput()->label(false); ?>
        </div>
        <div class="col-sm-6">
            <p class="phone_p">Email:</p>
            <?= $form->field($modelNewLeads, 'email')->input('email')->label(false); ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($modelNewLeads, 'device_model')->textinput(); ?>
        </div>
        <div class="col-sm-12">
            <p class="phone_p">Description</p>
            <?= $form->field($modelNewLeads, 'description')->textarea(['rows' => '6'])->label(false); ?>
        </div>
        <div class="col-sm-12">
            <div class="form_height"></div>
            <?= $form->field($modelNewLeads, 'hear_us')->dropDownList([
                '1' => 'Google',
                '2' => 'Search Engine',
                '3' => 'External Website',
                '4' => 'Word of Mouth',
                '5' => 'Leaflets/Newspaper',
                '6' => 'Yell.com',
                '7' => 'Bing',
                '8' => 'Yahoo'
            ], ['prompt' => 'Please select'])->label('Where did you hear about us?'); ?>
        </div>
        <div class="col-sm-12">
            <input type="text" name="name-email" class="hide">
        </div>
        <div class="col-sm-12">
            <?= Html::submitButton(Yii::t('app', 'Submit'), ['class' => 'btn btn-primary pull-right button_right_menu']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>