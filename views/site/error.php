<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
?>
<div class="container border_error">
    <div class="site-error col-xs-12">
        <div class=" col-xs-12  col-md-6 number_error">
            404
        </div>
        <div class="col-xs-12 col-md-6 text_site_error ">
            <h1><?= nl2br(Html::encode($message)) ?></h1>
            <ul>
                <li>1. The above error occurred while the Web server was processing your request.</li>
                <li>2. Please contact us if you think this is a server error. Thank you.</li>
            </ul>
            <div class="button_error_cl">
                <a href="<?= Url::home(); ?>" class="btn_per button_error">Go back to the main page
                </a></div>
        </div>
    </div>
</div>
