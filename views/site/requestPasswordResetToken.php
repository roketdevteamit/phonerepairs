<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-request-password-reset ">
    <h1 class="title"><?= Html::encode($this->title) ?></h1>

    <p class="text_reset_password">Please fill out your email. A link to reset password will be sent there.</p>
    <div class="col-sm-6" style="margin:0 auto;margin-top:50px;float:none;">
        <div class="row">
            <div class="col-sm-12 ">
                <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                <?= $form->field($model, 'email')->textInput(['class' => 'form_admin']) ?>
                <div class="form-group button_login">
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
