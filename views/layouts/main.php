<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

//use app\widgets\LangWidget;
//use app\models\Lang;
use app\assets\AppAsset;

AppAsset::register($this);

Yii::$app->language = 'en-US';
$configData = yii\helpers\ArrayHelper::map(app\models\Setting::find()->all(), 'key', 'value');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <?php $meta_tag = ''; ?>
        <?php if (isset($configData['meta_tag'])) { ?>
            <?php  $meta_tag = strip_tags($configData['meta_tag']); ?>
        <?php } ?>
        <meta name="description" content="<?= $meta_tag; ?>" />
        <meta name="keywords" content=""/>
        
        <link rel="shortcut icon" href="/images/icons/favicon.ico?v1" type="image/x-icon" />
        <link rel="apple-touch-icon" href="/images/icons/apple-touch-icon.png" />
        

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
</head>
<body>
<?php $this->beginBody() ?>
<?php
//var_dump($configData);
//?>
<?php $repairsArray = app\models\Repairs::find()->asArray()->orderBy('sort ASC')->all(); ?>
<?php $modelPages = \app\models\Pages::find()->where(['status' => 1])->asArray()->orderBy('sort ASC')->all(); ?>

<div class="">
    <div class="header_style">
        <div class="col-xs-12 col-sm-4 header_style_text">
            <div>
                <a href="/"><img src="<?= Url::home(); ?>images/main-logo-2.svg"></a>

                <div class="clearfix"></div>
                <h3>SAME DAY REPAIR CENTRE</h3>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 logo_style">
            <img src="<?= Url::home(); ?>images/same-day-logo.svg">
        </div>
        <div class="col-xs-12 col-sm-4 right_blocks_header ">
            <div>

                <h2 class="text-white text-right small-only-text-center right_text_style">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                    <a class="style_telephone_header"
                       href="tel:02033022930">
                        <?php
                        if (isset($configData['telephone'])) {
                            ?>
                            <?= strip_tags($configData['telephone']) ?>
                        <?php } else {
                        } ?>
                    </a>
                    <div class="clearfix height_blocks"></div>
                    <p class="text-right small-only-text-center ">
                        <?php
                        if (isset($configData['open_hours'])) {
                            ?>
                            <?= strip_tags($configData['open_hours']) ?>
                        <?php } else {
                        } ?>
                    </p>
                    <p class="text-right small-only-text-center">Our
                        Locations:
                        <?php if (isset($configData['location'])) { ?>
                            <?= strip_tags($configData['location']); ?>
                        <?php } else { ?>
                        <?php } ?>
                    </p>
                </h2>
                <ul class="nav navbar-nav  social_links">
                    <?php
                    if (isset($configData['facebook'])) {
                        ?>
                        <li>
                            <a target="_blank" href="<?= strip_tags($configData['facebook']) ?>">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php } else {
                    } ?>
                    <?php
                    if (isset($configData['twitter'])) {
                        ?>
                        <li><a target="_blank" href="<?= strip_tags($configData['twitter']) ?>"><i class="fa fa-twitter"
                                                                                                   aria-hidden="true"></i></a>
                        </li>
                    <?php } else {
                    } ?>
                    <?php
                    if (isset($configData['linkedin'])) {
                        ?>
                        <li><a target="_blank" href="<?= strip_tags($configData['linkedin']) ?>"><i
                                        class="fa fa-instagram"
                                        aria-hidden="true"></i>
                            </a>
                        </li>
                    <?php } else {
                    } ?>
                    <?php
                    if (isset($configData['linkedin'])) {
                        ?>
                        <li><a target="_blank" href="<?= strip_tags($configData['telephoneinstagram']) ?>"><i
                                        class="fa fa-linkedin"
                                        aria-hidden="true"></i>
                            </a></li>
                    <?php } else {
                    } ?>
                </ul>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
    <nav class="navbar navbar-inverse menu_header" style="position:relative;">
        <?php foreach ($repairsArray as $repairs) { ?>
            <div class="BrandMenuBlock BrandMenuBlock<?= $repairs['id']; ?>" data-repairs_id="<?= $repairs['id']; ?>"
                 style="display:none;position:absolute;width:100%;bottom:0px;top:44px;z-index: 9999999">
                <div style="background-color:#fafafa; border:1px solid #ddd;">
                    <div class="container">
                        <div class="blockBrand row"></div>

                    </div>

                    <div class="" style="border-top: 1px solid #ddd; text-align:center;">
                        <div class="blockModels" style="padding:10px; z-index: 1"></div>
                        <div style="clear:both;"></div>
                    </div>
                    <div class="container" style=" text-align:center;">
                        <a  href="<?= Url::home() . 'repairs/' . $repairs['slug']; ?>" class="viewAll" style="display:none;margin-bottom:10px;">VIEW ALL</a>
                        <div style="clear:both;"></div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="container-fluid main-navigation menu_style_header">

            <div id="navbar" class="navbar-collapse collapse style_mob_menu" aria-expanded="false">
                <ul class="nav navbar-nav" style="position:relative;">
                    <?php foreach ($repairsArray as $repairs) { ?>
                        <li class="dropdown">
                            <a class="repairsMenuHover repairsMenuHover<?= $repairs['id']; ?>"
                               data-repairs_id="<?= $repairs['id']; ?>"
                               href="<?= Url::home() . 'repairs/' . $repairs['slug']; ?>"
                               class="dropbtn"> <?= $repairs['name']; ?>
                                <span class="after_style"> </span>
                            </a>
                        </li>
                    <?php } ?>

                    <?php foreach ($modelPages as $page) { ?>
                        <?php if($page['type'] != '2'){?> 
                            <li><a href="<?= Url::home() . 'page/' . $page['slug']; ?>"><?= $page['name']; ?></a></li>
                        <?php }?>
                    <?php } ?>

                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php
                    if (isset($_GET['search'])) {
                        $search_value = $_GET['search'];
                    } else {
                        $search_value = '';
                    }
                    ?>
                    <li style="position:relative;">
                        <a class="inputSearchButton class_search"><span class="glyphicon glyphicon-search "></span></a>
                        <div class="inputSearch"
                             style="display:none;position:absolute;width:400px;z-index:999;right:0px;">
                            <form action="/search" method="get">
                                <input type="text" class="form-control form_style_search" name="search"
                                       value="<?= $search_value; ?>" placeholder="Search for...">
                            </form>
                        </div>
                    </li>
                    <?php if (!Yii::$app->user->isGuest) {
                        $str = strpos(Yii::$app->user->identity->email, "@");
                        $username = substr(Yii::$app->user->identity->email, 0, $str); ?>
                        <li class="dropdown">
                            <a class="dropdown-toggle admin_name" data-toggle="dropdown" href="#"><span
                                        class="glyphicon glyphicon-user padding_logout"></span><?= $username; ?>
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu button_logout">
                                <li><a href="<?= Url::home() . 'profile/enquiries'; ?>">Profile</a></li>
                                <li>
                                    <?= Html::beginForm(['/site/logout'], 'post') ?>
                                    <?= Html::submitButton(
                                        'Logout') ?>
                                    <?= Html::endForm() ?>
                                </li>
                            </ul>
                        </li>
                    <?php } else { ?>
                        <!--                    <li><a href="--><? //= Url::home() . 'site/signup'; ?><!--"><span class="glyphicon glyphicon-user"></span>-->
                        <!--                            Sign Up</a></li>-->
                        <li><a href="<?= Url::home() . 'login'; ?>"><span
                                        class="glyphicon glyphicon-user"></span>
                            </a></li>
                    <?php } ?>
                </ul>
            </div>

        </div>
    </nav>
    <nav class="navbar navbar-default menu_header1">
        <div class="container-fluid">
            <form action="/search" method="get">
                <div class="input-group userform" style="margin-top:12px;">
                    <input type="text" class="form-control" name="search" placeholder="Search for..." value="<?= $search_value; ?>">
                        <span class="input-group-btn" data-original-title="" title="">
                          <button type="submit" class="btn btn-default button_search">Go!</button>
                        </span>
                </div>
                
            </form>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed menu_mob" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <p class="block_menu block_menu_text">MENU</p>
                    <div class="block_menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar" style="background-color: black"></span>
                        <span class="icon-bar" style="background-color: black"></span>
                        <span class="icon-bar" style="background-color: black"></span>
                    </div>
                </button>

            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse style_mob_menu" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                   <?php foreach ($repairsArray as $repairs) { ?>
                        <li><a href="<?= Url::home() . 'repairs/' . $repairs['slug']; ?>"><?= $repairs['name']; ?></a></li>
                    <?php } ?>
                    <?php foreach ($modelPages as $page) { ?>
                        <li><a href="<?= Url::home() . 'page/' . $page['slug']; ?>"><?= $page['name']; ?></a></li>
                    <?php } ?>
                    <?php if (!Yii::$app->user->isGuest) {
                        $str = strpos(Yii::$app->user->identity->email, "@");
                        $username = substr(Yii::$app->user->identity->email, 0, $str); ?>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span
                                        class="glyphicon glyphicon-user"></span><?= $username; ?>
                                <span class="after_style"></span></a>
                            <ul class="dropdown-menu button_logout_mob">
                                <li><a href="<?= Url::home() . 'profile/enquiries'; ?>">Profile</a></li>
                                <li>
                                    <?= Html::beginForm(['/site/logout'], 'post') ?>
                                    <?= Html::submitButton(
                                        'Logout') ?>
                                    <?= Html::endForm() ?>
                                </li>
                            </ul>
                        </li>
                    <?php } else { ?>
                        <!--                    <li><a href="--><? //= Url::home() . 'site/signup'; ?><!--"><span class="glyphicon glyphicon-user"></span>-->
                        <!--                            Sign Up</a></li>-->
                        <li><a href="<?= Url::home() . 'login'; ?>"><span
                                        class="glyphicon glyphicon-log-in"></span>
                            </a></li>
                    <?php } ?>
                </ul>
            </div>

        </div><!-- /.navbar-collapse -->
    </nav>
</div><!-- /.container-fluid -->
<?php
NavBar::begin(['options' => ['style' => 'display:none;']]);
NavBar::end();
?>
<?php /* ?>
        <ul class="nav navbar-nav">
            <?= LangWidget::widget(); ?>
        </ul>
    <?php */ ?>
<?php
/*

echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'About', 'url' => ['/site/about']],
        ['label' => 'Contact', 'url' => ['/site/contact']],
        Yii::$app->user->isGuest ? (
            ['label' => 'Login', 'url' => ['/site/login']]
        ) : (
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . $username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        )
    ],
]);
NavBar::end(); */
?>
<?= Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>
<?= $content ?>

<div class="border_style"></div>
<div class="container container_style">
    <div class="col-xs-12 product_style">
        <?php foreach ($repairsArray as $repairs) { ?>
            <?php
            $img_src = "/images/pages/default.jpg";
            if (($repairs['img_src'] != '') && ($repairs['img_src'] != null)) {
                $img_src = "/images/repair/" . $repairs['img_src'];
            }
            ?>
            <div class="col-xs-6 col-sm-3 mob1">
                <a href="<?= Url::home() . 'repairs/' . $repairs['slug']; ?>">
                    <img src="<?= $img_src; ?>">
                    <p>
                        <?= $repairs['name']; ?>
                    </p>
                </a>
            </div>
        <?php } ?>
        <div class="col-xs-6 col-sm-3 mob1">
           <a href="<?= Url::home(); ?>makeenquiry">
                <img src="<?= Url::home() . 'images/repair/'; ?>data-recovery.jpg">
                <p>
                    Data Recovery
                </p>
            </a>
        </div>
    </div>
</div>
<div class="border_style"></div>
<div class="container container_style padding_0">
    <div class="col-sm-12 img_black padding_0">

        <img class="grow col-xs-4 col-sm-2 black1" src="/images/brand/brand-asus.png">
        <img class="grow col-xs-4 col-sm-2 black2" src="/images/brand/brand-acer.png">
        <img class="grow col-xs-4 col-sm-2 black3" src="/images/brand/brand-samsung.png">
        <img class="grow col-xs-4 col-sm-2 black4" src="/images/brand/brand-apple.png">
        <img class="grow col-xs-4 col-sm-2 black5" src="/images/brand/brand-nokia.png">
        <img class="grow col-xs-4 col-sm-2 black6" src="/images/brand/brand-sony.png">

        <!--                    <img class="grow" src="http://static.phonerepairs4u.co.uk/assets/images/brand-hp.png">-->
    </div>
</div>

<footer class="footer">

    <div class="container footer_body">
        <div class="footer_menu1 col-xs-12 col-sm-2">
            <p><a href="/repairs/laptop">Laptop Repair</a></p>
            <p><a href="/page/book-repair">Book Your Repair!</a></p>
        </div>
        <div class="footer_menu2 col-xs-12 col-sm-3">
            <p><a href="/page/mail-your-device">Mail Your Device</a></p>
            <p><a href="/page/corporate">Corporate Repairs</a></p>
            <p><a href="/page/contact-us">Contact Us</a></p>
        </div>
        <div class="footer_adres col-xs-12 col-sm-7">
            <p class="add">
                <?php
                if (isset($configData['location'])) {
                    ?>
                    <?= strip_tags($configData['location']) ?>
                <?php } else {
                } ?> </p>
            
            <?php if (isset($configData['location_address'])) { ?>
                <?= $configData['location_address'] ?>
            <?php } ?>
            
            <p class="tel">
                T.
            <?php if (isset($configData['telephone'])) { ?>
                <?= strip_tags($configData['telephone']); ?>
            <?php } ?>
            </p>
        </div>
        <div class="foter_style_height col-sm-12"></div>
        <div class="footer_information col-xs-12 col-sm-6">
            <p class="hover_style"><a style="color: white" href="<?= Url::home(); ?>page/terms-conditions">Terms & Conditions</a></p>
            <p>Copyright 2017 © PhoneRepairs4u.co.uk</p>
        </div>
        <div class="footer_mob col-xs-12 col-sm-6">
            <a class="style_telephone_header" href="tel:02033022930">
                <?php if (isset($configData['telephone'])) {
                    ?>
                    <p>Call now on:</p>
                    <?= strip_tags($configData['telephone']) ?>
                <?php } else {
                } ?>
            </a>
        </div>
        <div class="social_style col-xs-12 col-sm-12">
            <ul class="nav navbar-nav  social_links">
                <?php
                if (isset($configData['facebook'])) {
                    ?>
                    <li><a target="_blank" href="<?= strip_tags($configData['facebook']) ?>"><i
                                    class="fa fa-facebook"
                                    aria-hidden="true"></i></a>
                    </li>
                <?php } else { ?>
                <?php } ?>
                <?php
                if (isset($configData['twitter'])) {
                    ?>
                    <li><a target="_blank" href="<?= strip_tags($configData['twitter']) ?>"><i class="fa fa-twitter"
                                                                                               aria-hidden="true"></i></a>
                    </li>
                <?php } else {
                } ?>
                <?php if (isset($configData['linkedin'])) { ?>
                    <li><a target="_blank" href="<?= strip_tags($configData['linkedin']) ?>"><i
                                    class="fa fa-instagram"
                                    aria-hidden="true"></i>
                        </a>
                    </li>
                <?php } ?>
                <?php
                if (isset($configData['telephoneinstagram'])) {
                    ?>
                    <li><a target="_blank" href="<?= strip_tags($configData['telephoneinstagram']) ?>"><i
                                    class="fa fa-linkedin"
                                    aria-hidden="true"></i>
                        </a></li>
                <?php } else {
                } ?>
            </ul>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
