<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use kartik\datetime\DateTimePicker;

$this->title = $modelPage->name . ' | Phones Repair 4u';
?>
<?php
$img_src = "/images/pages/default.jpg";
if (($modelPage->header_img != '') && ($modelPage->header_img != null)) {
    $img_src = "/images/pages/" . $modelPage->header_img;
}
?>

<div style="margin: 0 0 1em 0;
        float: left;
        display: block;
        width: 100%;
        padding: 5em 0 4em;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center center;
        background-size: cover;
        background-image: url(<?= $img_src; ?>)">
    <h1 class="modelPage"><?= $modelPage->name; ?></h1>
</div>
<div>
    <?= $modelPage->content; ?>
</div>


<div>
    <?php
    if (Yii::$app->session->hasFlash('form_sended')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Thank you for contacting us!',
        ]);
    endif;
    if (Yii::$app->session->hasFlash('form_not_sended')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Some error!',
        ]);
    endif;
    ?>

    <div class="container">
        <div style="clear: both"></div>
        <div class="text_book">
            <b>GET FREE QUOTE!</b><br>
            <p>£5 discount on repairs when you book online!</p>
        </div>
        <?php $form = ActiveForm::begin(
            [
                'options' => [
                    'class' => 'userform'
                ]]
        ); ?>
        <div class="col-sm-12">
            <div class="col-xs-12 col-sm-6 padding_0">
                <div style="height: 0px">
                    <?= $form->field($modelNewLeads, 'key')->hiddenInput(['value' => 'BK'])->label(false); ?>
                    <?= $form->field($modelNewLeads, 'leads_type')->hiddenInput(['value' => 2])->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Name:</p>
                    <?= $form->field($modelNewLeads, 'name')->textinput()->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Email:</p>
                    <?= $form->field($modelNewLeads, 'email')->input('email')->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Phone:</p>
                    <?= date('Y-m-d h:m:i'); ?>
                    <?= $form->field($modelNewLeads, 'phone_number')->textinput(['placeholder' => 'Home Number or Mobile Number'])->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Booking Date / Time</p>
                    <?= $form->field($modelNewLeads, 'booking_date')->widget(DateTimePicker::classname(), [
                        'options' => ['placeholder' => date('Y-m-d'), 'value' => (string)date('Y-m-d h:m')],
                        'type' => DateTimePicker::TYPE_INPUT,
                        'pluginOptions' => [
                            'autoclose'=>true,
                        ]
                    ])->label(false); ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 padding_0">
                <div class="col-xs-12">
                    <p>Device Model</p>
                    <?= $form->field($modelNewLeads, 'device_model')->textinput(['placeholder' => 'e.g iPhone 6, Samsung Galaxy S5'])->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Device colour:</p>
                    <?= $form->field($modelNewLeads, 'device_color')->textinput()->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Device fault:</p>
                    <?= $form->field($modelNewLeads, 'device_fault')->textinput()->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Booking Branch</p>
                    <?= $form->field($modelNewLeads, 'branch_id')->dropDownList($branchArray)->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['name' => 'bookButton', 'class' => 'btn btn-primary pull-right button_book']) ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>