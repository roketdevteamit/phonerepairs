<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\BranchesmapAsset;

BranchesmapAsset::register($this);

$this->title = $modelPage->name.' | Phones Repair 4u';
?>
<?php
$img_src = "/images/pages/default.jpg";
if (($modelPage->header_img != '') && ($modelPage->header_img != null)) {
    $img_src = "/images/pages/" . $modelPage->header_img;
}
?>

<div  style="margin: 0 0 1em 0;
        float: left;
        display: block;
        width: 100%;
        padding: 5em 0 4em;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center center;
        background-size: cover;
        background-image: url(<?= $img_src; ?>)">
    <h1 style="color:white;text-align: center;font-size: 45px;font-weight: 800"><?= $modelPage->name; ?></h1>
</div>
<div class="container">
    <div class="style_text_branches">
        <?= $modelPage->content; ?>
    </div>
    <div class="heignt_40"></div>
    <?php foreach ($modelBranches as $branches){ ?>
        <div class=" padding_0" >
            <div class="col-sm-7 p_class padding_0">
                <p class="address_street"><b><?= $branches['address_street']; ?></b></p>
                <p><?= $branches['address']; ?></p>
                <p>
                    Call branch directly:
                    <?= $branches['telephone']; ?>
                </p>
                <p  style=" margin-top: 25px; ">
                    <span class="Opening_times"><b>Opening times:</b></span><br>
                    <?= $branches['open']; ?>
                </p>
            </div>
            <div class="col-sm-5 blockMap" style="padding: 0px;" data-branch_id="<?= $branches['id']; ?>"
                 data-address="<?= strip_tags($branches['address_street']); ?>, <?= strip_tags($branches['address']); ?>">
                <div id="map<?= $branches['id']; ?>" style="width:100%;height:200px;"></div>
            </div>
        </div>
        <hr>
    <?php } ?>
</div>
    