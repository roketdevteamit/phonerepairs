<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
$this->title = $modelPage->name.' | Phones Repair 4u';
?>
<?php
$img_src = "/images/pages/default.jpg";
if (($modelPage->header_img != '') && ($modelPage->header_img != null)) {
    $img_src = "/images/pages/" . $modelPage->header_img;
}
?>

<div style="margin: 0 0 1em 0;
        float: left;
        display: block;
        width: 100%;
        padding: 5em 0 4em;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center center;
        background-size: cover;
        background-image: url(<?= $img_src; ?>)">
    <h1 class="modelPage"><?= $modelPage->name; ?></h1>
</div>
<div class="container">

    <div class="style_text_corporate">
        <?= $modelPage->content; ?>
    </div>


    <?php
    if (Yii::$app->session->hasFlash('form_sended')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Thank you for contacting us!',
        ]);
    endif;
    if (Yii::$app->session->hasFlash('form_not_sended')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Some error!',
        ]);
    endif;
    ?>

    <div class="style_head"></div>
    <p class="title">Corporate Repair Form</p>
    <?php $form = ActiveForm::begin(
        [
            'options' => [
                'class' => 'userform'
            ]]
    ); ?>
    <div class="col-xs-12 padding_0">
        <div class="col-xs-12 col-sm-6 padding_0">
            <div style="height:0px;">
                <?= $form->field($modelNewLeads, 'key')->hiddenInput(['value' => 'CR'])->label(false); ?>
                <?= $form->field($modelNewLeads, 'leads_type')->hiddenInput(['value' => '4'])->label(false); ?>
            </div>
            <div class="col-xs-12">
                <p>Company Name</p>
                <?= $form->field($modelNewLeads, 'name')->textinput()->label(false); ?>
            </div>
            <div class="col-xs-12">
                <p>Company Representative Name</p>
                <?= $form->field($modelNewLeads, 'company_r_name')->textinput()->label(false); ?>
            </div>
            <div class="col-xs-12">
                <p>Company Contact No.</p>
                <?= $form->field($modelNewLeads, 'phone_number')->textinput()->label(false); ?>
            </div>
            <div class="col-xs-12">
                <p>Company Email</p>
                <?= $form->field($modelNewLeads, 'email')->input('email')->label(false); ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 padding_0" >
            <div class="col-xs-12">
                <p>Company URL</p>
                <?= $form->field($modelNewLeads, 'company_url')->textinput()->label(false); ?>
            </div>
            <div class="col-xs-12 ">
                <p>Additional Info</p>
                <?= $form->field($modelNewLeads, 'description')->textarea()->label(false); ?>
            </div>
            <div class="col-xs-12  button_width userform">
                <?= Html::submitButton(Yii::t('app', 'Submit'), ['name' => 'bookButton', 'class' => 'btn btn-primary pull-right']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>