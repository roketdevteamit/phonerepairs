<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\LinkPager;
    
    $this->title = $modelPage->name.' | Phones Repair 4u';
?>
<?php 
    $img_src = "/images/pages/default.jpg";
    if(($modelPage->header_img != '') && ($modelPage->header_img != null)){
        $img_src = "/images/pages/".$modelPage->header_img;
    }
?>

<div style="margin: 0 0 1em 0;
    float: left;
    display: block;
    width: 100%;
    padding: 5em 0 4em;
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center center;
    background-size: cover;
    background-image: url(<?= $img_src; ?>)">
    <h1 style="color:white;text-align: center;font-size: 45px;font-weight: 800"><?= $modelPage->name; ?></h1>
</div>
<div >
    <?= $modelPage->content; ?>

</div>