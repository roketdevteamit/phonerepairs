<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
$this->title = $modelPage->name.' | Phones Repair 4u';
?>
<?php
$img_src = "/images/pages/default.jpg";
if (($modelPage->header_img != '') && ($modelPage->header_img != null)) {
    $img_src = "/images/pages/" . $modelPage->header_img;
}
?>

<div style="margin: 0 0 1em 0;
        float: left;
        display: block;
        width: 100%;
        padding: 5em 0 4em;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-position: center center;
        background-size: cover;
        background-image: url(<?= $img_src; ?>)">
    <h1 style="color:white;text-align: center;font-size: 45px;font-weight: 800"><?= $modelPage->name; ?></h1>
</div>
<div class="container">
    <div class="style_text_corporate">
        <?= $modelPage->content; ?>
    </div>
</div>
<?php
if (Yii::$app->session->hasFlash('form_sended')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Thank you for contacting us!',
    ]);
endif;
if (Yii::$app->session->hasFlash('form_not_sended')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Some error!',
    ]);
endif;
?>


<?php $form = ActiveForm::begin(
    [
        'options' => [
            'class' => 'p_style_mail userform'
        ]]
); ?>
<div class="style_forms_mail">
    <div class="container">
        <div class="col-xs-12  class_size_form  padding_0" >
            <div class="col-xs-12 col-sm-6">
                <div style="height: 0px">
                    <?= $form->field($modelNewLeads, 'key')->hiddenInput(['value' => 'ML'])->label(false); ?>
                    <?= $form->field($modelNewLeads, 'leads_type')->hiddenInput(['value' => '1'])->label(false); ?>
                </div>
                
                <div class="col-xs-12">
                    <p>Name</p>
                    <?= $form->field($modelNewLeads, 'name')->textinput()->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Full Address (incl. post code)</p>
                    <?= $form->field($modelNewLeads, 'address')->textinput()->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Email Address</p>
                    <?= $form->field($modelNewLeads, 'email')->input('email')->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Contact No.</p>
                    <?= $form->field($modelNewLeads, 'phone_number')->textinput()->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Device Model</p>
                    <?= $form->field($modelNewLeads, 'device_model')->textinput()->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>Phone Pass</p>
                    <?= $form->field($modelNewLeads, 'phone_password')->textinput()->label(false); ?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6" style="padding: 0px;">
                <div class=" col-xs-12">
                    <p>Quote Given?</p>
                    <?= $form->field($modelNewLeads, 'quote')->textinput()->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <p>IMEI</p>
                    <?= $form->field($modelNewLeads, 'IMEI')->textinput()->label(false); ?>
                </div>
                <div class="col-xs-12">
                    <span class="p_style_mail">15 digit number, type in <b>*#06#</b>  into your phone.</span>
                    <br>
                    <p>Description of your fault/notes</p>
                    <?= $form->field($modelNewLeads, 'description')->textarea(['rows' => '5','placeholder'=>'Yor description'])->label(false); ?>
                </div>
                <div class="col-xs-12 button_width userform">
                    <?= Html::submitButton(Yii::t('app', 'Submit'), ['name' => 'bookButton', 'class' => 'btn btn-primary pull-right']) ?>
                <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="clear: both"></div>
<div class="container container_center">
    <div class="style_h4">Once you submit the form, please wait briefly as your form is generated, once it is
        generated you will be
        redirected.</div>
    <div class="style_head"></div>
</div>