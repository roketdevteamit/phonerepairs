<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;
$this->title = 'Corporate | Phones Repair 4u';
?>
<?php Yii::$app->language = 'en-US'; ?>


<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu',[]); ?>
        
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height: 0px; padding-top: 10px;">
                                <i class="fa " data-action="show"> </i> Corporate
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border_gray">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelCorporate,
                                        'columns' => [
                                            [
                                                'attribute' => 'id',
                                                'format' => 'html',
                                                'value' => function ($modelCorporate) {    
                                                    return '<a href="/profile/corporateshow?id='.$modelCorporate['id'].'">'.$modelCorporate['key'].''.$modelCorporate['id'].'</a>';
                                                }
                                            ],
                                            [
                                                'attribute' => 'Company name',
                                                'format' => 'html',
                                                'value' => function ($modelCorporate) {    
                                                    return $modelCorporate['name'];
                                                }
                                            ],
                                            [
                                                'attribute' => 'Company Representative Name',
                                                'format' => 'html',
                                                'value' => function ($modelCorporate) {    
                                                    return $modelCorporate['company_r_name'];
                                                }
                                            ],
                                            [
                                                'attribute' => 'Company Email',
                                                'format' => 'html',
                                                'value' => function ($modelCorporate) {    
                                                    return $modelCorporate['email'];
                                                }
                                            ],
                                            [
                                                'attribute' => 'Company Contact No.',
                                                'format' => 'html',
                                                'value' => function ($modelCorporate) {    
                                                    return $modelCorporate['phone_number'];
                                                }
                                            ],
                                            'date_create',
//                                            [
//                                                'class' => 'yii\grid\ActionColumn',
//                                                'template' => '{show} {delete} {update}',
//                                                'buttons' => [
//                                                    'show' => function ($url,$modelRepairs) {
//                                                            return Html::a(
//                                                            '<span class="glyphicon glyphicon-eye-open"></span>', 
//                                                            'repairshow?id='.$modelRepairs['id']);
//                                                    },
//                                                    'update' => function ($url,$modelRepairs) {
//                                                            return Html::a(
//                                                            '<span class="glyphicon glyphicon-pencil"></span>', 
//                                                            'repairupdate?id='.$modelRepairs['id']);
//                                                    },
//                                                    'delete' => function ($url,$modelRepairs) {
//                                                            return Html::a(
//                                                            '<span class="glyphicon glyphicon-trash"></span>', 
//                                                            'repairdelete?id='.$modelRepairs['id']);
//                                                    },
//                                                ],
//                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>