<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;
//var_dump($this->context->getRoute());exit;

    $class = ($this->context->getRoute() == 'profile/enquiries')?'active':''; 
    $class1 = ($this->context->getRoute() == 'profile/bookings')?'active':''; 
    $class2 = ($this->context->getRoute() == 'profile/mail_in')?'active':''; 
    $class3 = ($this->context->getRoute() == 'profile/pick_ups')?'active':''; 
    $class4 = ($this->context->getRoute() == 'profile/corporate')?'active':''; 
    $class5 = ($this->context->getRoute() == 'profile/index')?'active':''; 
?>
<style>
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        color: #464a4c;
        background-color: #fff;
        border-color: #ddd #ddd #fff;
    }
</style>
<?php Yii::$app->language = 'en-US'; ?>

<ul class="nav nav-tabs tab_title_client">
    <li class="nav-item">
        <?= HTML::a('Enquiries', Url::home().'profile/enquiries', ['class' => 'nav-link '.$class]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Bookings', Url::home().'profile/bookings', ['class' => 'nav-link '.$class1]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Mail in', Url::home().'profile/mail_in', ['class' => 'nav-link '.$class2]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Pick Ups', Url::home().'profile/pick_ups', ['class' => 'nav-link '.$class3]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Corporate', Url::home().'profile/corporate', ['class' => 'nav-link '.$class4]); ?>
    </li>
    <li class="nav-item">
        <?= HTML::a('Setting', Url::home().'profile/index', ['class' => 'nav-link '.$class5]); ?>
    </li>
</ul>
