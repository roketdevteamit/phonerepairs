<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;
$this->title = 'Enquiries | Phones Repair 4u';
?>
<?php Yii::$app->language = 'en-US'; ?>

<div class="dashboard-container">
    <div class="container">
        <?php
            if(Yii::$app->session->hasFlash('delete_enquiries')):
                echo Alert::widget([
                    'options' => [
                        'class' => 'alert-info',
                    ],
                    'body' => 'Sended',
                ]);
            endif;
        ?>
        <?= $this->render('menu',[]); ?>
        
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height: 0px; padding-top: 10px;">
                                <i class="fa" data-action="show"> </i> Enquiry
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border_gray">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelEnquiry,
                                        'columns' => [
                                            [
                                                'attribute' => 'id',
                                                'format' => 'html',
                                                'value' => function ($modelEnquiry) {    
                                                    return '<a href="/profile/enquiriesshow?id='.$modelEnquiry['id'].'">'.$modelEnquiry['key'].''.$modelEnquiry['id'].'</a>';
                                                }
                                            ],
                                            'name',
                                            'email',
                                            'phone_number',
                                            'date_create',
                                            [
                                                'attribute' => 'Source',
                                                'value' => function ($modelEnquiry) {    
                                                    if(($modelEnquiry['hear_us'] != '') && ($modelEnquiry['hear_us'] != null)){
                                                         $hear_us = ['1' => 'Google', '2' => 'Search Engine', '3' => 'External Website', '4' => 'Word of Mouth', '5' => 'Leaflets/Newspaper', '6' => 'Yell.com', '7' => 'Bing', '8' => 'Yahoo'];
                                                        return $hear_us[(int)$modelEnquiry['hear_us']];                    
                                                    }else{
                                                        return '';        
                                                    }
                                                }
                                            ],
                                            'status',
//                                            [
//                                                'attribute' => 'Service',
//                                                'value' => function ($modelEnquiry) {    
//                                                    if($modelEnquiry['service_id'] != 0){
//                                                        return \app\widgets\GetServiceNameWidget::widget(['service_id' => $modelEnquiry['service_id']]);
//                                                    }
//                                                }
//                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>