<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;

$this->title = 'Profile | Phones Repair 4u';

?>


<div class="container">
    <div class="">
        <?= $this->render('menu', []); ?>
    </div>
    <!--    <div style="clear: both"></div>-->
<?php
    if (Yii::$app->session->hasFlash('profile_updated')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => 'Profile updated',
        ]);
    endif;
    if (Yii::$app->session->hasFlash('profile_not_updated')):
        echo Alert::widget([
            'options' => [
                'class' => 'alert-error',
            ],
            'body' => 'Profile not  updated',
        ]);
    endif;
    ?>
    <div class="col-xs-12 col-sm-8  block_style">
        <div class="title title_form" style="height:40px;">
            <i class="fa" data-action="show"> </i> Change user
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?php $form = ActiveForm::begin(
                    [
                        'options' => [
                            'class' => 'userform'
                        ]]
                ); ?>
                <div class="col-xs-12 col-sm-4">
                    <p>Name</p>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <?= $form->field($modelUser, 'name')->textInput()->label(false); ?>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <p>Email</p>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <?= $form->field($modelUser, 'email')->textInput()->label(false); ?>
                </div>
                
                <div class="col-xs-12 col-sm-4">
                    <p>Address</p>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <?= $form->field($modelUser, 'address')->textInput()->label(false); ?>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <p>Phone number</p>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <?= $form->field($modelUser, 'phone_number')->textInput()->label(false); ?>
                </div>
                
                
                <div class="col-xs-12 col-sm-4">
                    <p>Password</p>
                </div>
                
                <div class="col-xs-12 col-sm-8">
                    <?= $form->field($modelUser, 'password')->textInput()->label(false); ?>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <p>Password repeat</p>
                </div>
                <div class="col-xs-12 col-sm-8">
                    <?= $form->field($modelUser, 'password_repeat')->textInput()->label(false); ?>
                </div>
                <div class="col-xs-12 col-sm-8 button_width userform button_merge">
                    <?= Html::submitButton(Yii::t('app', 'Sent'), ['name' => 'change_commect', 'class' => 'btn btn-primary pull-right']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
