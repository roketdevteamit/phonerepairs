<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;
$this->title = 'Pick ups | Phones Repair 4u';

?>
<?php Yii::$app->language = 'en-US'; ?>


<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu',[]); ?>
        
        <div class="dashboard-wrapper-lg">
            <div class="row wrap" >
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height: 0px; padding-top: 10px;">
                                <i class="fa" data-action="show"> </i> Bookings
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border_gray">
                                    <?= GridView::widget([
                                        'dataProvider' => $modelPickups,
                                        'columns' => [
                                            'id',
                                            'name',
                                            'email',
                                            'date_create',
//                                            [
//                                                'class' => 'yii\grid\ActionColumn',
//                                                'template' => '{show} {delete} {update}',
//                                                'buttons' => [
//                                                    'show' => function ($url,$modelRepairs) {
//                                                            return Html::a(
//                                                            '<span class="glyphicon glyphicon-eye-open"></span>', 
//                                                            'repairshow?id='.$modelRepairs['id']);
//                                                    },
//                                                    'update' => function ($url,$modelRepairs) {
//                                                            return Html::a(
//                                                            '<span class="glyphicon glyphicon-pencil"></span>', 
//                                                            'repairupdate?id='.$modelRepairs['id']);
//                                                    },
//                                                    'delete' => function ($url,$modelRepairs) {
//                                                            return Html::a(
//                                                            '<span class="glyphicon glyphicon-trash"></span>', 
//                                                            'repairdelete?id='.$modelRepairs['id']);
//                                                    },
//                                                ],
//                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>