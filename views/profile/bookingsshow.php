<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;
$this->title = 'Bookings details | Phones Repair 4u';
?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('message_send')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Sended',
    ]);
endif;
if (Yii::$app->session->hasFlash('message_not_send')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Not sended',
    ]);
endif;
?>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu', []); ?>

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8  block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Bookings
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 ">
                                    <table class="table">
                                        <tr>
                                            <td>Name</td>
                                            <td><?= $modelBooking->name; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Phone number</td>
                                            <td><?= $modelBooking->phone_number; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><?= $modelBooking->email; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Device model</td>
                                            <td><?= $modelBooking->device_model; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Description</td>
                                            <td><?= $modelBooking->description; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Branch</td>
                                            <td>
                                                <?php if ($modelBooking->branch_id != 0) { ?>
                                                    <?= \app\widgets\GetBranchAddressWidget::widget(['branch_id' => $modelBooking->branch_id]); ?>
                                                <?php } ?>
                                        </tr>
                                        <tr>
                                            <td>Model</td>
                                            <td>
                                                <?php if ($modelBooking->model_id != 0) { ?>
                                                    <?= \app\widgets\GetModelsNameWidget::widget(['models_id' => $modelBooking->model_id]); ?>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Service</td>
                                            <td>
                                                <?php if ($modelBooking->service_id != 0) { ?>
                                                    <?= \app\widgets\GetServiceNameWidget::widget(['service_id' => $modelBooking->service_id]); ?>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php $status_array = ['1' => 'Order received', '2' => 'Order in progress', '3' => 'Awaiting details from client', '4' => 'Order ready for packaging', '5' => 'Order on route to delivery']; ?>
                                        <tr>
                                            <td>Booking status</td>
                                            <td>
                                                <?php if ($modelBooking->status != 0) { ?>
                                                    <?= $status_array[$modelBooking->status]; ?>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-8  block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i>  Email quote
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div style="height: 0px">
                                        <?= $form->field($modelNewLeadmessage, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
                                        <?= $form->field($modelNewLeadmessage, 'leads_id')->hiddenInput(['value' => $modelBooking->id])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Content</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                        <?= $form->field($modelNewLeadmessage, 'content')->textarea()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform button_merge">
                                        <?= Html::submitButton(Yii::t('app', 'Sent'), ['name' => 'sent_commect', 'class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                                <div class="col-sm-12">
                                    <?php if ($modelLeadmessage) { ?>
                                        <div class="well">
                                            <?php foreach ($modelLeadmessage as $leadmessage) { ?>
                                                <div class="col-sm-12">
                                                    <p>
                                                        <i><b><?= $leadmessage['email'] . ' - ' . $leadmessage['date_create']; ?></b></i>
                                                    </p>
                                                    <p><?= $leadmessage['content']; ?></p>
                                                </div>
                                                <div style="clear: both"></div>
                                                <hr>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>