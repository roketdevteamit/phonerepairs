<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\widgets\LinkPager;
use yii\grid\GridView;
use dosamigos\tinymce\TinyMce;
use yii\helpers\StringHelper;
$this->title = 'Enquiries | Phones Repair 4u';
?>
<?php Yii::$app->language = 'en-US'; ?>
<?php
if (Yii::$app->session->hasFlash('message_send')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-info',
        ],
        'body' => 'Sended',
    ]);
endif;
if (Yii::$app->session->hasFlash('message_not_send')):
    echo Alert::widget([
        'options' => [
            'class' => 'alert-error',
        ],
        'body' => 'Not sended',
    ]);
endif;
?>

<div class="dashboard-container">
    <div class="container">
        <?= $this->render('menu', []); ?>

        <div class="dashboard-wrapper-lg">
            <div class="row wrap">
                <div class="col-xs-12 col-sm-8  block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title" style="height:40px;">
                                <i class="fa " data-action="show"> </i> Enquiries
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12 table_border_gray">
                                    <table class="table">
                                        <tr>
                                            <td>Name</td>
                                            <td><?= $modelEnquiry->name; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Phone number</td>
                                            <td><?= $modelEnquiry->phone_number; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><?= $modelEnquiry->email; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Device model</td>
                                            <td><?= $modelEnquiry->device_model; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Description</td>
                                            <td><?= $modelEnquiry->description; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Where did you hear about us?</td>
                                            <td>
                                                <?php
                                                $hear_us = [
                                                    '0' => '',
                                                    '1' => 'Google',
                                                    '2' => 'Search Engine',
                                                    '3' => 'External Website',
                                                    '4' => 'Word of Mouth',
                                                    '5' => 'Leaflets/Newspaper',
                                                    '6' => 'Yell.com',
                                                    '7' => 'Bing',
                                                    '8' => 'Yahoo'
                                                ];
                                                echo $hear_us[$modelEnquiry->hear_us];
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-8  block_style" style="margin-bottom: 20px">
                    <div class="widget">
                        <div class="widget-header">
                            <div class="title title_form" style="height:40px;">
                                <i class="fa " data-action="show"> </i>  Email quote
                            </div>
                        </div>
                        <div class=" widget-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php $form = ActiveForm::begin(
                                        [
                                            'options' => [
                                                'class' => 'userform'
                                            ]]
                                    ); ?>
                                    <div style="height:0px ;">
                                    <?= $form->field($modelNewLeadmessage, 'user_id')->hiddenInput(['value' => \Yii::$app->user->id])->label(false); ?>
                                    <?= $form->field($modelNewLeadmessage, 'leads_id')->hiddenInput(['value' => $modelEnquiry->id])->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-4">
                                        <p>Content</p>
                                    </div>
                                    <div class="col-xs-12 col-sm-8">
                                    <?= $form->field($modelNewLeadmessage, 'content')->textarea()->label(false); ?>
                                    </div>
                                    <div class="col-xs-12 col-sm-8 button_width userform button_merge">
                                        <?= Html::submitButton(Yii::t('app', 'Sent'), ['name' => 'sent_commect', 'class' => 'btn btn-primary pull-right']) ?>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                                <div class="col-sm-12">
                                    <?php if ($modelLeadmessage) { ?>
                                        <div class="well">
                                            <?php foreach ($modelLeadmessage as $leadmessage) { ?>
                                                <div class="col-sm-12">
                                                    <p>
                                                        <i><b><?= $leadmessage['email'] . ' - ' . $leadmessage['date_create']; ?></b></i>
                                                    </p>
                                                    <p><?= $leadmessage['content']; ?></p>
                                                </div>
                                                <div style="clear: both"></div>
                                                <hr>
                                            <?php } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>